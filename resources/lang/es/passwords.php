<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Su password ha sido cambiado!',
    'sent' => 'Se ha enviado un un link para resetear se password al correo!',
    'throttled' => 'Por favor espere antes de reintentar.',
    'token' => 'El token para reseteo de contraseña es envalido.',
    'user' => "No podemos encontrar el usuario con esa direccion de email.",

];
