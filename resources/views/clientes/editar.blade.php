<x-jet-form-section submit="update">
    <x-slot name="title">
        {{ __('Editar cliente') }}
    </x-slot>

    <x-slot name="description">
		{{ __('') }}
		<div class="py-5">
			<x-jet-nav-link href="{{ route('clientes') }}" wire:loading.attr="disabled" >
				<x-jet-button type="buton">
					{{ __('Volver a la lista') }}
				</x-jet-button>
			</x-jet-nav-link>
			{{--<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver a la lista') }}
			</x-jet-button>--}}
		</div>
    </x-slot>
	<x-slot name="form">
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nombre" value="{{ __('Nombre / Razón social') }}" />
			<x-jet-input id="nombre" type="text" class="mt-1 block w-full" wire:model.defer="nombre" autocomplete="nombre" />
			<x-jet-input-error for="nombre" class="mt-2" />
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nit" value="{{ __('Nit') }}" />
			<x-jet-input id="nit" type="text" class="mt-1 block w-full" wire:model.defer="nit" autocomplete="nit" />
			<x-jet-input-error for="nit" class="mt-2" />
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="telefono" value="{{ __('Teléfono') }}" />
			<x-jet-input id="telefono" type="text" class="mt-1 block w-full" wire:model.defer="telefono" autocomplete="telefono" />
			<x-jet-input-error for="telefono" class="mt-2" />
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="email_factura" value="{{ __('Email para facturación') }}" />
			<x-jet-input id="email_factura" type="text" class="mt-1 block w-full" wire:model.defer="email_factura" autocomplete="email_factura" />
			<x-jet-input-error for="email_factura" class="mt-2" />
		</div>   
		
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="direccion" value="{{ __('Dirección') }}" />
			<x-jet-input id="direccion" type="text" class="mt-1 block w-full" wire:model.defer="direccion" autocomplete="direccion" />
			<x-jet-input-error for="direccion" class="mt-2" />
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="ciudad" value="{{ __('Ciudad') }}" />
			<x-jet-input name="ciudad" list="municipios" id="ciudad" type="text" class="mt-1 block w-full" wire:model.defer="ciudad" autocomplete="ciudad" />
			<x-jet-input-error for="ciudad" class="mt-2" />

			<datalist id="municipios">
				@foreach ($municipios as $municipio)
					<option value="{{$municipio->municipio}}">	
				@endforeach
			</datalist>
		</div>  
		
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="notas" value="{{ __('Notas sobre el cliente') }}" />
			<x-textarea id="notas" class="mt-1 block w-full" wire:model="notas" autocomplete="notas" rows="4"/>
			<x-jet-input-error for="notas" class="mt-2" />
		</div>  

		<x-slot name="actions">
			@if (session()->has('mensaje'))
				<div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('mensaje') }}
				</div>
			@endif
			
			<x-jet-button wire:loading.attr="disabled">
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		

	</x-slot>
</x-jet-form-section>
