<x-section submit="update" cols="">
    <x-slot name="title">
        {{ __('Datos cliente') }}
    </x-slot>

    <x-slot name="description">
		{{ __('') }}
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
    </x-slot>
	<x-slot name="datos">
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nombre" value="{{ __('Nombre') }}" />
			{{$nombre}}
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nit" value="{{ __('Nit') }}" />
			{{$nit}}
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="telefono" value="{{ __('Teléfono') }}" />
			{{$telefono}}
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="direccion" value="{{ __('Dirección') }}" />
			{{$direccion}}
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="ciudad" value="{{ __('Ciudad') }}" />
			{{$ciudad}}
		</div>  

		<div class="col-span-8 sm:col-span-8">
			<x-jet-label for="ciudad" value="{{ __('Notas sobre el cliente') }}" />
			{{$notas}}
		</div>  

		<div class="col-span-8 sm:col-span-8">
			<span class="text-md text-bold">Contactos</span>
			<table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
				<tr>
					<thead>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Nombre</th>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Cargo</th>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Email</th>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Teléfono</th>
					</thead>
				</tr>
				<tbody>
					@foreach ($contactos as $contacto)
						<tr>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->nombre}}</td>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->cargo}}</td>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->email}}</td>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->telefono}}</td>
						</tr> 
					@endforeach
				</tbody>
			</table>
		</div>

	</x-slot>
</x-section>

