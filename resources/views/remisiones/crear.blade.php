<x-jet-form-section submit="store">
    <x-slot name="title">
        {{ __('Nueva cotización') }}
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
	</x-slot>
	
	<!-- Formulario -->
	<x-slot name="form">
		<!-- Cliente -->
		<div class="col-span-6 sm:col-span-6" >
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			<x-jet-input class="w-full" type="text" wire:model="queryCliente" id="cliente" />
			@if(!empty($queryCliente))
				<div class="border rounded overflow-y-auto h-32 bg-gray-50 absolute z-5 w-1/2">
					<ul>
						@foreach ($clientes as $cliente)
							<li class="block px-4 hover:bg-gray-300 cursor-pointer" wire:click="seleccionarCliente({{$cliente->id}})">
								<span class="text-sm text-gray-900">{{ $cliente->nombre}}</span>
							</li>
						@endforeach
					</ul>
				</div>
			@endif
			@error('cliente_id')
				<p class="text-red-700 bg-red-200 w-full border border-red-600 rounded-md px-2" >{{$message}}</p>
			@enderror
			<!-- Cliente nombre -->
			<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 pt-4 pb-0">
				<x-jet-label for="nit" value="{{ __('Nombre/razón social:') }}" />
				<span class="font-bold"> {{ $cliente_data['nombre'] }} </span>
			</div>
		</div>
		
		<!-- Cliente -->
		{{--<div class="col-span-6 sm:col-span-6" >
			
			@if (session()->has('errorCliente'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorCliente') }}
				</div>
			@endif
		</div>  --}}

		<!-- Nit -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="nit" value="{{ __('Nit:') }}" />
			<span class="text-xs"> {{ $cliente_data['nit'] }} </span>
		</div>

		<!-- Teléfono -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="telefono" value="{{ __('Teléfono:') }}" />
			<span class="text-xs"> {{ $cliente_data['telefono'] }} </span>
		</div>

		<!-- Ciudad -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="ciudad" value="{{ __('Ciudad:') }}" />
			<span class="text-xs"> {{ $cliente_data['ciudad'] }} </span>
		</div>

		<!-- Revisó -->
		<div class="col-span-2 sm:col-span-2 md:col-span- lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="reviso" value="{{ __('Revisado por') }}" />
			<x-select id="reviso" class="mt-1 block w-full" wire:model.defer="reviso" required >
				<option value=""></option>
				@foreach ($usuarios as $usuario)
					<option value="{{$usuario->id}}">{{$usuario->name}}</option>
				@endforeach
			</x-jet-select>
			<x-jet-input-error for="reviso" class="mt-2" />
		</div>  

		<!-- Aprobó -->
		<div class="col-span-2 sm:col-span-2 md:col-span- lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="aprobo" value="{{ __('Aprobado por') }}" />
			<x-select id="aprobo" class="mt-1 block w-full" wire:model.defer="aprobo" required >
				<option value=""></option>
				@foreach ($usuarios as $usuario)
					<option value="{{$usuario->id}}">{{$usuario->name}}</option>
				@endforeach
			</x-jet-select>
			<x-jet-input-error for="aprobo" class="mt-2" />
		</div>  
		<!-- Factura -->
		<div class="col-span-2 sm:col-span-2 md:col-span- lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="factura" value="{{ __('# Factura') }}" />
			<x-jet-input clas="" wire:model.defer="factura"></x-jet-input>
			<x-jet-input-error for="aprobo" class="mt-2" />
		</div>  

		<!-- Acciones -->
		<x-slot name="actions">
			<x-jet-action-message class="mr-3" on="saved">
				{{ __('Guardado...') }}
			</x-jet-action-message>
	
			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		
	</x-slot>

</x-jet-form-section>
