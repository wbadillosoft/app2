<div class="col-span-9 sm:col-span-8 mb-1 flex" >
	<x-jet-button name="nueva_remision" id="nueva_remision" class="mt-1 mr-4 inline" wire:click="formCrear">
		Nueva remisión
	</x-jet-button>
	<x-jet-input type="text" wire:model.refer="search" class="mt-1 inline" placeholder="Buscar remision..." name="search" />
	<span class="flex-auto pl-4">
		{{ $remisiones->links() }}
	</span>
	
</div>

<div class="bg-white overflow-x-auto  shadow-xl sm:rounded-lg">
	<table class="table-auto" style="width:100%">
		<thead>
		  <tr>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Remisión</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Fecha</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Cliente</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">SubTotal</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">IVA</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Total</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider"># Factura</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider"></th>
		  </tr>
		</thead>

		<tbody class="bg-white divide-y divide-gray-200">
			@foreach ($remisiones as $remision)
			<tr>
				
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">{{$remision->codigo}}</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-xs leading-5 text-gray-500">	
						{{$remision->fecha}}
						<div class=" text-gray-200">
							{{$remision->usuario->name}}
						</div></div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						{{ $remision->cliente->nombre}}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($remision->subtotal,0,',','.') }}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($remision->iva,0,',','.') }}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($remision->total,0,',','.') }}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						{{$remision->factura}} 
					</div>
				</td>
			
				<td class="px-4 py-2 whitespace-no-wrap border-bottom text-center">
					<div class="leading-0 text-gray-500">
						{{--<x-button class="px-1 py-1 text-xs bg-teal-600" wire:click="show({{ $remision->id }})">Ver</x-jet-button>--}}
						<x-button class="px-1 py-1 text-xs bg-yellow-600" wire:click="formEditar({{ $remision->id }})">Editar</x-jet-button>
					</div>
				</td>
			</tr>
			@endforeach
		
		</tbody>
	  </table>
	  <div class=" m-2">
		{{ $remisiones->links() }}
	</div>
</div>
