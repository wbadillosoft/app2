<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-base text-gray-800 leading-tight">
            {{ __('Bienvenido a la aplicación de administración de Esisco') }}
        </h2>
    </x-slot>

    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-4 lg:px-4">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-xs p-4 flex flex-wrap justify-center gap-4">
               
                <x-link2 link="/procesos" class="border border-gray-100 hover:bg-gray-50 text-sm w-56 min-w-48 grid grid-cols-3 place-content-center ">
                    <span class="col-span-1">
                        <div class=" rounded-full h-12 w-12 flex items-center justify-center border">
                            <i class="la-2x las la-tasks text-orange-700"></i>
                        </div>
                    </span>
                    <span class="col-span-2">Procesos y actividades</span>
                </x-link2>

                <x-link2 link="" class="border border-gray-100 hover:bg-gray-50 text-sm w-56 min-w-48 grid grid-cols-3 place-content-center ">
                        <span class="col-span-1">
                            <div class=" rounded-full h-12 w-12 flex items-center justify-center border">
                                <i class="la-2x las la-file-invoice text-blue-700"></i>
                            </div>
                        </span>
                        <span class="col-span-2">Consultar cartera</span>
                </x-link2>
                

                <x-link2 link="" class="border border-gray-100 hover:bg-gray-50 text-sm w-56 min-w-48 grid grid-cols-3 place-content-center ">
                    <span class="col-span-1">
                        <div class=" rounded-full h-12 w-12 flex items-center justify-center border">
                            <i class="la-2x las la-signal text-green-700"></i>
                        </div>
                    </span>
                    <span class="col-span-2">Ventas por mes</span>
                </x-link2>
            </div>
        </div>
    </div>
</x-app-layout>
