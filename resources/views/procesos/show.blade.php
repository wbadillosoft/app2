<x-loading></x-loading>
<x-section submit="" cols="">
    <x-slot name="title">
        {{ __('Seguimiento de actividades por proceso') }}
    </x-slot>

    <x-slot name="description">	
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
    </x-slot>
	<x-slot name="datos">
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nombre" value="{{ __('Título') }}" />
			{{$titulo}}
		</div>         

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			{{$proceso->cliente->nombre}}
		</div>  
		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="estado" value="{{ __('Estado actual') }}" />
			@if($estado)Cerrado
			@else Abierto
			@endif
		</div>         
		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="fecha_apertura" value="{{ __('Fecha de inicio') }}" />
			{{$fecha_apertura}}
		</div>
		@if($estado)        
			<div class="col-span-3 sm:col-span-3">
				<x-jet-label for="fecha_apertura" value="{{ __('Fecha de cierre') }}" />
				{{$fecha_cierre}}
			</div>      
		@endif   

		<div class="col-span-8 sm:col-span-8">
			<span class="text-md text-bold">Actividades / Observaciones</span>
			<table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
				<tr>
					<thead>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Actividad</th>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Fecha</th>
					</thead>
				</tr>
				<tbody>
					@foreach ($proceso->actividades as $actividad)
						<tr>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">
								{{$actividad->actividad}}
								<div class="text-xs text-blue-400">
									Registrado por: {{$actividad->usuario}}
								</div>
							</td>
							<td class="px-3 py-2 whitespace-normal font-light text-xs border-bottom w-32 ">{{$actividad->fecha}}</td>
						</tr> 
					@endforeach
				</tbody>
			</table>
		</div>

	</x-slot>
</x-section>

