<x-loading></x-loading>

<div>
	<div class="col-span-9 sm:col-span-8 mb-1 px-1 w-full md:w-auto" x-data="{ filtro: @entangle('filtro') }">
		<x-jet-button name="nuevo_proceso" id="nuevo_proceso" class="mt-1 inline w-full md:w-auto" wire:click="formCrear">
			Abrir un nuevo proceso
		</x-jet-button>
		<div class="hidden md:inline">
			<x-button name="todos_procesos" id="todos_procesos" class="mt-1 inline bg-teal-500 px-1 md:px-4 py-2" wire:click="listado(2)">
				Todos
			</x-button>
			<x-button name="" id="" class="mt-1 inline bg-green-500 px-1 md:px-4 py-2" wire:click="listado(0)">
				Abiertos
			</x-button>
			<x-button name="" id="" class="mt-1 inline bg-red-500 px-1 md:px-4 py-2" wire:click="listado(1)">
				Cerrados
			</x-button>
			<x-button name="" id="btnFiltro" class="mt-1 inline bg-gray-400 px-1 md:px-4 py-2" wire:click="openFiltro">
				<i class="las la-filter"></i> Filtrar
			</x-button>
		</div>

		<div class="md:hidden inline">
			<div class="flex space-x-1">
				<div class="flex-grow mt-1 bg-teal-500 px-1 py-2 text-white text-center rounded" wire:click="listado(2)"> Todos </div>
				<div class="flex-grow mt-1 bg-green-500 px-1 py-2 text-white text-center rounded" wire:click="listado(0)"> Abiertos </div>
				<div class="flex-grow mt-1 bg-red-500 px-1 py-2 text-white text-center rounded" wire:click="listado(1)"> Cerrados </div>
				<div class="flex-grow mt-1 bg-gray-400 px-1 py-2 text-white text-center rounded" wire:click="openFiltro"> Filtrar </div>
			</div>
		</div>
	</div>
	
<div class="bg-white shadow-xl sm:rounded-lg border">
	<table class="table-auto w-full" >
		<thead class="table-row-group w-full">
		  <tr class="table-row">
			<th class="h-7 table-cell px-4 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider "></th>
			<th class="h-7 table-cell px-4 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider">Proceso</th>
			<th class="h-7 hidden lg:table-cell px-2 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider">Cliente</th>
			<th class="h-7 hidden lg:table-cell px-2 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider">Inicio</th>
			{{-- <th class="h-7 hidden lg:table-cell px-2 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider">Estado</th> --}}
			<th class="h-7 hidden lg:table-cell px-2 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider">Tiempo</th>
			<th class="h-7 table-cell px-2 py-4 bg-gray-300 text-left text-xs leading-4 font-large text-gray-700 uppercase tracking-wider"></th>
		</thead>

		<tbody class="bg-white divide-y divide-gray-100 table-row-group">
			@foreach ($procesos as $proceso)   
				@php
					$tiempo = (strtotime($proceso->fecha_apertura) - strtotime(Date('ymd')) )/86400;
					$dias   = $proceso->dias; 
					$tiempo = abs($tiempo); 
					$tiempo = floor($tiempo);
					//if($tiempo>1) $tiempo.='s';

					$color_text 		= 'text-gray-500';
					$color_text_ligth 	= 'text-gray-300';
					$icon 				= 'la-smile text-gray-700';

					if($tiempo>3 && $tiempo<($dias-3)) 
					{
						$color_text 	  = 'text-yellow-400';
						$color_text_ligth = 'text-yellow-300';
						$icon 			  = 'la-exclamation text-yellow-400';
					}
					if($tiempo>$dias) 
					{
						$color_text 		= 'text-red-600';
						$color_text_ligth 	= 'text-red-400';
						$icon 				= 'la-exclamation-triangle text-red-700';
					}
					if($proceso->estado) 
					{
						$color_text 		= 'text-blue-600';
						$color_text_ligth 	= 'text-blue-400';
						$icon 				= 'la-check text-blue-700';
					}
					$tiempo = floor($tiempo).' día';
					if($tiempo>1) $tiempo.='s';					
				@endphp 
			<tr class="table-row">
				<td class="table-cell px-1 md:px-4 py-2 whitespace-no-wrap border-bottom text-center"> 
					@if($tiempo>7) <i class="las la-2x {{$icon}}"></i>
					@elseif($tiempo>3 && $tiempo<7) <i class="las la-2x {{$icon}}"></i>
					@else<i class="las la-2x {{$icon}}"></i>
					@endif
				</td>

				<td class="max-w-xs table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="max-w-full text-elipsis overflow-hidden truncate text-sm leading-5 md:font-normal font-bold {{$color_text}}">{{$proceso->titulo}}</div>
					
					<div class="lg:hidden block font-thin">
						<div class="text-xs leading-5 {{$color_text}}">{{$proceso->nombre}}</div>
						<div class="text-xs text-gray-400 {{$color_text_ligth}}">{{ $proceso->asignado }}</div>
						<div class="text-xs leading-5 {{$color_text}} grid grid-cols-2 place-content-between ">
							<span>
								{{ $tiempo }} ({{ $proceso->dias }})
							</span>
							
							<div class="leading-0 {{$color_text}} flex justify-end ">
								<div class="text-gray-500 inline-flex items-center w-10 h-10 p-2 rounded-full hover:bg-gray-300 cursor-pointer"  wire:click="show({{ $proceso->id }})">
									<svg 
										class="p-1"
										fill="rgb(161 161 170)" 
										xmlns="http://www.w3.org/2000/svg" 
										viewBox="0 0 576 512">
										<path d="M144.3 32.04C106.9 31.29 63.7 41.44 18.6 61.29c-11.42 5.026-18.6 16.67-18.6 29.15l0 357.6c0 11.55 11.99 19.55 22.45 14.65c126.3-59.14 219.8 11 223.8 14.01C249.1 478.9 252.5 480 256 480c12.4 0 16-11.38 16-15.98V80.04c0-5.203-2.531-10.08-6.781-13.08C263.3 65.58 216.7 33.35 144.3 32.04zM557.4 61.29c-45.11-19.79-88.48-29.61-125.7-29.26c-72.44 1.312-118.1 33.55-120.9 34.92C306.5 69.96 304 74.83 304 80.04v383.1C304 468.4 307.5 480 320 480c3.484 0 6.938-1.125 9.781-3.328c3.925-3.018 97.44-73.16 223.8-14c10.46 4.896 22.45-3.105 22.45-14.65l.0001-357.6C575.1 77.97 568.8 66.31 557.4 61.29z"/>
									</svg>					
								</div>
								@if(!$proceso->estado)
								<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-300 cursor-pointer" wire:click="formEditar({{ $proceso->id }})">
									<svg 
										class="p-1"
										fill="rgb(161 161 170)" 
										xmlns="http://www.w3.org/2000/svg" 
										viewBox="0 0 576 512">
										<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
									</svg>
								<div>
								@else
								<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-50 cursor-not-allowed" >
									<svg 
										class="p-1"
										fill="rgb(212 212 216)" 
										xmlns="http://www.w3.org/2000/svg" 
										viewBox="0 0 576 512">
										<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
									</svg>
								<div>
								@endif
							</div>
						</div>
					</div>
				</td>

				<td class="max-w-sm hidden md:table-cell px-4 py-2 whitespace-no-wrap border-bottom">
					<div class="lg:block text-elipsis overflow-hidden truncate text-sm leading-5 {{$color_text}}">{{$proceso->nombre}}</div>
				</td>
				<td class="hidden lg:table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 {{$color_text}}">
						{{$proceso->fecha_apertura}}
						<div class="text-xs text-gray-400 {{$color_text_ligth}}">{{ $proceso->asignado }}</div>
					</div>
				</td>
				{{-- <td class="hidden lg:table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 {{$color_text}}">
						@if(!$proceso->estado)
							Abierto
						@else 
							Cerrado
						@endif
					</div>
				</td> --}}
				<td class="hidden lg:table-cell  px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 {{$color_text}}"> 
						{{ $tiempo }} ({{ $proceso->dias }})
					</div>
				</td>
				<td class="hidden md:table-cell px-4 py-2 whitespace-no-wrap border-bottom text-center">
					<div class="leading-0 {{$color_text}} block md:inline">
						<div class="text-gray-500 inline-flex items-center w-10 h-10 p-2 rounded-full hover:bg-gray-300 cursor-pointer"  wire:click="show({{ $proceso->id }})">
							<svg 
								class="p-1"
								fill="rgb(161 161 170)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M144.3 32.04C106.9 31.29 63.7 41.44 18.6 61.29c-11.42 5.026-18.6 16.67-18.6 29.15l0 357.6c0 11.55 11.99 19.55 22.45 14.65c126.3-59.14 219.8 11 223.8 14.01C249.1 478.9 252.5 480 256 480c12.4 0 16-11.38 16-15.98V80.04c0-5.203-2.531-10.08-6.781-13.08C263.3 65.58 216.7 33.35 144.3 32.04zM557.4 61.29c-45.11-19.79-88.48-29.61-125.7-29.26c-72.44 1.312-118.1 33.55-120.9 34.92C306.5 69.96 304 74.83 304 80.04v383.1C304 468.4 307.5 480 320 480c3.484 0 6.938-1.125 9.781-3.328c3.925-3.018 97.44-73.16 223.8-14c10.46 4.896 22.45-3.105 22.45-14.65l.0001-357.6C575.1 77.97 568.8 66.31 557.4 61.29z"/>
							</svg>					
						</div>
						@if(!$proceso->estado)
						<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-300 cursor-pointer" wire:click="formEditar({{ $proceso->id }})">
							<svg 
								class="p-1"
								fill="rgb(161 161 170)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
							</svg>
						<div>
						@else
						<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-50 cursor-not-allowed" >
							<svg 
								class="p-1"
								fill="rgb(212 212 216)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
							</svg>
						<div>
						@endif
					</div>
				</td>
			</tr>

			@endforeach
		</tbody>
	  </table>
	  {{ $procesos->links() }}
	 
</div>

<x-confirmation-modal wire:model="filtro" bgColor="bg-blue-100">
	<x-slot name="icono">
		<i class="las la-filter la-2x text-blue-600"></i>
	</x-slot>
	<x-slot name="title">Aplicar filtro</x-slot>
	<x-slot name="content">
		<div class="grid grid-cols-12 gap-2">
			<div class="col-span-8" wire:ignore>
				<x-jet-label for="searchCliente" value="{{ __('Cliente') }}" />
				<select name="searchCliente" id="searchCliente" class="searchCliente" wire:model.defer="searchCliente">
					<option value=""></option>
					@foreach ($clientes as $item)
						<option value="{{$item->id}}">{{$item->nombre}}</option>
					@endforeach
				</select>
				<x-jet-input-error for="searchCliente" class="mt-2" />
			</div>  
			<div class="col-span-4">
				<x-jet-label for="searchFecha" value="{{ __('Fecha') }}" />
				<x-input  class="mt-2" type="date" id="searchFecha" wire:model.defer="searchFecha"/>
			</div>
			<div class="col-span-4">
				<x-jet-label for="searchAbiertoPor" value="{{ __('Abierto por') }}" />
				<x-select class="w-full"  wire:model.defer="searchAbiertoPor">
					<option value=""> </option>
					@foreach($usuarios as $usuario)
					<option value="{{$usuario->name}}">{{$usuario->name}}</option>
					@endforeach
				</x-select>
			</div>
			<div class="col-span-4">
				<x-jet-label for="searchResponsable" value="{{ __('Responsable') }}" />
				<x-select class="w-full"  wire:model.defer="searchResponsable">
					<option value=""> </option>
					@foreach($usuarios as $usuario)
					<option value="{{$usuario->name}}">{{$usuario->name}}</option>
					@endforeach
				</x-select>
			</div>
			<div class="col-span-4">
				<x-jet-label for="searchAsignadoA" value="{{ __('Asignado a') }}" />
				<x-input class="w-full" type="text" id="searchAsignadoA" wire:model.defer="searchAsignadoA"/>
			</div>
		</div>
	</x-slot>
	<x-slot name="footer">
		<button type="button"  wire:click="filtrar" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">
				Filtrar
		  </button>
		  <button type="button" id="cancelar" class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
				Cancelar
		  </button>
	</x-slot>
	
</x-confirmation-modal>
<style>
	.select2-selection__clear
	{
		width: 24px !important;
		height: 24px !important;
		border-radius: 50% !important;
		background-color: lightgrey !important;
		margin-right: 25px !important;
	}
</style>
<script>
	$(document).ready(function() {
    	$('.searchCliente').select2({
			width: '100%',
			allowClear: true,
			placeholder :'Seleccione un cliente',
			tags: true
		});
		
    	$('.searchCliente').on('change', function(){
			@this.set('searchCliente', this.value)
		});
		
    	$('#cancelar').on('click', function(){
			@this.set('filtro', false)
		});
		 
		$('#btnFiltro').click(function() {
			$(".searchCliente").val(null).trigger("change"); 
		});
	});
</script>
</div>