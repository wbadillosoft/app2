<style>

input.icon{ 
	background: url(imgs/lupa.png) no-repeat transparent;
	padding-left:2rem;
    background-size: 1rem;
    background-position: 0.5rem 0.75rem;
}
.option{
	font-size: 0.9rem;
}
.option .active{
	color: gray;
}

</style>
<x-loading></x-loading>
<x-jet-form-section submit="store" x-cloak >
    <x-slot name="title">
        {{ __('Nuevo proceso') }}
    </x-slot>

    <x-slot name="description">
		
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
    </x-slot>
	<x-slot name="form" >
		
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="abierto_por" value="{{ __('Abierto por') }}" />
			{!! auth()->user()->name !!}
			{{--<x-select id="abierto_por" name="abierto_por"  class="mt-1 block w-full" wire:model.defer="abierto_por">
				<option value="0"></option>
				@foreach ($usuarios as $usuario)
					<option value="{{$usuario->name}}">{{ $usuario->name }}</option>
				@endforeach
			</x-select>--}}
		
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="titulo" value="{{ __('Título') }}" />
			<x-jet-input id="titulo" type="text" class="mt-1 block w-full" wire:model.defer="titulo" autocomplete="titulo" />
			<x-jet-input-error for="titulo" class="mt-2" />
			@if (session()->has('errorTitulo'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorTitulo') }}
				</div>
			@endif
		</div>                    
	
		<div class="col-span-6 sm:col-span-6" wire:ignore>
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			<select name="cliente_id" id="select-cliente" wire:model.defer="cliente_id" class="select-cliente" style="width: 100%">
				<option value="" disabled="disabled"></option>
				@foreach ($clientes as $item)
					<option value="{{$item->id}}">{{$item->nombre}}</option>
				@endforeach
			</select>

			<x-jet-input-error for="cliente" class="mt-2" />
			<x-jet-action-message class="mr-3" on="errorCliente">
				{{ __('Seleccione un cliente.') }}
			</x-jet-action-message>
		</div>  
		
		<x-slot name="actions">
			<x-jet-action-message class="mr-3" on="saved">
				{{ __('Guardado...') }}
			</x-jet-action-message>
	
			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		
	</x-slot>
</x-jet-form-section>


