
<x-jet-form-section submit="update">
    <x-slot name="title">
        {{ __('Editar proceso') }}
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
			<x-jet-button wire:loading.attr="disabled" class="bg-green-700 hover:bg-green-900" wire:click="cerrar">
				{{ __('Cerrar proceso') }}
			</x-jet-button>
		</div>
	</x-slot>
	
	<x-slot name="form">

		<div class="col-span-6 sm:col-span-2">
			<x-jet-label for="abierto_por" value="{{ __('Abierto por') }}" />
			{!! $abierto_por !!}
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="titulo" value="{{ __('Título') }}" />
			<x-jet-input id="titulo" type="text" class="mt-1 block w-full" wire:model.defer="titulo" autocomplete="titulo" />
			<x-jet-input-error for="titulo" class="mt-2" />
		</div>    

		<div class="col-span-6 sm:col-span-2">
			<x-jet-label for="responsable" value="{{ __('Responsable') }}" />
			<x-select id="responsable" name="responsable"  class="mt-1 block w-full" wire:model.defer="responsable">
				<option value="0"></option>
				@foreach ($usuarios as $usuario)
					<option value="{{$usuario->name}}">{{ $usuario->name }}</option>
				@endforeach
			</x-select>
			<x-jet-input-error for="responsable" class="mt-2" />
			@if (session()->has('errorResponsable'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorResponsable') }}
				</div>
			@endif
		</div>  
			
		<div class="col-span-6 sm:col-span-4" wire:ignore>
			<x-jet-label for="asignado" value="{{ __('Asignado a') }}" />
			<x-jet-input id="asignado" type="text" class="mt-1 block w-full" wire:model.defer="asignado" autocomplete="asignado" />
			
			{{--
				<select class="select-multiple" wire:model.defer ="asignado" name="asignado[]" multiple="multiple" style="width:100%">
				<option value="" disabled="disabled"></option>
				@foreach ($usuarios as $item)
					<option value="{{$item->name}}">{{$item->name}}</option>	
				@endforeach
			</select>
			--}}
			<x-jet-input-error for="asignado" class="mt-2" />
		</div>  

		<div class="col-span-6 sm:col-span-6" wire:ignore>
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			<select name="cliente_id" id="select-cliente" wire:model.defer="cliente_id" class="select-cliente" style="width: 100%">
				<option value="" disabled="disabled"></option>
				@foreach ($clientes as $item)
					<option value="{{$item->id}}">{{$item->nombre}}</option>
				@endforeach
			</select>
			<x-jet-input-error for="cliente" class="mt-2" />
		</div>  
		
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="dias" value="{{ __('Tiempo estimado de proceso (Días)') }}" />
			<div class="col-span-2 sm:col-span-2">
				<x-jet-input id="dias" type="text" class="mt-1 block w-full" wire:model.defer="dias" autocomplete="dias" value="3"/>
			</div>
			<x-jet-input-error for="dias" class="mt-2" />
		</div>  

		<x-slot name="actions">
			@if (session()->has('mensaje'))
				<div class="text-success text-bold mr-4 pr-4" id="message" style="block">
					{{ session('mensaje') }}
				</div>
			@endif
			<x-jet-action-message class="mr-3 text-success text-bold mr-4 pr-4" on="updated">
				{{ __('Datos actualizados...') }}
			</x-jet-action-message>
			
			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
			<x-loading></x-loading>
		</x-slot>		

	</x-slot>
	
</x-jet-form-section>
