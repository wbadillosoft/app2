<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'App_2') }}</title>
        {{-- <link rel="icon" href="{{{ asset('imgs/favicon.ico') }}}"> --}}

        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
        <link rel="icon" type="image/png" href="{{ asset('icon.png') }}">

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        {{--<link rel="stylesheet" href="{{ asset('css/dist.css') }}">--}}
        <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
        <link rel="stylesheet" href="{{ asset('css/check.css') }}">
        <link rel="stylesheet" href="{{ asset('css/line-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/select2.min.css')}}">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
        
        {{--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />--}}

        @livewireStyles

        <!-- Scripts -->
        <script defer src="https://unpkg.com/alpinejs@3.7.0/dist/cdn.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.all.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script src="{{asset('js/select2.min.js')}}"></script>

        <style>
            [x-cloak] { display: none !important; }
        </style>
        @stack('js')

    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            {{--@livewire('navigation-dropdown')--}}
            @livewire('menu-principal')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>

        @stack('modals')

        {{-- <x-notification /> --}}

        @livewire('livewire-ui-modal')
        
        @livewireScripts

      
        <script>
       
            const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: true,
                    timerProgressBar: false,
                });
            const ToastInfo = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                });
                
            var pusher = new Pusher('84142e14b0d9a3f931b3', {
              cluster: 'sa1'
            });
        
            var channel = pusher.subscribe('notificaciones');

            channel.bind('solicitud', function(data) {
                Livewire.emit('nuevaSolicitud')
                Toast.fire({
                    background:'#6d28d9',
                    icon: 'success',
                    iconColor: '#FFF',
                    color: '#FFF',
                    title: 'Ha recibido una nueva solicitud de servicio de: ' + JSON.stringify(data.cliente),
                })
            });

          </script>
          
    </body>
</html>
