<x-section submit="update" cols="">
    <x-slot name="title">
        {{ __('Datos del producto') }}
    </x-slot>

    <x-slot name="description">
		{{ $descripcion }}
		<div class="text-xl" >
			<h2>
				Código: {{ $codigo }}
			</h2>
		</div>

		<div class="py-5">
			<x-jet-nav-link href="{{ route('productos') }}" wire:loading.attr="disabled" >
				<x-jet-button type="buton">
					{{ __('Volver a la lista') }}
				</x-jet-button>
			</x-jet-nav-link>
		</div>

    </x-slot>
	<x-slot name="datos">
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nombre" value="{{ __('Producto') }}" />
			{{$nombre}}
		</div>         

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="marca" value="{{ __('Marca') }}" />
			{{$marca}}
		</div>  
		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="referencia" value="{{ __('Referencia') }}" />
			{{$referencia}}
		</div>         

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="precio" value="{{ __('Precio') }}" />
			$ {{$precio}}
		</div>     
		
		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="iva" value="{{ __('IVA') }}" />
			{{$iva}} %
		</div>     

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="tiempo" value="{{ __('Tiempo de entrega') }}" />
			{{$tiempo}} días
		</div>         

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="presentacion" value="{{ __('Presentación') }}" />
			{{$presentacion}}
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="descripcion" value="{{ __('Descripción') }}" />
			{{$descripcion}}
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="observaciones" value="{{ __('Observaciones') }}" />
			{{$observaciones}}
		</div>  

		<div class="col-span-8 sm:col-span-8" x-data="{ open: false }">
			<span class="text-md text-bold">Proveedores</span>
			<table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
				<tr>
					<thead>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Proveedor</th>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Ciudad</th>
					</thead>
				</tr>
				<tbody>
					@foreach ($proveedores as $proveedor)
						<tr>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">
								<span class="cursor-pointer text-blue-800" wire:click="$set('proveedor_sel', {{$proveedor->proveedor}})" @click="open = true" >{{$proveedor->proveedor->nombre}}</span>
							</td>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$proveedor->proveedor->ciudad}}</td>
						</tr> 
					@endforeach
				</tbody>
			</table>
	
				<div x-show="open">
					<x-modal-info titulo="Datos del proveedor"> 
						<div class="text-sm"  @click.away="open = false">
							<div class="w-full" wire:loading>
								<svg xmlns="http://www.w3.org/2000/svg"  class="animate-spin h-6 w-6 m-auto " viewBox="0 0 512 512">
									<path d="M304 48C304 74.51 282.5 96 256 96C229.5 96 208 74.51 208 48C208 21.49 229.5 0 256 0C282.5 0 304 21.49 304 48zM304 464C304 490.5 282.5 512 256 512C229.5 512 208 490.5 208 464C208 437.5 229.5 416 256 416C282.5 416 304 437.5 304 464zM0 256C0 229.5 21.49 208 48 208C74.51 208 96 229.5 96 256C96 282.5 74.51 304 48 304C21.49 304 0 282.5 0 256zM512 256C512 282.5 490.5 304 464 304C437.5 304 416 282.5 416 256C416 229.5 437.5 208 464 208C490.5 208 512 229.5 512 256zM74.98 437C56.23 418.3 56.23 387.9 74.98 369.1C93.73 350.4 124.1 350.4 142.9 369.1C161.6 387.9 161.6 418.3 142.9 437C124.1 455.8 93.73 455.8 74.98 437V437zM142.9 142.9C124.1 161.6 93.73 161.6 74.98 142.9C56.24 124.1 56.24 93.73 74.98 74.98C93.73 56.23 124.1 56.23 142.9 74.98C161.6 93.73 161.6 124.1 142.9 142.9zM369.1 369.1C387.9 350.4 418.3 350.4 437 369.1C455.8 387.9 455.8 418.3 437 437C418.3 455.8 387.9 455.8 369.1 437C350.4 418.3 350.4 387.9 369.1 369.1V369.1z"/>
								</svg>
							</div>
							<table wire:loading.class="hidden">
								<tr>
									<td><b class="text-red-500">Nombre:</b></td>
									<td> {{$proveedor_sel['nombre'] }} </td>
								</tr>
								<tr>
									<td><b class="text-red-500">NIT:</b></td>
									<td>{{$proveedor_sel['nit']}} </td>
								</tr>
								<tr>
									<td><b class="text-red-500">Teléfono:</b></td>
									<td>{{$proveedor_sel['telefono']}} </td>
								</tr>
								<tr>
									<td><b class="text-red-500">Dirección:</b></td>
									<td>{{$proveedor_sel['direccion']}} </td>
								</tr>
								<tr>
									<td><b class="text-red-500">Email:</b></td>
									<td>{{$proveedor_sel['email']}} </td>
								</tr>
							</table>
						</div>
					</x-modal-info>
				</div>
			
				
		</div>

		<div class="col-span-8 sm:col-span-8">
			<span class="text-md text-bold">Costos</span>
			<table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
				<tr>
					<thead>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Costo</th>
						<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Proveedor</th>
					</thead>
				</tr>
				<tbody>
					@foreach ($costos as $costo)
						<tr>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">$ {{ number_format($costo->costo, 0, '', '.')}}</td>
							<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$costo->proveedor}}</td>
						</tr> 
					@endforeach
				</tbody>
			</table>
		</div>
	</x-slot>
</x-section>



