<x-form-section-2 submit="">
    <x-slot name="title">
        {{ __('Solicitud de servicio') }} <b>[ {{ $solicitud->codigo }} ]</b>
    </x-slot>

    <x-slot name="description">
        <div class="py-5">
            <x-jet-nav-link href="{{ route('solicitudes') }}" wire:loading.attr="disabled">
                <x-jet-button type="buton">
                    {{ __('Volver a la lista') }}
                </x-jet-button>
            </x-jet-nav-link>
        </div>
    </x-slot>

    <x-slot name="form">

        <div class="col-span-6 sm:col-span-2">
            <x-jet-label for="nombre" value="{{ __('Cliente') }}" />
            <x-jet-input id="nombre" type="text" class="mt-1 block w-full border-none bg-gray-50"
                wire:model.defer="nombre" autocomplete="nombre" />
            <x-jet-input-error for="nombre" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-2">
            <x-jet-label for="nit" value="{{ __('Nit') }}" />
            <x-jet-input id="nit" type="text" class="mt-1 block w-full border-none bg-gray-50" wire:model.defer="nit"
                autocomplete="nit" />
            <x-jet-input-error for="nit" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-2">
            <x-jet-label for="ciudad" value="{{ __('Ciudad') }}" />
            <x-jet-input id="ciudad" type="text" class="mt-1 block w-full border-none bg-gray-50"
                wire:model.defer="ciudad" autocomplete="ciudad" />
            <x-jet-input-error for="ciudad" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-2">
            <x-jet-label for="contacto" value="{{ __('Contacto') }}" />
            <x-jet-input id="contacto" type="text" class="mt-1 block w-full border-none bg-gray-50"
                wire:model.defer="contacto" autocomplete="contacto" />
            <x-jet-input-error for="contacto" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-2">
            <x-jet-label for="telefono" value="{{ __('Teléfono') }}" />
            <x-jet-input id="telefono" type="text" class="mt-1 block w-full border-none bg-gray-50"
                wire:model.defer="telefono" autocomplete="telefono" />
            <x-jet-input-error for="telefono" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-2">
            <x-jet-label for="email" value="{{ __('E-mail') }}" />
            <x-jet-input id="email" type="email" class="mt-1 block w-full border-none bg-gray-50"
                wire:model.defer="email" autocomplete="email" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>

      {{--   <x-slot name="actions">
            <x-jet-button wire:loading.attr="disabled">
                {{ __('Guardar') }}
            </x-jet-button>
        </x-slot> --}}
    </x-slot>

</x-form-section-2>

<div class="mt-5 md:mt-0 md:col-span-2">
    <div class="shadow overflow-hidden sm:rounded-md">
        <div class="px-4 py-5 bg-white sm:p-6">
            <table class="w-full col-span-6 table-auto border-collapse border border-slate-300 ">
                <thead class="text-xs text-gray-700 uppercase py-4">
                    <tr>
                        <td colspan="8" class="border border-transparent text-right py-2">
                            <x-button-blanck type="button"
                                class="pl-1 flex items-center bg-indigo-800  hover:bg-indigo-600 active:bg-indigo-900 focus:border-indigo-900 focus:ring-indigo-300"
                                onclick="Livewire.emit('openModal', 'nuevo-equipo', {{ json_encode(['solicitudId' => $solicitud->id]) }})">
                                <svg class="w-10 h-6 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                </svg>
                                <div class="pl-2 border-0 border-l text-sm font-normal">Agregar equipo</div>
                            </x-button-blanck>
                        </td>
                    </tr>

                    <tr>
                        <th class="text-left pl-1 py-4 bg-gray-200 w-10 px-2">Item</th>
                        <th class="text-left py-4 bg-gray-200 ">Equipo</th>
                        <th class="text-left py-4 bg-gray-200 ">Identificación</th>
                        <th class="text-left py-4 bg-gray-200 ">Servicios</th>
                        {{-- <th class="text-left py-4 bg-gray-200 ">Serial</th>
                        <th class="text-left py-4 bg-gray-200 ">Código Interno</th>
                        <th class="text-left py-4 bg-gray-200 ">Área/Ubicación</th> --}}
                        <th class="text-left py-4 bg-gray-200 "></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detalles as $item)
                        <tr class="hover:bg-slate-100 border-b hover:bg-gray-50 py-6">
                            <td class="py-2 pl-1 w-6 text-sm text-gray-600">{{ $loop->index + 1 }}</td>
                            <td class="py-4 w-1/4 text-sm px-1 text-gray-400">
                                <div class="font-bold mt-0 block py-0.5 border-0 border-white focus:ring-0 focus:border-indigo-700 text-gray-700 ">
                                    {{ $item->equipo }}
                                </div>
                                <div><span class="text-semibold text-gray-700">Marca:</span> {{ $item->marca }}</div>
                                <div><span class="text-semibold text-gray-700">Modelo: </span>{{ $item->modelo }}</div>
                            </td>
                            <td class="w-1/4 text-sm px-1 py-0.5 border-0 text-gray-400">
                                <div><span class="text-semibold text-gray-700">Serial: </span> {{ $item->serial }}</div>
                                <div><span class="text-semibold text-gray-700">Código interno: </span> {{ $item->codigo_interno }}</div>
                                <div><span class="text-semibold text-gray-700">Área/Ubicación: </span> {{ $item->ubicacion }}</div>
                            </td>

                            <td class=" mt-0 w-full px-0.5 py-0.5 border-0 text-gray-400 text-sm">
                                @if($item->mantenimiento)
                                    <div><span class="text-semibold text-gray-700">Mantenimiento: </span>{{ $item->mantenimiento_tipo }}</div> 
                                    <div><span class="text-semibold text-gray-700">Descripción del servicio: </span>{{ $item->descripcion }}</div> 
                                @endif
                                @if($item->metrologia)
                                    <div><span class="text-semibold text-gray-700">Metrología: </span>
                                        {{ $item->metrologia_tipo }} ({{ $item->magnitud }})
                                        @if($item->magnitud==='temperatura')
                                            <span class="text-semibold text-gray-700"> / Puntos de calibración: </span> 
                                        @else
                                            <span class="text-semibold text-gray-700"> / Rango: </span> 
                                        @endif
                                        {{ $item->rango }}
                                    </div> 
                                        <div>
                                       
                                        </div> 
                                @endif
                            </td>    
                            <td class=" text-right">
                                <button type="button" class="pr-2"
                                    onclick="Livewire.emit('openModal', 'editar-equipo', {{ json_encode(['equipoId' => $item->id]) }})">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-600" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" stroke-width="1">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                    </svg>
                                </button>
{{-- 
                                <button type="button" class="pr-2"
                                    wire:click="$emit('eliminar', {{ $item->id }})">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-600" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" stroke-width="1">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                    </svg>
                                </button> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Delete User Confirmation Modal -->
{{-- <x-jet-dialog-modal wire:model="deleteModal">
    <x-slot name="title">
        {{ __('Eliminar Item') }}
    </x-slot>

    <x-slot name="content">
        {{ __('¿Está seguro de eliminar este item del listado?.') }}
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="$toggle('deleteModal')" wire:loading.attr="disabled">
            {{ __('Cancelar') }}
        </x-jet-secondary-button>

        <x-jet-danger-button class="ml-3" wire:click="deleteItem" wire:loading.attr="disabled">
            {{ __('Eliminar Item') }}
        </x-jet-danger-button>
    </x-slot>
</x-jet-dialog-modal> --}}
</div>
