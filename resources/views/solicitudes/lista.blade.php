<div class="p-8 bg-white overflow-hidden shadow-xl sm:rounded-lg">
	<table class="table-auto" style="width:100%" id="myTable2" data-page-length='25'>
		<thead>
		  <tr>
			<th class="px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Código</th>
			<th class="px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Cliente</th>
			<th class="px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Fecha</th>
			<th class="px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Nro. Equipos</th>
			<th class="px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider"></th>
		  </tr>
		</thead>

		<tbody class="bg-white divide-y divide-gray-100">
			@foreach ($solicitudes as $solicitud)    
			<tr class="hover:bg-gray-50">
				<td class="px-4 py-1 whitespace-no-wrap ">
					<div class="text-sm leading-5 text-gray-500">
						{{$solicitud->codigo}} 
					</div>
				</td>
				<td class="px-4 py-1 whitespace-no-wrap ">
					<div class="text-sm leading-5 text-gray-500">
						{{$solicitud->nombre}} 
						@if($solicitud->estado===1)<span class="bg-red-600 h-4 text-xs px-1 py-0.5 inline-block leading-none text-center whitespace-nowrap align-baseline font-bold text-white rounded-full">
							nueva
						</span>
						@endif
					</div>
				</td>
				<td class="px-4 py-1 whitespace-no-wrap ">
					<div class="text-sm leading-5 text-gray-500">{{$solicitud->fecha}}</div>
				</td>
				<td class="px-4 py-1 whitespace-no-wrap ">
					<div class="text-sm leading-5 text-gray-500">
						{{$solicitud->detalles()->count()}} 
					</div>
				</td>
				<td class="px-1 py-1 whitespace-no-wrap text-right">
					<div class="leading-0 text-gray-500">
						<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-300 cursor-pointer" wire:click="formEditar({{ $solicitud->id }})">
							<svg 
								class="p-1"
								fill="rgb(161 161 170)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
							</svg>
						<div>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	  </table>
</div>
