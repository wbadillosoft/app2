<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('Información de perfil') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Actualice la información del perfil y al terminar pulsar el botón guardar.') }}
    </x-slot>

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Foto') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20"
                          x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Seleccione una nueva foto') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Quitar foto') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif
        <!-- Nombre -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="name" value="{{ __('Nombre') }}" />
            <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model.defer="state.name" autocomplete="name" />
            <x-jet-input-error for="name" class="mt-2" />
        </div>

        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email" value="{{ __('Email') }}" />
            <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="state.email" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
       
        <!-- Email cotizaciones-->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email_cotizacion" value="{{ __('Email para cotizaciones') }}" />
            <x-jet-input id="email_cotizacion" type="email_cotizacion" class="mt-1 block w-full" wire:model.defer="state.email_cotizacion" />
            <x-jet-input-error for="email_cotizacion" class="mt-2" />
        </div>
        
        <!-- Telefono -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="telefono" value="{{ __('Teléfono') }}" />
            <x-jet-input id="telefono" type="telefono" class="mt-1 block w-full" wire:model.defer="state.telefono" />
            <x-jet-input-error for="telefono" class="mt-2" />
        </div>
        
        <!-- Cargo -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="cargo" value="{{ __('Cargo') }}" />
            <x-jet-input id="cargo" type="text" class="mt-1 block w-full" wire:model.defer="state.cargo" />
            <x-jet-input-error for="cargo" class="mt-2" />
        </div>

        @if($this->user->rol==0)
        <!-- Estado -->
        <div wire:model.defer="state.status" class="col-span-6 sm:col-span-4">
            <x-jet-label for="status" value="{{ __('Estado') }}" />
            <x-select name="status" id="status" class="mt-1 block" wire:model.defer="state.status">
                <option value="1">Activo</option>
                <option value="0">Inactivo</option>
            </x-select>
            <x-jet-input-error for="status" class="mt-2" />
        </div>

        <!-- Rol -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="rol" value="{{ __('Rol') }}" />
            <x-select name="status" id="status" class="mt-1 block" wire:model.defer="state.rol">
                <option value="0">Administrador</option>
                <option value="1">Usuario</option>
            </x-select>
           
            <x-jet-input-error for="rol" class="mt-2" />
        </div>
        @endif

    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Guardado...') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Guardar') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
