<x-loading></x-loading>
<x-jet-form-section submit="store">
    <x-slot name="title">
        {{ __('Nueva cotización') }}
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
	</x-slot>
	
	<!-- Formulario -->
	<x-slot name="form">
		<!-- Cliente -->
		<div class="col-span-6 sm:col-span-6" wire:ignore>
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			<x-select class="w-full" id="selectCliente" wire:model="cliente_id" name="cliente">
				<option value=""></option>
				@foreach ($clientes as $cliente)
					<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
				@endforeach
			</x-select>
			
			<x-jet-input-error for="cliente" class="mt-2" />
			@if (session()->has('errorCliente'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorCliente') }}
				</div>
			@endif
		</div>  

			<!-- Contacto -->
			<div class="col-span-6 sm:col-span-6 md:col-span-3 lg:col-span-3 xl:col-span-3 gap-4">
				<x-jet-label for="contacto" value="{{ __('Contacto:') }}" />
				<x-select id="contacto" name="contacto"  class="mt-1 block w-full" wire:model.defer="contacto_id" >
					<option value=""></option>
					@foreach ($contactos as $contacto)
						<option value="{{$contacto->id}}">{{ $contacto->nombre }}</option>
					@endforeach
				</x-select>
			</div>
	
			<!-- Validez -->
			<div class="col-span-6 sm:col-span-6 md:col-span-3 lg:col-span-3 xl:col-span-3 gap-4">
				<x-jet-label for="validez" value="{{ __('Validez de la oferta (días)') }}" />
				<x-jet-input id="validez" type="text" class="mt-1 block w-full" wire:model.defer="validez" autocomplete="validez" required onkeyup="formato(this)" onchange="formato(this)"/>
				<x-jet-input-error for="validez" class="mt-2" />
			</div>     	

		<!-- Acciones -->
		<x-slot name="actions">
			<x-jet-action-message class="mr-3" on="saved">
				{{ __('Guardado...') }}
			</x-jet-action-message>
	
			<x-jet-button wire:loading.attr="disabled">
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		
	</x-slot>
</x-jet-form-section>

<script>
	$(document).ready(function() {
    	$('#selectCliente').select2({
			width: '100%',
		});
		
    	$('#selectCliente').on('change', function(){
			@this.set('cliente_id', this.value)
		});
	});
</script>