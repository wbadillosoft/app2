<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ asset('css/pdf.css')}}">
	<link rel="icon" href="{{{ asset('imgs/favicon.ico') }}}">
	<title>Cotización - Proforma {{$cotizacion->codigo}} </title>

</head>
	{{--<style>
		{{$page}}
	</style>--}}
<body>
<div class="body">
<div class="contenedor">
	<div class="" id="header">
		<table cellspacing="0" width="100%">
			<tr>
				<td width="20%">
					<img src="{{$logo}}" class="logo">
				</td>
				<td width="40%" class="text-center text-sm font-sans">
					<b>ESISCO</b>  <p><b>Evaluación de Sistemas de Control<b></p>
					<p>Nit: 900 976 766-6</p>
					<p>Tel: (4) 590 1247 / servicios@esisco.com.co / Medellín</p>
					<p>Operado por CSG S.A.S</p>
				</td>
				<td width="20%">
					<div class="text-center bold text-lg">Cotización / Proforma</div>
				</td>
				<td>
					<div class="consecutivo text-center bold text-lg ">{{$cotizacion->codigo}}</div>
				</td>
			</tr>
		</table>
		
		<div>&nbsp;</div>
		<div class="linea"></div>
	</div>
	<div id="footer">
		<span style="font-size:7pt; text-align:center ">Esisco  / Medellín / servicios@esisco.com.co / (4) 5901247</span>
	</div>
	<div id="contenido">
		<table class="text-sm" width="100%">
			<tbody>
				<tr>
					<th width="15%">CLIENTE:</th>
					<td width="50%">{{$cotizacion->cliente->nombre}}</td>
					<th width="20%">FECHA:</th>
					<td>{{$cotizacion->fecha}}</td>
				</tr>
				<tr>
					<th>NIT:</th>
					<td>{{$cotizacion->cliente->nit}}</td>
					<th>VALIDEZ:</th>
					<td>{{$cotizacion->validez}} días</td>					
				</tr>
				<tr>
					<th>TELÉFONO:</th>
					<td>{{$cotizacion->cliente->telefono}}</td>
					<th>CONTACTO:</th>
					<td>@if($contacto!==NULL) {{$contacto->nombre}} @endif </td>					
				</tr>
				<tr>
					<th>DIRECCIÓN:</th>
					<td>{{$cotizacion->cliente->direccion}}</td>
					<th>EMAIL:</th>
					<td>@if($contacto!==NULL) {{$contacto->email}} @endif</td>
				</tr>
				<tr>
					<th>CIUDAD:</th>
					<td>{{$cotizacion->cliente->ciudad}}</td>
					
				</tr>
				
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
						<div class="linea"></div>
						<br>
					</td>
				</tr>
			</tfoot>
		</table>
		<br>


        <table cellspacing="0" cellpadding="3" border="1" width="100%" class="text-xs">
            <tr>
				<th class="th w-4">Item</th>
                <th class="th w-7">Código</th>
                {{--<th class="th w-10">Referencia</th>--}}
                <th class="th w-50">Descripción</th>
                <th class="th w-12">Precio unit.</th>
                <th class="th w-7">Cant.</th>
                <th class="th w-7">%Desc.</th>
                <th class="th w-7">% IVA</th>
                <th class="th w-12">Subtotal</th>
			</tr>
			<?php 
			foreach($cotizacion->detalles as $k => $detalle)
			{
				$producto   = $detalle->producto;
				$precio     = number_format($producto->precio,0,',', '.');
				$subtotal   = number_format($detalle->total,0,',', '.');
			?>
				<tr>
					<td>{{$k + 1}}</td>
					<td>{{$producto->codigo}}</td>
					{{--<td>{{$producto->referencia}}</td>--}}
					<td><strong>{{$producto->nombre}}</strong>
						<br/><span style="font-size:6pt" >{{$producto->descripcion}} @if($detalle->tiempo>0) - Tiempo de entrega: {{$detalle->tiempo}} días @endif</span></td>
						{{--<br/><span style="font-size:6pt" >Marca:{{$producto->marca}} --- Presentación:{{$producto->presentacion}} @if($detalle->tiempo>0)/ Tiempo de entrega: {{$detalle->tiempo}} días @endif</span></td>--}}
					<td class="text-derecha">$ {{$precio}} </td>
					<td class="text-centro">{{$detalle->cantidad}}</td>
					<td class="text-derecha">{{$detalle->porc_descuento}} %</td>
					<td class="text-derecha">{{$detalle->iva}} %</td>
					<td class="text-derecha">$ {{$subtotal}}</td>
				   
				</tr>
			<?php
			}
			?>
		</table>
		<br>
		<?php 
		$cotizacion_subtotal= number_format($cotizacion->subtotal,0,',', '.');
        $cotizacion_iva     = number_format($cotizacion->iva,0,',', '.');
		$cotizacion_total   = number_format($cotizacion->total,0,',', '.');
		?>

		<table cellpadding="4" width="100%" class="text-xs" cellspacing="0">
			<tr>
				<td class="w-72" rowspan="3" ><b class="text-sm">CONDICIONES COMERCIALES:</b>{{ $cotizacion->condiciones}} </td>
				<th class="th w-14 text-derecha border">Subtotal:</th>
				<td class="w-14 border" >$ {{$cotizacion_subtotal}} </td>
			</tr>
			<tr>
				<th class="th text-derecha border">IVA:</th>
				<td class="border">$ {{$cotizacion_iva}} </td>
			</tr>
			<tr>
				<th class="th text-derecha border">Total:</th>
				<td class="border">$ {{$cotizacion_total}} </td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td class="text-sm"><b>OBSERVACIONES:</b></td>
			</tr>
			<tr>
				<td class="text-sm">{{ $cotizacion->observaciones}}</td>
			</tr>
			<tr>
				<td class="text-sm">
					<b>NOTAS:</b>
					<span  class="text-xs">
					<br>*** PAGO: TRANSFERENCIA ELECTRÓNICA Y/O CONSIGNACIÓN NACIONAL. (Cuenta de ahorros Bancolombia 01268316147 a nombre de CSG S.A.S)
					<br>*** EN CASO DE CONSIGNACIÓN CON CHEQUE, DEBE ESPERAR AL CANJE DE ESTE.
					<br>*** LA VIGENCIA DE PRECIOS SE MANTIENE POR EL TIEMPO ESTIPULADO EN LA VALIDEZ DE ESTA OFERTA.
					<br>*** CUALQUIER INQUIETUD SOBRE LA PRESENTE OFERTA SERÁ ATENDIDA POR SU ASESOR COMERCIAL.
				</span>
					<br>
					<br>
					
				</td>
			</tr>
			<tr>
				<td class="text">
					<b>Cordialmente: </b><br>
					<span class="text-sm"> 
						<p>{{$cotizacion->usuario->name}}</p>
						<p>{{$cotizacion->usuario->cargo}}</p>
						<p>{{$cotizacion->usuario->telefono}}</p>
						<p>{{$cotizacion->usuario->email}}</p>
					</span>
				</td>
			</tr>
		</table>
	</div>
</div>
</div>
</body>
</html>