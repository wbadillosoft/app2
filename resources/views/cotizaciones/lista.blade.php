<x-loading></x-loading>
<div class="col-span-9 sm:col-span-8 mb-1 px-2" >
	<x-jet-button name="nueva_cotizacion" id="nueva_cotizacion" class="mt-1 inline-block mr-4 sm:w-auto w-full h-10 " wire:click="formCrear">
		Nueva cotización
	</x-jet-button>
	{{--<x-jet-input type="text" wire:model.refer="search" class="mt-1 inline-block sm:w-auto w-full h-10" placeholder="Buscar cotización..." name="search" />
	
	<span class="flex-auto pl-4">
		{{ $cotizaciones->links() }}
	</span>--}}
</div>

<div class="p-4 bg-white overflow-x-auto shadow-xl sm:rounded-lg">
	<table class="table-auto" style="width:100%" id="myTable" data-page-length='25'>
		<thead class="table-row-group">
			<tr class="table-row h-7">
			<th class="h-7 lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Cotización</th>
			<th class="h-7 hidden lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Fecha</th>
			<th class="h-7 hidden lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Cliente</th>
			<th class="h-7 hidden lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">SubTotal</th>
			<th class="h-7 hidden lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">IVA</th>
			<th class="h-7 hidden lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Total</th>
			<th class="h-7 lg:w-auto w-14 lg:table-cell px-6 py-4 bg-gray-100 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider"></th>
		  </tr>
		</thead>

		<tbody class="bg-white divide-y divide-gray-200 table-row-group">
			@foreach ($cotizaciones as $cotizacion)   
			<tr class="table-row">
				<td class="table-cell px-1 md:px-4 py-2 whitespace-no-wrap border-bottom"> 
					<div class="text-xs lg:text-sm leading-5 text-gray-500">{{$cotizacion->codigo}} <span class="lg:hidden inline "> / {{$cotizacion->fecha}}</span></div>
					{{-- SOLO EN PANTALLAS PEQUENAS--}}
					<div class="lg:hidden block font-thin">
						<div class="text-xs text-gray-700 font-bold">{{ $cotizacion->cliente->nombre}}</div>
						<div class="text-xs text-gray-500">Subtotal: ${{ number_format($cotizacion->subtotal,0,',','.') }}</div>
						<div class="text-xs text-gray-500">IVA:${{ number_format($cotizacion->iva,0,',','.') }}</div>
						<div class="text-xs text-gray-500">Total: ${{ number_format($cotizacion->total,0,',','.') }}</div>
					</div>
				</td>
				<td class="hidden lg:table-cell table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-xs leading-5 text-gray-500">	
						{{$cotizacion->fecha}}
						<div class=" text-gray-300">
							{{$cotizacion->usuario->name}}
						</div></div>
				</td>
				<td class="hidden lg:table-cell table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						{{ $cotizacion->cliente->nombre}}
					</div>
				</td>
				<td class="hidden lg:table-cell table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($cotizacion->subtotal,0,',','.') }}
					</div>
				</td>
				<td class="hidden lg:table-cell table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($cotizacion->iva,0,',','.') }}
					</div>
				</td>
				<td class="hidden lg:table-cell table-cell px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($cotizacion->total,0,',','.') }}
					</div>
				</td>
			
				<td class="lg:table-cell table-cell px-4 py-2 whitespace-no-wrap border-bottom text-center">
					
					<div class="leading-0 text-gray-500 inline">
						<a target="_blank" href="/cotizaciones/pdf/{{ $cotizacion->id }}" class="inline-flex items-center w-10 h-10 p-2 rounded-full hover:bg-gray-300 cursor-pointer">
							<svg 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 384 512" 
								fill="rgb(161 161 170)" 
								class="p-1">
								<path d="M88 304H80V256H88C101.3 256 112 266.7 112 280C112 293.3 101.3 304 88 304zM192 256H200C208.8 256 216 263.2 216 272V336C216 344.8 208.8 352 200 352H192V256zM224 0V128C224 145.7 238.3 160 256 160H384V448C384 483.3 355.3 512 320 512H64C28.65 512 0 483.3 0 448V64C0 28.65 28.65 0 64 0H224zM64 224C55.16 224 48 231.2 48 240V368C48 376.8 55.16 384 64 384C72.84 384 80 376.8 80 368V336H88C118.9 336 144 310.9 144 280C144 249.1 118.9 224 88 224H64zM160 368C160 376.8 167.2 384 176 384H200C226.5 384 248 362.5 248 336V272C248 245.5 226.5 224 200 224H176C167.2 224 160 231.2 160 240V368zM288 224C279.2 224 272 231.2 272 240V368C272 376.8 279.2 384 288 384C296.8 384 304 376.8 304 368V320H336C344.8 320 352 312.8 352 304C352 295.2 344.8 288 336 288H304V256H336C344.8 256 352 248.8 352 240C352 231.2 344.8 224 336 224H288zM256 0L384 128H256V0z"/>
							</svg>
						</a>
						<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-300 cursor-pointer" wire:click="formEditar({{ $cotizacion->id }})">
							<svg 
								class="p-1"
								fill="rgb(161 161 170)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
							</svg>
						<div>
					</div>
				</td>
			</tr>
			@endforeach
		
		</tbody>
	  </table>
	  {{--<div class=" m-2">
		{{ $cotizaciones->links() }}
	</div>--}}
</div>

