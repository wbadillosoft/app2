<x-loading></x-loading>
<x-form-section-cotizacion class="md:grid-cols-1 " submit="updateEncabezado" cols="md:col-span-4" x-data="{ edit: false }" >
    <x-slot name="btn">
		<x-jet-button wire:loading.attr="disabled" wire:click="volver">
			{{ __('Volver') }}
		</x-jet-button>
    </x-slot>
    <x-slot name="title">
		<b>{{ __('Cotización # '. $cotizacion->codigo) }} </b>
    </x-slot>
    <x-slot name="fecha">
		<b>{{ __('Fecha de cotizacion :'. $cotizacion->fecha) }} </b>
    </x-slot>
	
	<x-slot name="form">
		<!-- Cliente -->
		<div class="col-span-6 sm:col-span-6" wire:ignore>
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			<x-select class="w-full" id="selectCliente" wire:model="cliente_id" name="cliente">
				<option value=""></option>
				@foreach ($clientes as $cliente)
					<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
				@endforeach
			</x-select>
			
			<x-jet-input-error for="cliente" class="mt-2" />
			@if (session()->has('errorCliente'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorCliente') }}
				</div>
			@endif
		</div>  

		<!-- Contacto -->
		<div class="col-span-6 sm:col-span-6 md:col-span-3 lg:col-span-3 xl:col-span-3 gap-4">
			<x-jet-label for="contacto_id" value="{{ __('Contacto:') }}" />
			<x-select id="contacto_id" name="contacto_id"  class="mt-1 block w-full" wire:model.defer="contacto_id" >
				<option value="0"></option>
				@foreach ($contactos as $contacto)
					<option value="{{$contacto->id}}">{{ $contacto->nombre }}</option>
				@endforeach
			</x-select>
		</div>
		<!-- Validez -->
		<div class="col-span-6 sm:col-span-6 md:col-span-3 lg:col-span-3 xl:col-span-3 gap-4">
			<x-jet-label for="validez" value="{{ __('Validez de la oferta (días)') }}" />
			<x-jet-input id="validez" type="text" class="mt-1 block w-full" wire:model.defer="validez" autocomplete="validez" required onkeyup="formato(this)" onchange="formato(this)"/>
			<x-jet-input-error for="validez" class="mt-2" />
		</div> 
		<!-- Nit -->
		<div class="col-span-2 gap-4">
			<x-jet-label for="nit" value="{{ __('Nit:') }}" />
			<span class="text-xs"> {{ $cliente_data['nit'] }} </span>
		</div>

		<!-- Teléfono -->
		<div class="col-span-2 gap-4">
			<x-jet-label for="telefono" value="{{ __('Teléfono:') }}" />
			<span class="text-xs"> {{ $cliente_data['telefono'] }} </span>
		</div>

		<!-- Ciudad -->
		<div class="col-span-2 gap-4">
			<x-jet-label for="ciudad" value="{{ __('Ciudad:') }}" />
			<span class="text-xs"> {{ $cliente_data['ciudad'] }} </span>
		</div>

		<x-slot name="actions">
			<x-jet-button wire:loading.attr="disabled" on="saved">
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		

	</x-slot>
	
</x-form-section-cotizacion>

<script>
	$(document).ready(function() {
    	$('#selectCliente').select2({
			width: '100%',
		});
		
    	$('#selectCliente').on('change', function(){
			@this.set('cliente_id', this.value)
		});
	});
</script>