<x-form-section class="md:grid-cols-5" submit="updateEncabezado" cols="md:col-span-4" x-data="{ edit: false}" >
    <x-slot name="title">
		Editar orden de compra
		<p class="font-bold">{{ __($orden->codigo) }}</p>
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>			  
		</div>
		
		@if($orden->factura!=="")
			<div class="inline-flex py-5">
				<div class="bg-white flex-1 cursor-pointer" style="width:22px; height:22px">
					@if($orden->estado)
						<i class="las la-check-square text-green-600 la-2x relative" style="top:-5px; left:-5px" wire:click="updateRecibir"></i>
					@else
						<i class="las la-window-close la-2x text-red-500 relative" style="top:-5px; left:-5px" wire:click="updateRecibir"></i>
					
					@endif
				</div>
				<div class="inline flex-1 pl-2 font-bold"> Recibido </div>
			</div>
		@endif
	</x-slot>
	
	<x-slot name="form">	
		<!-- Proveedor -->
		<div class="col-span-6 sm:col-span-6" >
			<div :class="{'block': edit, 'hidden': ! edit}" class="hidden">
				<x-jet-label for="proveedor" value="{{ __('Proveedor') }}" />
				<div class="w-full">
					<x-jet-input class="w-10/12" type="text" wire:model="queryProveedor" id="proveedor" />
					<x-button-light  class="bg-transparent text-red-600 hover:border-red-500 active:bg-white focus:bg-white transform translate-y-2" @click="edit = ! edit" wire:click="$set('queryProveedor', '')">
						<i class="las la-times la-2x mt-1"></i>
					</x-button-light >
				</div>
				@if(!empty($queryProveedor))
					<div class="border rounded overflow-y-auto {{$hselect}} bg-gray-50 absolute z-5 w-1/2">
						<ul>
							@foreach ($proveedores as $proveedor)
								<li class="block px-4 hover:bg-gray-300 cursor-pointer" wire:click="seleccionarProveedor({{$proveedor->id}})" @click="edit = ! edit" >
									<span class="text-sm text-gray-900">{{ $proveedor->nombre}}</span>
								</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>

			<!-- Proveedor nombre -->
			<div :class="{'block': ! edit, 'hidden': edit}" class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 pt-4 pb-0">
				<x-jet-label for="nit" value="{{ __('Proveedor:') }}" />
				<span class="font-bold"> {{ $proveedor_data['nombre'] }} </span>
				<x-button-light type="button" class="bg-transparent text-teal-600 hover:border-teal-500 col-span-1 mx-1 w-10 px-2" @click="edit = ! edit">
					<i class="las la-pencil-alt" style="font-size: 24px"></i>
				</x-button>
			</div>

			@if (session()->has('errorProveedor'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorProveedor') }}
				</div>
			@endif
		</div>  

		<!-- Nit -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="nit" value="{{ __('Nit:') }}" />
			<span class="text-xs"> {{ $proveedor_data['nit'] }} </span>
		</div>

		<!-- Teléfono -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="telefono" value="{{ __('Teléfono:') }}" />
			<span class="text-xs"> {{ $proveedor_data['telefono'] }} </span>
		</div>

		<!-- Ciudad -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="ciudad" value="{{ __('Ciudad:') }}" />
			<span class="text-xs"> {{ $proveedor_data['ciudad'] }} </span>
		</div>

		<!-- Aprobó -->
		<div class="col-span-2 sm:col-span-2 md:col-span- lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="aprobo" value="{{ __('Aprobado por') }}" />
			<x-select id="aprobo" class="mt-1 block w-full" wire:model.defer="aprobo" required >
				@foreach ($usuarios as $usuario)
					<option value="{{$usuario->id}}">{{$usuario->name}}</option>
				@endforeach
			</x-jet-select>
			<x-jet-input-error for="aprobo" class="mt-2" />
		</div>  

		<!-- Forma de pago -->
		<div class="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="forma_pago" value="{{ __('Forma de pago') }}" />
			<x-jet-input id="forma_pago" type="text" class="mt-1 block w-full" wire:model.defer="forma_pago" autocomplete="forma_pago" required />
			<x-jet-input-error for="forma_pago" class="mt-2" />
		</div> 
		<!-- Cotización -->
		<div class="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="cotizacion" value="{{ __('# Cotización (si aplica)') }}" class="text-red-500"/>
			<x-jet-input id="cotizacion" type="text" class="mt-1 block w-full" wire:model.defer="cotizacion" />
			<x-jet-input-error for="cotizacion" class="mt-2" />
		</div> 

		<!-- Acciones -->
		<x-slot name="actions">
			@if (session()->has('mensaje'))
				<div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('mensaje') }}
				</div>
			@endif

			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		

	</x-slot>

</x-form-section>

