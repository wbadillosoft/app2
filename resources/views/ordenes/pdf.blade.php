<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ asset('css/pdf.css')}}">
	<title>Orden de compra - {{$orden->codigo}}</title>
</head>
<body>
	<div class="" id="header">
		<table cellspacing="0" width="100%">
			<tr>
				<td width="20%">
					<img src="{{$logo}}" class="logo">
				</td>
				<td width="40%" class="text-center text-xs font-sans">
					<b>DISTRIBUCIONES MBL<b> <p><b>Consultorías en Sistemas de Gestión S.A.S</b></p>
					<p>Nit: 900 976 766-6</p>
					<p>Tel: 300 4525982 </p>
					<p>distribuciones.mbl@gmail.com</p>
					<p>Medellín</p>
				</td>
				<td width="25%">
					<h3 class="text-lg">Orden de compra #</h3>
					<h3>&nbsp;</h3>
				</td>
				<td>
					<div class="consecutivo  text-center bold text-lg">{{$orden->codigo}}</div>
					<p class="pageNumber text-center">Página </p>
				</td>
			</tr>
		</table>
		<div class="linea"></div>
	</div>
	<div id="footer">
		<span style="font-size:7pt; text-align:center ">Distribuciones MBL / Medellín / distribuciones.mbl@gmail.com / (4) 5901247</span>
	</div>
	<div id="contenido">
		<table class="text-sm" width="100%">
			<tbody>
				<tr>
					<th width="15%">PROVEEDOR:</th>
					<td width="35%">{{$orden->proveedor->nombre}}</td>
					<th width="20%">FECHA:</th>
					<td width="30%">{{$orden->fecha}}</td>
				</tr>
				<tr>
					<th>NIT:</th>
					<td>{{$orden->proveedor->nit}}</td>
					<th>FORMA DE PAGO:</th>
					<td>{{$orden->forma_pago}}</td>					
				</tr>
				<tr>
					<th>TELÉFONO:</th>
					<td>{{$orden->proveedor->telefono}}</td>
					<th>APROBADO POR:</th>
					<td>{{$aprobo->name}}</td>					
				</tr>
				<tr>
					<th>DIRECCIÓN:</th>
					<td>{{$orden->proveedor->direccion}}</td>
					<th>COTIZACIÓN:</th>
					<td>{{$orden->cotizacion}}</td>		
				</tr>
				<tr>
					<th>CIUDAD:</th>
					<td>{{$orden->proveedor->ciudad}}</td>
					<th>DIRECCIÓN DESPACHO:</th>
					<td>Calle 49AA#77C-01</td>
				</tr>
				<tr>
					<th>EMAIL:</th>
					<td>{{$orden->proveedor->email}}</td>
					<th>EMAIL:</th>
					<td>distribuciones.mbl@gmail.com</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
						<div class="linea"></div>
						<br>
					</td>
				</tr>
			</tfoot>
		</table>
		<br>


        <table cellspacing="0" cellpadding="3" border="1" width="100%" class="text-xs">
            <tr>
                <th class="th w-14">Referencia</th>
                <th class="th w-40">Descripción</th>
                <th class="th w-12">Precio unit.</th>
                <th class="th w-7">Cant.</th>
                <th class="th w-7">%Desc.</th>
                <th class="th w-7">% IVA</th>
                <th class="th w-12">Subtotal</th>
			</tr>
			<?php 
			foreach($orden->detalles as $detalle)
			{
				$producto   = $detalle->producto;
				$precio     = number_format($detalle->costo,0,',', '.');
				$subtotal   = number_format($detalle->total,0,',', '.');
			?>
				<tr>
					<td>{{$producto->referencia}}</td>
					<td>{{$producto->nombre}}
						<br/><span style="font-size:6pt" >Marca:{{$producto->marca}} --- Presentación:{{$producto->presentacion}} </span>
					</td>
					<td class="text-derecha">$ {{$precio}} </td>
					<td class="text-centro">{{$detalle->cantidad}}</td>
					<td class="text-derecha">{{$detalle->porc_descuento}} %</td>
					<td class="text-derecha">{{$detalle->iva}} %</td>
					<td class="text-derecha">$ {{$subtotal}}</td>
				   
				</tr>
			<?php
			}
			?>
		</table>
		<br>
		<?php 
		$orden_subtotal= number_format($orden->subtotal,0,',', '.');
        $orden_iva     = number_format($orden->iva,0,',', '.');
		$orden_total   = number_format($orden->total,0,',', '.');
		?>

		<table cellpadding="4" width="100%" class="text-sm" cellspacing="0">
			<tr>
				<td class="w-72" rowspan="3" ><b>Observaciones:</b>{{ $orden->observaciones}} </td>
				<th class="th w-14 text-derecha border">Subtotal:</th>
				<td class="w-14 border" >$ {{$orden_subtotal}} </td>
			</tr>
			<tr>
				<th class="th text-derecha border">IVA:</th>
				<td class="border">$ {{$orden_iva}} </td>
			</tr>
			<tr>
				<th class="th text-derecha border">Total:</th>
				<td class="border">$ {{$orden_total}} </td>
			</tr>
		</table>
	</div>
	
</body>
</html>