<x-jet-form-section submit="store">
    <x-slot name="title">
        {{ __('Nueva orden de compra') }}
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
	</x-slot>
	
	<!-- Formulario -->
	<x-slot name="form">
		<!-- Proveedor -->
		<div class="col-span-6 sm:col-span-6" >
			<x-jet-label for="proveedor" value="{{ __('Proveedor') }}" />
			<x-jet-input class="w-full" type="text" wire:model="queryProveedor" id="proveedor" />
			@error('proveedor_id')
				<p class="text-red-700 bg-red-200 w-full border border-red-600 rounded-md px-2" >{{$message}}</p>
			@enderror
			@if(!empty($queryProveedor))
				<div class="border rounded overflow-y-auto h-32 bg-gray-50 absolute z-5 w-1/2">
					<ul>
						@foreach ($proveedores as $proveedor)
							<li class="block px-4 hover:bg-gray-300 cursor-pointer" wire:click="seleccionarProveedor({{$proveedor->id}})">
								<span class="text-sm text-gray-900">{{ $proveedor->nombre}}</span>
							</li>
						@endforeach
					</ul>
				</div>
			@endif

			<!-- proveedor nombre -->
			<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 pt-4 pb-0">
				<x-jet-label for="nit" value="{{ __('Proveedor:') }}" />
				<span class="font-bold"> {{ $proveedor_data['nombre'] }} </span>
			</div>
		</div>
		
		<!-- proveedor -->
		<div class="col-span-6 sm:col-span-6" >
			
			@if (session()->has('errorproveedor'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorproveedor') }}
				</div>
			@endif
		</div>  

		<!-- Nit -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="nit" value="{{ __('Nit:') }}" />
			<span class="text-xs"> {{ $proveedor_data['nit'] }} </span>
		</div>

		<!-- Teléfono -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="telefono" value="{{ __('Teléfono:') }}" />
			<span class="text-xs"> {{ $proveedor_data['telefono'] }} </span>
		</div>

		<!-- Ciudad -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="ciudad" value="{{ __('Ciudad:') }}" />
			<span class="text-xs"> {{ $proveedor_data['ciudad'] }} </span>
		</div>

		<!-- Aprobó -->
		<div class="col-span-2 sm:col-span-2 md:col-span- lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="aprobo" value="{{ __('Aprobado por') }}" />
			<x-select id="aprobo" class="mt-1 block w-full" wire:model.defer="aprobo" required >
				@foreach ($usuarios as $usuario)
					<option value="{{$usuario->id}}">{{$usuario->name}}</option>
				@endforeach
			</x-jet-select>
			<x-jet-input-error for="aprobo" class="mt-2" />
		</div>  

		<!-- Forma de pago -->
		<div class="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="forma_pago" value="{{ __('Forma de pago') }}" />
			<x-jet-input id="forma_pago" type="text" class="mt-1 block w-full" wire:model.defer="forma_pago" autocomplete="forma_pago" required />
			<x-jet-input-error for="forma_pago" class="mt-2" />
		</div> 
		<!-- Cotización -->
		<div class="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-2">
			<x-jet-label for="cotizacion" value="{{ __('# Cotización (si aplica)') }}" class="text-red-500"/>
			<x-jet-input id="cotizacion" type="text" class="mt-1 block w-full" wire:model.defer="cotizacion" />
			<x-jet-input-error for="cotizacion" class="mt-2" />
		</div>   

		<!-- Acciones -->
		<x-slot name="actions">
			<x-jet-action-message class="mr-3" on="saved">
				{{ __('Guardado...') }}
			</x-jet-action-message>
	
			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		
	</x-slot>

</x-jet-form-section>
