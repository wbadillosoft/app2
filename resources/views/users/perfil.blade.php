<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('Información de perfil') }}
    </x-slot>

    <x-slot name="description">
        {{ __('') }}
    </x-slot>

    <x-slot name="form">
     
        <!-- Nombre -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="name" value="{{ __('Nombre') }}" />
            <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model.defer="name" autocomplete="name" required/>
            <x-jet-input-error for="name" class="mt-2" />
        </div>

        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email" value="{{ __('Email') }}" />
            <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="email" required />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
        

        <!-- Email para cotiaciones -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email_cotizacion" value="{{ __('Email para cotizaciones') }}" />
            <x-jet-input id="email_cotizacion" type="email_cotizacion" class="mt-1 block w-full" wire:model.defer="email_cotizacion" required />
            <x-jet-input-error for="email_cotizacion" class="mt-2" />
        </div>
        
        <!-- Cargo -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="cargo" value="{{ __('Cargo') }}" />
            <x-jet-input id="cargo" type="text" class="mt-1 block w-full" wire:model.defer="cargo" />
            <x-jet-input-error for="cargo" class="mt-2" />
        </div>

       
        <!-- Estado -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="status" value="{{ __('Estado') }}" />
            <x-select name="status" id="status" class="mt-1 block" wire:model.defer="status">
                <option value="1">Activo</option>
                <option value="0">Inactivo</option>
            </x-select>
            <x-jet-input-error for="status" class="mt-2" />
        </div>

        <!-- Rol -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="rol" value="{{ __('Rol') }}" />
            <x-select name="status" id="status" class="mt-1 block" wire:model.defer="rol">
                <option value="0">Administrador</option>
                <option value="1">Usuario</option>
            </x-select>
           
            <x-jet-input-error for="rol" class="mt-2" />
        </div> 
   </x-slot>   

    <x-slot name="actions">
        <div>
            @if (session()->has('mensaje-data'))
                <div class="text-success text-bold mr-4 pr-4" id="message" >
                    {{ session('mensaje-data') }}
                </div>
            @endif
        </div>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Guardar') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
