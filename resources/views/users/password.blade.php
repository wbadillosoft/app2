<x-jet-form-section submit="updatePass">
    <x-slot name="title">
        {{ __('Actualización de contraseña') }}
    </x-slot>

    <x-slot name="description">
        {{ __('La contraseña deba tener al menos 8 caracteres') }}
    </x-slot>

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="password" value="{{ __('Contraseña') }}" />
            <x-jet-input id="password" type="text" class="mt-1 block w-full" wire:model.defer="password" autocomplete="current-password" />
            <x-jet-input-error for="password" class="mt-2" />
        </div>

    </x-slot>

    <x-slot name="actions">
      
        <div>
            @if (session()->has('mensaje-pass'))
                <div class="text-success text-bold mr-3"  id="alert-pass">
                    {{ session('mensaje-pass') }}
                </div>
            @endif
        </div>
    

        <x-jet-button wire:loading.attr="disabled" wire:target="photo" >
            {{ __('Guardar') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
