<x-jet-form-section submit="update">
    <x-slot name="title">
        {{ __('Editar producto') }}
    </x-slot>

    <x-slot name="description">
		{{ $descripcion }}
		<div class="text-xl">
			<h2>
				Código: {{ $codigo }}
			</h2>
		</div>
		<div class="py-5">
			<x-jet-nav-link href="{{ route('productos') }}" wire:loading.attr="disabled" >
				<x-jet-button type="buton">
					{{ __('Volver a la lista') }}
				</x-jet-button>
			</x-jet-nav-link>
			{{--<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Ver lista') }}
			</x-jet-button>--}}
		</div>
	</x-slot>
	

	
	<x-slot name="form">
	{{--	<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="codigo" value="{{ __('Código') }}" />
			<x-jet-input id="codigo" type="text" class="mt-1 block w-full" wire:model.defer="codigo" autocomplete="codigo" />
			<x-jet-input-error for="codigo" class="mt-2" />
		</div>                    
	--}}
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nombre" value="{{ __('Producto') }}" />
			<x-jet-input id="nombre" type="text" class="mt-1 block w-full" wire:model.defer="nombre" autocomplete="nombre" />
			<x-jet-input-error for="nombre" class="mt-2" />
		</div>                    

		<div class="col-span-6 sm:col-span-6">
		{{--<div class="col-span-3 sm:col-span-3"> {{-- ESISCO --}}
			<x-jet-label for="marca" value="{{ __('Marca') }}" />
			<x-jet-input id="marca" type="text" class="mt-1 block w-full" wire:model.defer="marca" autocomplete="marca" />
			<x-jet-input-error for="marca" class="mt-2" />
		</div>  

		{{-- MBL / Comentar este input para ESISOO --}}
		<x-jet-input id="categoria" type="hidden" class="mt-1 block w-full" wire:model="categoria" autocomplete="referencia" value="0"/>

		{{-- ESISCO / Comentar este div para MBL --}}
		{{--<div class="col-span-3 sm:col-span-3"> 
			<x-jet-label for="categoria" value="{{ __('Categoría') }}" />		
			<x-select class="w-full" wire:model.defer="categoria" >
				<option value="Servicio metrología">Servicio metrología</option>
				<option value="Servicio mantenimiento">Servicio mantenimiento</option>
				<option value="Equipos/Productos">Equipos/Productos</option>
				<option value="Consumibles/Repuestos">Consumibles/Repuestos</option>
				<option value="Otros">Otros</option>
			</x-select>
			<x-jet-input-error for="categoria" class="mt-2" />
		</div>  --}}

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="referencia" value="{{ __('Referencia') }}" />
			<x-jet-input id="referencia" type="text" class="mt-1 block w-full" wire:model.defer="referencia" autocomplete="referencia" />
			<x-jet-input-error for="referencia" class="mt-2" />
		</div>                    

	{{-- 	<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="presentacion" value="{{ __('Presentación') }}" />
			<x-jet-input id="presentacion" type="text" class="mt-1 block w-full" wire:model.defer="presentacion" autocomplete="presentacion" />
			<x-jet-input-error for="presentacion" class="mt-2" />
		</div>  
 --}}
		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="precio" value="{{ __('Precio') }}" />
			<x-jet-input 
				id="precio" 
				required 
				class="mt-1 block w-full" 
				wire:model.defer="precio" 
				autocomplete="precio" 
				onkeyup="formato(this)" 
				onchange="formato(this)" />
			<x-jet-input-error for="precio" class="mt-2" />
		</div>  

		<div class="col-span-3 sm:col-span-3">
			<x-jet-label for="iva" value="{{ __('% IVA') }}" />
			<x-jet-input id="iva" require type="text" class="mt-1 block w-full" wire:model.defer="iva" autocomplete="iva" onkeyup="formato(this)" onchange="formato(this)" />
			<x-jet-input-error for="iva" class="mt-2" />
		</div> 

		{{-- <div class="col-span-2 sm:col-span-2">
			<x-jet-label for="tiempo" value="{{ __('Tiempo de entrega') }}" />
			<x-jet-input id="tiempo" require type="text" class="mt-1 block w-full" wire:model.defer="tiempo" autocomplete="tiempo" onkeyup="formato(this)" onchange="formato(this)"  />
			<x-jet-input-error for="tiempo" class="mt-2" />
		</div> 
 --}}
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="descripcion" value="{{ __('Descripción') }}" />
			<x-textarea id="descripcion" class="mt-1 block w-full" wire:model="descripcion" autocomplete="descripcion" rows="4"/>
			<x-jet-input-error for="descripcion" class="mt-2" />
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="observaciones" value="{{ __('Observaciones') }}" />
			<x-textarea id="observaciones" class="mt-1 block w-full" wire:model="observaciones" autocomplete="observaciones" rows="4"/>
			<x-jet-input-error for="observaciones" class="mt-2" />
		</div>  

		<x-slot name="actions">
			@if (session()->has('mensaje'))
				<div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('mensaje') }}
				</div>
			@endif
			
			<x-jet-button wire:loading.attr="disabled">
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		

	</x-slot>
	
</x-jet-form-section>
