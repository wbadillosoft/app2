<div class="col-span-9 sm:col-span-8 mb-1 flex" >
	<x-jet-button name="nueva_prefactura" id="nueva_prefactura" class="mt-1 mr-4 inline" wire:click="formCrear">
		Nueva prefactura
	</x-jet-button>
	<x-jet-input type="text" wire:model.refer="search" class="mt-1 inline" placeholder="Buscar prefactura..." name="search" />
	<span class="flex-auto pl-4">
		{{ $prefacturas->links() }}
	</span>
	
</div>

<div class="bg-white overflow-x-auto  shadow-xl sm:rounded-lg">
	<table class="table-auto" style="width:100%">
		<thead>
		  <tr>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Prefactura</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Fecha</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Cliente</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">SubTotal</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">IVA</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Total</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider">Estado</th>
			<th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-large text-gray-500 uppercase tracking-wider"></th>
		  </tr>
		</thead>

		<tbody class="bg-white divide-y divide-gray-200">
			@foreach ($prefacturas as $prefactura)
			<tr>
				
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">{{$prefactura->codigo}}</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-xs leading-5 text-gray-500">	
						{{$prefactura->fecha}}
						<div class=" text-gray-200">
							{{$prefactura->usuario->name}}
						</div></div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						{{ $prefactura->cliente->nombre}}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($prefactura->subtotal,0,',','.') }}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($prefactura->iva,0,',','.') }}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						${{ number_format($prefactura->total,0,',','.') }}
					</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">
						{{$prefactura->factura}} @if($prefactura->estado) <i class="las la-check text-green-500"></i> @else<i class="las la-hourglass-half text-red-600 text-lg"></i> @endif
					</div>
				</td>
			
				<td class="px-4 py-2 whitespace-no-wrap border-bottom text-center">
					<div class="leading-0 text-gray-500">
						<a target="_blank" href="/prefacturas/pdf/{{ $prefactura->id }}" class="inline-flex items-center px-1 py-1 bg-white border border-red-800 rounded-md font-semibold text-xs text-red-800 uppercase tracking-widest hover:bg-gray-100 active:bg-red-200 focus:outline-none focus:border-red-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
							{{__('pdf')}}
						</a>
						<x-button class="px-1 py-1 text-xs bg-yellow-600" wire:click="formEditar({{ $prefactura->id }})">Editar</x-jet-button>
					</div>
				</td>
			</tr>
			@endforeach
		
		</tbody>
	  </table>
	  <div class=" m-2">
		{{ $prefacturas->links() }}
	</div>
</div>
