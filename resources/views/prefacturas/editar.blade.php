<x-form-section class="md:grid-cols-5" submit="updateEncabezado" cols="md:col-span-4" x-data="{ edit: false }">
    <x-slot name="title">
		Editar prefactura
		<p class="font-bold">{{ __($prefactura->codigo) }}</p>
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>			  
		</div>
		
		@if($prefactura->factura!=="")
			<div class="inline-flex py-5">
				<div class="bg-white cursor-pointer" style="width:22px; height:22px">
					@if($prefactura->estado)
						<i class="las la-check-square text-green-600 la-2x relative" style="top:-5px; left:-5px" wire:click="updateEnviar"></i>
					@else
						<i class="las la-window-close la-2x text-red-500 relative" style="top:-5px; left:-5px" wire:click="updateEnviar"></i>
					@endif
				</div>
				<div class=" flex-1 pl-2 font-bold">@if($prefactura->estado)Cancelada @else Sin Cancelar @endif</div>
			</div>
		@endif
	</x-slot>
	
	<x-slot name="form">	
		<!-- Cliente -->
		<div class="col-span-6 sm:col-span-6" >
			<div :class="{'block': edit, 'hidden': ! edit}" class="hidden">
				<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
				<div class="w-full">
					<x-jet-input class="w-10/12" type="text" wire:model="queryCliente" id="cliente" />
					<span class="top-0 right-0 bg-transparent text-red-600 hover:border-white active:bg-white focus:bg-white transform translate-y-2" @click="edit = ! edit" wire:click="$set('queryCliente', '')">
						<i class="las la-times la-2x mt-1 text-red-600 "></i>
					</span >
				</div>

								
				@if(!empty($queryCliente))
				<div class="w-10/12 border rounded overflow-y-auto max-h-32 bg-gray-50 relative z-50">
					<ul>
						@foreach ($clientes as $cliente)
						<li class="block px-4 hover:bg-gray-300 cursor-pointer" wire:click="seleccionarCliente({{$cliente->id}})" @click="edit = ! edit" >
							<span class="text-sm text-gray-900">{{ $cliente->nombre}}</span>
						</li>
						@endforeach
					</ul>
				</div>
				@endif

			</div>
		</div>

			<!-- Cliente nombre -->
			<div :class="{'inline': ! edit, 'hidden': edit}" class="col-span-5 sm:col-span-5 md:col-span-5 lg:col-span-5 xl:col-span-5 pt-4 pb-0">
				<x-jet-label for="cliente" value="{{ __('Nombre/razón social:') }}" />
				<span class="font-bold"> {{ $cliente_data['nombre'] }} </span>
				<x-button-light type="button" class="bg-transparent text-teal-600 hover:border-teal-500 col-span-1 mx-1 w-10 px-2" @click="edit = ! edit">
					<i class="las la-pencil-alt" style="font-size: 24px"></i>
				</x-button>
			</div>

			@if (session()->has('errorCliente'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorCliente') }}
				</div>
			@endif
		</div>  
<div class="grid grid-cols-6 gap-4">
		<!-- Nit -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="nit" value="{{ __('Nit:') }}" />
			<span class="text-xs"> {{ $cliente_data['nit'] }} </span>
		</div>

		<!-- Teléfono -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="telefono" value="{{ __('Teléfono:') }}" />
			<span class="text-xs"> {{ $cliente_data['telefono'] }} </span>
		</div>

		<!-- Ciudad -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="ciudad" value="{{ __('Ciudad:') }}" />
			<span class="text-xs"> {{ $cliente_data['ciudad'] }} </span>
		</div>

		<!-- Email para factura -->
		<div class="col-span-3 sm:col-span-3 md:col-span- lg:col-span-3 xl:col-span-3 gap-4">
			<x-jet-label for="email_factura" value="{{ __('Email factura') }}" />
			<x-jet-input id="email_factura" type="text" class="mt-1 block w-full" wire:model.defer="email_factura" autocomplete="email_factura" required />
			<x-jet-input-error for="email_factura" class="mt-2" />
		</div>  

		<!-- Vencimiento -->
		<div class="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="vencimiento" value="{{ __('Fecha de vencimiento') }}" />
			<x-jet-input id="vencimiento" type="date" class="mt-1 block w-full" wire:model.defer="vencimiento" autocomplete="vencimiento" required />
			<x-jet-input-error for="vencimiento" class="mt-2" />
		</div> 
		<!-- Factura -->
		<div class="col-span-1 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1 gap-2">
			<x-jet-label for="factura" value="{{ __('# Factura') }}" class="text-red-500"/>
			<x-jet-input id="factura" type="text" class="mt-1 block w-full" wire:model.defer="factura" />
			<x-jet-input-error for="factura" class="mt-2" />
		</div> 
	</div>
		<!-- Acciones -->
		<x-slot name="actions">
			@if (session()->has('mensaje'))
				<div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('mensaje') }}
				</div>
			@endif

			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		

	</x-slot>
	
</x-form-section>

