<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ asset('css/pdf.css')}}">
	<link rel="icon" href="{{{ asset('imgs/favicon.ico') }}}">
	<title>Prefactura {{$prefactura->codigo}} </title>

</head>
	
<body>
<div class="body">
<div class="contenedor">
	<div class="" id="header">
		<table cellspacing="0" width="100%">
			<tr>
				<td width="20%">
					<img src="{{$logo}}" class="logo">
				</td>
				<td width="40%"></td>
				<td width="20%">
					<h3 class="text-lg">Prefactura #</h3>
					
				</td>
				<td>
					<div class="consecutivo  text-center bold text-lg">{{$prefactura->codigo}}</div>
				</td>
			</tr>
		</table>
		<div>&nbsp;</div>
		
		<div class="linea"></div>
	</div>
	<div id="footer">
		<span style="font-size:7pt; text-align:center ">Firma :_________________________________</span>
	</div>
	<div id="contenido">
		<table class="text-sm" width="100%">
			<tbody>
				<tr>
					<th width="15%">CLIENTE:</th>
					<td width="50%">{{$prefactura->cliente->nombre}}</td>
					<th width="20%">FECHA:</th>
					<td>{{$prefactura->fecha}}</td>
				</tr>
				<tr>
					<th>NIT:</th>
					<td>{{$prefactura->cliente->nit}}</td>
					<th>F. VENCIMEINTO:</th>
					<td>{{$prefactura->vencimiento}}</td>					
				</tr>
				<tr>
					<th>TELÉFONO:</th>
					<td>{{$prefactura->cliente->telefono}}</td>
					<th>ELABORÓ:</th>
					<td>{{$elaboro->name}}</td>					
				</tr>
				<tr>
					<th>DIRECCIÓN:</th>
					<td>{{$prefactura->cliente->direccion}}</td>
					<th>FACTURA ELECT.:</th>
					<td>{{$prefactura->factura}}</td>
				</tr>
				<tr>
					<th>CIUDAD:</th>
					<td>{{$prefactura->cliente->ciudad}}</td>
					
				</tr>
				<tr>
					<th>EMAIL FACT.:</th>
					<td>{{$prefactura->cliente->email_factura}}</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
						<div class="linea"></div>
						<br>
					</td>
				</tr>
			</tfoot>
		</table>
		<br>


        <table cellspacing="0" cellpadding="3" border="1" width="100%" class="text-xs">
            <tr>
                <th class="th w-7">Código</th>
                {{--<th class="th w-10">Referencia</th>--}}
                <th class="th w-50">Descripción</th>
                <th class="th w-12">Precio unit.</th>
                <th class="th w-7">Cant.</th>
                <th class="th w-7">%Desc.</th>
                <th class="th w-7">% IVA</th>
                <th class="th w-12">Subtotal</th>
			</tr>
			<?php 
			foreach($prefactura->detalles as $detalle)
			{
				$producto   = $detalle->producto;
				$precio     = number_format($producto->precio,0,',', '.');
				$subtotal   = number_format($detalle->total,0,',', '.');
			?>
				<tr>
					<td>{{$producto->codigo}}</td>
					{{--<td>{{$producto->referencia}}</td>--}}
					<td>{{$producto->nombre}}</td>
					<td class="text-derecha">$ {{$precio}} </td>
					<td class="text-centro">{{$detalle->cantidad}}</td>
					<td class="text-derecha">{{$detalle->porc_descuento}} %</td>
					<td class="text-derecha">{{$detalle->iva}} %</td>
					<td class="text-derecha">$ {{$subtotal}}</td>
				   
				</tr>
			<?php
			}
			?>
		</table>
		<br>
		<?php 
		$prefactura_subtotal= number_format($prefactura->subtotal,0,',', '.');
        $prefactura_iva     = number_format($prefactura->iva,0,',', '.');
		$prefactura_total   = number_format($prefactura->total,0,',', '.');
		?>

		<table cellpadding="4" width="100%" class="text-xs" cellspacing="0">
			<tr>
				<td class="w-72" rowspan="3" ><b>Observaciones:</b>{{ $prefactura->observaciones}} </td>
				<th class="th w-14 text-derecha border">Subtotal:</th>
				<td class="w-14 border" >$ {{$prefactura_subtotal}} </td>
			</tr>
			<tr>
				<th class="th text-derecha border">IVA:</th>
				<td class="border">$ {{$prefactura_iva}} </td>
			</tr>
			<tr>
				<th class="th text-derecha border">Total:</th>
				<td class="border">$ {{$prefactura_total}} </td>
			</tr>
		</table>
	</div>
</div>
</div>
</body>
</html>