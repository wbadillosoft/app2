<x-jet-form-section submit="store">
    <x-slot name="title">
        {{ __('Nueva prefactura') }}
    </x-slot>

    <x-slot name="description">
		<div class="py-5">
			<x-jet-button wire:loading.attr="disabled" wire:click="volver">
				{{ __('Volver') }}
			</x-jet-button>
		</div>
	</x-slot>
	
	<!-- Formulario -->

	<x-slot name="form">
		<!-- Cliente -->

		<div class="col-span-6 sm:col-span-6" >
			<x-jet-label for="cliente" value="{{ __('Cliente') }}" />
			<x-jet-input class="w-full" type="text" wire:model="queryCliente" id="cliente" />
			
			@if(!empty($queryCliente))
			<div class="w-full border rounded overflow-y-auto max-h-32 bg-gray-50 relative z-50 col-span-6">
				<ul>
					@foreach ($clientes as $cliente)
					<li class="block px-4 hover:bg-gray-300 cursor-pointer" wire:click="seleccionarCliente({{$cliente->id}})">
						<span class="text-sm text-gray-900">{{ $cliente->nombre}}</span>
					</li>
					@endforeach
				</ul>
			</div>
			@endif

			<!-- Cliente nombre -->
			<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 pt-4 pb-0">
				<x-jet-label for="nit" value="{{ __('Nombre/razón social:') }}" />
				<span class="font-bold"> {{ $cliente_data['nombre'] }} </span>
			</div>
		</div>
		
		<!-- Cliente -->
		<div class="col-span-6 sm:col-span-6" >
			
			@if (session()->has('errorCliente'))
				<div class="text-red-500 text-bold mr-4 pr-4" id="message" style="block" >
					{{ session('errorCliente') }}
				</div>
			@endif
		</div>  

		<!-- Nit -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="nit" value="{{ __('Nit:') }}" />
			<span class="text-xs"> {{ $cliente_data['nit'] }} </span>
		</div>

		<!-- Teléfono -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="telefono" value="{{ __('Teléfono:') }}" />
			<span class="text-xs"> {{ $cliente_data['telefono'] }} </span>
		</div>

		<!-- Ciudad -->
		<div class="col-span-6 sm:col-span-6 md:col-span-2 lg:col-span-2 xl:col-span-2 gap-4">
			<x-jet-label for="ciudad" value="{{ __('Ciudad:') }}" />
			<span class="text-xs"> {{ $cliente_data['ciudad'] }} </span>
		</div>

		<!-- Email para factura -->
		<div class="col-span-6 sm:col-span-6 md:col-span-3 lg:col-span-3 xl:col-span-3 gap-4">
			<x-jet-label for="email_factura" value="{{ __('Email factura') }}" />
			<x-jet-input id="email_factura" type="text" class="mt-1 block w-full" wire:model.defer="email_factura" autocomplete="email_factura" required />
			<x-jet-input-error for="email_factura" class="mt-2" />
		</div>  

		<!-- Vencimiento -->
		<div class="col-span-6 sm:col-span-6 md:col-span-3 lg:col-span-3 xl:col-span-3 gap-4">
			<x-jet-label for="vencimiento" value="{{ __('Fecha de vencimiento') }}" />
			<x-jet-input id="vencimiento" type="date" class="mt-1 block w-full" wire:model.defer="vencimiento" autocomplete="vencimiento" required/>
			<x-jet-input-error for="validez" class="mt-2" />
		</div>      

		<!-- Acciones -->
		<x-slot name="actions">
			<x-jet-action-message class="mr-3" on="saved">
				{{ __('Guardado...') }}
			</x-jet-action-message>
	
			<x-jet-button wire:loading.attr="disabled" >
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		
	</x-slot>

</x-jet-form-section>
