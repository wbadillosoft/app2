<div>
    <x-jet-form-section submit="agregarActividad">
        <x-slot name="title">
            {{ __('Actividades') }}
        </x-slot>
    
        <x-slot name="description">
            {{ __('') }}
        </x-slot>
        
        <x-slot name="form">
            <div class = "col-span-11 md:col-span-11 sm:col-span-11 w-full">
                
                <div class="mt-5 md:mt-0 md:col-span-6">
                    <table class="min-w-full divide-y divide-gray-400 bg-gray-50 " style="width:100%">
                        <tr>
                            <thead>
                                <th class="px-6 py-3 bg-gray-900 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">Actividad</th>
                                <th class="px-6 py-3 bg-gray-900 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider w-32">Fecha</th>
                            </thead>
                        </tr>
                        <tbody>
                            @foreach ($proceso->actividades as $actividad)
                                <tr>
                                    <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom"> 
                                        {{$actividad->actividad}} 
                                        <div class="text-xm text-blue-500 text-xs">
                                            Agreagada por: {{$actividad->usuario}} 
                                        </div>
                                    </td>
                                    <td class="px-3 py-2 whitespace-normal font-light text-xs border-bottom">{{$actividad->fecha}} </td>
                                </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
            <!-- Actividades -->
            <div class="col-span-11 sm:col-span-11 w-full">
                <x-jet-label for="actividad" value="{{ __('Actividades/Observaciones') }}" />
                <x-textarea id="actividad" class="mt-1 block w-full" wire:model.defer="actividad" autocomplete="actividad" rows="4"/>
                <x-jet-input-error for="actividad" class="mt-2" />
                @if (session()->has('erroractividad'))
                    <div class="text-red-600 text-bold mr-4 pr-4" id="error" style="block" >
                        {{ session('erroractividad') }}
                    </div>
                @endif
            </div> 

        </x-slot>
       
        <x-slot name="actions">
            @if (session()->has('mensaje'))
                <div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
                    {{ session('mensaje') }}
                </div>
            @endif
            
            <x-jet-button wire:loading.attr="disabled" class="mx-4 bg-indigo-600 ">
                {{ __('Agregar') }}
            </x-jet-button>

        </x-slot>
    </x-jet-form-section>
</div>