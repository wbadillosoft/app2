<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Usuarios') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-6 sm:px-6 lg:px-8">
            <div class="bg-white shadow-xl sm:rounded-lg">
               
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-0 align-middle inline-block min-w-full sm:px-0 lg:px-0">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200" style="width:100%">
                            <thead>
                               
                            <tr>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Usuario
                                </th>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Cargo
                                </th>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Estado
                                </th>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Rol
                                </th>
                                <th class="px-6 py-3 bg-gray-50"></th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($users as $user)    
                                <tr>
                                    <td class="px-6 py-4 whitespace-no-wrap">
                                        <div class="flex items-center">
                                            <div class="flex-shrink-0 h-10 w-10">
                                            
                                                @if ($user->profile_photo_path==!null)
                                                    <img class="h-10 w-10 rounded-full" src="{{asset('storage/'.$user->profile_photo_path)}}" alt="">
                                                @else
                                                @php
                                                    $nombre = $user->name;

                                                @endphp
                                                    <img class="h-10 w-10 rounded-full object-cover" src="https://ui-avatars.com/api/?name={{$user->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="">
                                                @endif
                                            </div>

                                            <div class="ml-4">
                                                <div class="text-sm leading-5 font-medium text-gray-900">
                                                    {{$user->name}}
                                                </div>
                                                <div class="text-sm leading-5 text-gray-500">
                                                    {{$user->email}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-no-wrap">
                                        <div class="text-sm leading-5 text-gray-500">{{$user->cargo}}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-no-wrap">
                                        <div class="text-sm leading-5 text-gray-500">
                                            @if($user->status == 1)
                                                Activo
                                            @else
                                                Inactivo
                                            @endif
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-no-wrap">
                                        <div class="text-sm leading-5 text-gray-500">
                                            @if($user->rol == 0)
                                                Administrador
                                            @else
                                                Usuario
                                            @endif
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                        @if(Auth::user()->rol == 0)                                       
                                            <x-link-img href="{{url('/user/edit/'.$user->id)}}" class="text-indigo-600 hover:text-indigo-900">
                                                <i class="las la-lg la-pen"></i>
                                            </x-link-img>
                                         @endif  
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
  
              
            </div>
        </div>
    </div>


</div>
