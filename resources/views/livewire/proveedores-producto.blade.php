<div x-data="{ open: false }">
    <x-jet-form-section submit="">
        <x-slot name="title">
            {{ __('Proveedores') }}
        </x-slot>
    
        <x-slot name="description">
            {{ __('') }}
        </x-slot>
       
        <x-slot name="form">
            <div class = "col-span-11 md:col-span-11 sm:col-span-11 w-full" >                
                <div class="mt-5 md:mt-0 md:col-span-6">
                    <table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
                        <tr>
                            <thead>
                                <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Proveedor</th>
                                <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Ciudad</th>
                                <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"></th>
                            </thead>
                        </tr>
                        <tbody>
                        
                            @foreach ($proveedores_producto as $proveedor)
                                <tr>
                                    <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">
                                        <span class="cursor-pointer text-blue-800" wire:click="$set('proveedor_sel', {{$proveedor->proveedor}})" @click="open = true" >{{$proveedor->proveedor->nombre}}</span>
                                    </td>
                                    <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$proveedor->proveedor->ciudad}}</td>
                                    <td class=" whitespace-no-wrap text-sm border-bottom text-right ">
                                        <x-jet-button type="button" class="button bg-red-600 py-1 px-1" wire:click="quitarProveedor({{ $proveedor->id }})">
                                            <i class="las la-lg la-times"></i>
                                        </x-jet-button>
                                    </td>
                                </tr> 
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
            
                {{--<div x-show="open" @click.away="open = false">
                    <x-modal-info titulo="Datos del proveedor"> 
                        <div class="text-sm"  @click.away="open = false">
                            <table>
                                <tr>
                                    <td><b class="text-red-500">Nombre:</b></td>
                                    <td>{{$proveedor_sel['nombre'] }} </td>
                                </tr>
                                <tr>
                                    <td><b class="text-red-500">NIT:</b></td>
                                    <td>{{$proveedor_sel['nit']}} </td>
                                </tr>
                                <tr>
                                    <td><b class="text-red-500">Teléfono:</b></td>
                                    <td>{{$proveedor_sel['telefono']}} </td>
                                </tr>
                                <tr>
                                    <td><b class="text-red-500">Dirección:</b></td>
                                    <td>{{$proveedor_sel['direccion']}} </td>
                                </tr>
                                <tr>
                                    <td><b class="text-red-500">Email:</b></td>
                                    <td>{{$proveedor_sel['email']}} </td>
                                </tr>
                            </table>
                        </div>
                    </x-modal-info>
                </div> --}}
           
            <!-- Proveedores -->
            
            <div class="col-span-11 sm:col-span-11 w-full">
                <x-jet-label for="proveedor" value="{{ __('Proveedor') }}" />
                <x-select name="proveedor" class="mt-1 block w-full" id="proveedor"  wire:model.defer="nuevoProveedor" >
                    <option value='0'></option>
                    @foreach ($proveedores as $proveedor)
                    <option value="{{ $proveedor->id }}">{{ $proveedor->nombre }} - {{ $proveedor->ciudad }}</option>
                    @endforeach    
                </x-select>
                <x-jet-input-error for="proveedor" class="mt-2" />
                @if (session()->has('error'))
                <div class="text-red-600 text-bold mr-4 pr-4" id="error" style="block" >
                    {{ session('error') }}
                </div>
            @endif
            </div>
          
        </x-slot>
       
        <x-slot name="actions">
            @if (session()->has('mensaje'))
                <div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
                    {{ session('mensaje') }}
                </div>
            @endif
            
            <x-jet-button wire:loading.attr="disabled" wire:click="agregarProveedor({{$producto}})" class="mx-4 bg-indigo-600 ">
                {{ __('Agregar') }}
            </x-jet-button>

        </x-slot>
    </x-jet-form-section>
</div>