<div>
    <x-slot name="header">   
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Productos') }}
        </h2> 
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            @if($mostrar_lista)
                <div>
                    @include('productos.lista')
                </div>
            @endif
                
            @if($mostrar_editar)
                <div>
                  
                    @include('productos.editar')
                </div>
                <x-jet-section-border />
                
                <div > 
                    @livewire('proveedores-producto', ['producto' => $producto->id])
                </div>   
                 
                <x-jet-section-border />
                <div >
                    @livewire('costos-producto', ['producto' => $producto])
                </div>       
                   
            @endif

            @if($mostrar_crear)
                <div>
                    @include('productos.crear')
                </div>
            @endif

            @if($mostrar_show)
                <div>
                    @include('productos.show')
                </div>
            @endif
        </div>
    </div>
    @push('js')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"></script>
	<script>
		$(document).ready( function () {
			$('#myTable2').DataTable({
                "language": {
                    "url": "////cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"
                },
                ordering:  false,
            });
		} );
        
        document.addEventListener('DOMContentLoaded', () => {
            Livewire.on('saved', productoId => {
                ToastInfo.fire({
                background:'#059669',
                icon: 'success',
                iconColor: '#FFF',
                color: '#FFF',
                title: 'Los datos han sido guardados',
                })
            })
        })
    </script>
@endpush

</div>
