<div>
    <x-slot name="header">   
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Seguimiento a procesos') }}
        </h2>  
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            @if($mostrar_lista)
                <div>
                    @include('procesos.lista')
                </div>
            @endif
                
            @if($mostrar_editar)
                <div>
                    @include('procesos.editar')
                </div>
                <x-jet-section-border />
                <div>
                    @livewire('actividades', ['proceso' => $proceso])
                </div>    
                <script>
                    $(document).ready(function() {
                        $('.select-cliente').select2({width: 'resolve'});
                        $('.select-cliente').on('change', function(){
                            @this.set('cliente_id', this.value)
                        });
                    });
                </script>
                      
            @endif

            @if($mostrar_crear)
                <div>
                   @include('procesos.crear', [
                        'clientes'=>$clientes,
                    ])
                </div>
                <script>
                    $(document).ready(function() {
                        $('.select-cliente').select2({width: 'resolve'});
                        $('.select-cliente').on('change', function(){
                            @this.set('cliente_id', this.value)
                        });
                    });
                </script>
                
            @endif
            @if($mostrar_show)
                <div>
                    @include('procesos.show')
                    <script>
                        document.addEventListener('livewire:load', () => {
                            $('#select-cliente').select2();
                            alert('loaded');
                        })
                    </script>
                </div>
            @endif            
        </div>
    </div>
    
@push('js')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"></script>
	<script>
		$(document).ready( function () {
			$('#myTable2').DataTable({
                "language": {
                    "url": "////cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"
                },
                ordering:  false,
            });
		} );

        function formato(input)
        {
            var numero = input.value.replace(/\D/g,'');
                numero = numero
                    .replace(/([0-9])([0-9]{3})$/, '$1.$2')  
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".")
                input.value = numero;
        }

       document.addEventListener('DOMContentLoaded', () => {
            Livewire.on('saved', procesoId => {
                ToastInfo.fire({
                background:'#059669',
                icon: 'success',
                iconColor: '#FFF',
                color: '#FFF',
                title: 'Los datos han sido guardados',
                })
            })
        })
    </script>

@endpush

   
</div>