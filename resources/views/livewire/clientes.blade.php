<div>
    <x-slot name="header">
       
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Clientes') }}
            </h2>      
    </x-slot>
 
    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            @if($mostrar_lista)
                <div>
                    @include('clientes.lista')
                </div>
            @endif
                
            @if($mostrar_editar)
                <div>
                    @include('clientes.editar')
                </div>
                <x-jet-section-border />
                <div >
                    @livewire('contactos-cliente', ['cliente' => $cliente])
                </div>                
            @endif

            @if($mostrar_crear)
                <div>
                    @include('clientes.crear')
                </div>
            @endif

            @if($mostrar_show)
                <div>
                    @include('clientes.show')
                </div>
            @endif
        </div>
    </div>
    @push('js')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"></script>
	<script>
		$(document).ready( function () {
			$('#clientesTable').DataTable({
                "language": {
                    "url": "////cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"
                },
                ordering:  false,
            });
		} );
	</script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            Livewire.on('saved', clienteId => {
                ToastInfo.fire({
                background:'#059669',
                icon: 'success',
                iconColor: '#FFF',
                color: '#FFF',
                title: 'Los datos han sido guardados',
                })
            })
            
        });
      </script>
@endpush

</div>
