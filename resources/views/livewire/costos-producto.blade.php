<div>
    <x-jet-form-section submit="">
        <x-slot name="title">
            {{ __('Costos') }}
        </x-slot>
    
        <x-slot name="description">
            {{ __('') }}
        </x-slot>
       
        <x-slot name="form">
            <div class = "col-span-11 md:col-span-11 sm:col-span-11 w-full">
                
                <div class="mt-5 md:mt-0 md:col-span-6">
                    <table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
                        <tr>
                            <thead>
                                <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Costo</th>
                                <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Proveedor</th>
                                <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"></th>
                            </thead>
                        </tr>
                        <tbody>
                            @foreach ($costos as $costo)
                                <tr>
                                    <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">$ {{number_format($costo->costo,0,'','.')}}</td>
                                    <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$costo->proveedor}}</td>
                                    <td class=" whitespace-no-wrap text-sm border-bottom text-right ">
                                        <x-jet-button type="button" class="button bg-red-600 py-1 px-1" wire:click="quitarCosto({{ $costo->id }})">
                                            <i class="las la-lg la-times"></i>
                                        </x-jet-button>
                                    </td>
                                </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
            <!-- Costos -->
            <div class="col-span-4 sm:col-span-3 w-full">
                <x-jet-label for="costo" value="{{ __('Costo') }}" />
                <x-jet-input name="costo" class="mt-1 block w-full" id="Costo"  wire:model.defer="costo" onkeyup="formato(this)"  onchange="formato(this)" />
                <x-jet-input-error for="costo" class="mt-2" />
                @if (session()->has('errorcosto'))
                    <div class="text-red-600 text-bold mr-4 pr-4" id="error" style="block" >
                        {{ session('errorcosto') }}
                    </div>
                @endif
            </div>

            <div class="col-span-6 sm:col-span-6 w-full">
                <x-jet-label for="proveedor" value="{{ __('Proveedor') }}" />
                <x-select name="proveedor" class="mt-1 block w-full" id="proveedor"  wire:model.defer="proveedor" >
                    <option value='0'></option>
                    @foreach ($proveedores as $proveedor)
                    <option value="{{ $proveedor->nombre }}">{{ $proveedor->nombre }} - {{ $proveedor->ciudad }}</option>
                    @endforeach    
                </x-select>
                <x-jet-input-error for="proveedor" class="mt-2" />
                @if (session()->has('errorproveedor'))
                    <div class="text-red-600 text-bold mr-4 pr-4" id="error" style="block" >
                        {{ session('errorproveedor') }}
                    </div>
                @endif
            </div>
          
        </x-slot>
       
        <x-slot name="actions">
            @if (session()->has('mensaje'))
                <div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
                    {{ session('mensaje') }}
                </div>
            @endif
            
            <x-jet-button wire:loading.attr="disabled" wire:click="agregarCosto({{$producto->id}})" class="mx-4 bg-indigo-600 ">
                {{ __('Agregar') }}
            </x-jet-button>

        </x-slot>
    </x-jet-form-section>
</div>