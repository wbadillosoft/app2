<div>
    <x-slot name="header">   
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Ordenes de compra') }}
        </h2>  
    </x-slot>

    <div class="container mx-auto py-6" >
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            @if($mostrar_lista)
                <div>
                    @include('ordenes.lista')
                </div>
            @endif

            @if($mostrar_editar)
                <div>
                    @include('ordenes.editar', [
                        'proveedores'=>$proveedores,
                    ])
                </div>
                <x-jet-section-border />
                <div>
                    @livewire('detalles-orden', ['orden' => $orden])
                </div>       
            @endif

            @if($mostrar_crear)
                <div>
                    @include('ordenes.crear', [
                        'proveedores'=>$proveedores,
                    ])
                </div>
            @endif

            @if($mostrar_show)
                <div>
                    @include('ordenes.show')
                </div>
            @endif             
        </div>
    </div>

    <script>
      
        function formato(input)
        {
            var numero = input.value.replace(/\D/g,'');
                numero = numero
                    .replace(/([0-9])([0-9]{3})$/, '$1.$2')  
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".")
                input.value = numero;
        }
        function numero(input)
        {
            var numero  = input.value.replace(/\D/g,'');
            input.value = numero;
        }

    </script>
</div>