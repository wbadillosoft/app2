<div>
    <x-jet-dialog-modal wire:model="modalProducto">
        <x-slot name="title"> {{ __('Producto') }}  </x-slot>
            
                <x-slot name="content">
                    <div class="min-w-full">
                        <x-jet-input class="w-full" type="text" wire:model="queryProducto" />
                        @if(!empty($queryProducto))
                        <div class="border rounded overflow-y-auto h-32 absolute z-50 bg-white" style="width:93%">
                            <ul>
                                @foreach ($productos as $producto)
                                    <li class="block px-4 hover:bg-gray-300 cursor-pointer" wire:click="seleccionarProducto({{$producto->id}})">
                                        <span class="text-xs text-indigo-700">[{{$producto->codigo}}] </span> 
                                        <span class="text-sm text-gray-900">{{ $producto->nombre}}</span>
                                        <span class="text-xs text-gray-400 italic">[Ref:{{$producto->referencia}}]</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                
                    <div class="py-2">
                        <table class="min-w-full divide-y divide-gray-200 text-sm text-left" style="width:100%">
                            <tr>
                                <th colspan="2" class="py-3">
                                    {{$producto_sel['nombre']}}
                                </th>
                            </tr>
                            <tr>
                            <th class="border border-gray-100 bg-gray-50">Codigo</th>
                            <th class="border border-gray-50 bg-gray-50">Marca</th>
                            </tr>
                            <tr>
                                <td class="border border-gray-50">{{$producto_sel['codigo']}}</td>
                                <td class="border border-gray-50">{{$producto_sel['marca']}}</td>
                            </tr>
                            <tr>
                                <th class="border border-gray-50 bg-gray-50">Presentación</th>
                                <th class="border border-gray-50 bg-gray-50">Precio</th>
                            
                            </tr>
                            <tr>
                                <td class="border border-gray-50">{{$producto_sel['presentacion']}}</td>
                                <td class="border border-gray-50">$ {{ number_format($producto_sel['precio'], 0,',', '.')}}</td>
                            </tr>
                        </table>
                    </div>
                </x-slot>
                <x-slot name="footer">
                    <x-jet-input class="hidden" type="text" wire:model="producto_id" />
                    <x-jet-button type="button" class="bg-red-600 hover:bg-red-800 m-x4 text-center" wire:click="cerrarModal">Cerrar</x-jet-button>
                    @if(!$editarProducto) 
                        <x-jet-button type="submmit" class="bg-blue-600 hover:bg-blue-800" wire:click="agregarProducto">Agregar</x-jet-button>
                    @elseif($editarProducto) 
                        <x-jet-button type="button" class="bg-blue-600 hover:bg-blue-800" wire:click="modificarProducto">Modificar</x-jet-button>
                    @endif
                </x-slot>
        </x-jet-dialog-modal>
    
    <x-form-table class="md:grid-cols-6" submit="updateDetalles" cols="md:col-span-6" id="itemsform">
       
        <x-slot name="form">
            <div class = "col-span-11 md:col-span-11 sm:col-span-11 w-full">
                <div class="mt-5 md:mt-0 md:col-span-6">
                    <table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
                        <tr>
                            <caption class="text-right py-3">
                                <x-jet-button type="button" class="button bg-blue-900 py-1 px-1" wire:click="buscarProductos">
                                Agregar producto <i class="las la-plus-square text-2xl pl-2"></i>
                                </x-jet-button>
                                
                            </caption>
                            <thead>
                                <th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-6">Item #</th>
                                <th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-6">Código</th>
                                {{--<th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-10">Referencia</th>--}}
                                <th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-80">Descripción</th>
                                <th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-5">Cantidad</th>
                                <th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-25">Observaciones</th>
                                <th class="px-2 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider w-6"></th>
                            </thead>
                        </tr>
                        <tbody>
                            @foreach ($remision->detalles as $item)
                            <tr>
                                <td class="px-2 py-1 bg-white text-left text-xs leading-4 text-gray-600 tracking-wider"> {{$loop->index + 1}} </td>
                                <td class="px-2 py-1 bg-white text-left text-xs leading-4 text-gray-600 tracking-wider"> {{$item->producto->codigo}} </td>
                                {{--<td class="px-2 py-1 bg-white text-left text-xs leading-4 text-gray-600 tracking-wider">{{$item->producto->referencia}} </td>--}}
                                <td class="px-2 py-1 bg-white text-left text-xs leading-4 text-gray-600 tracking-wider w-48">
                                    <input  type="text" 
                                        class="mt-1 block w-full form-input rounded-md shadow-sm text-xs px-1" 
                                        wire:focus ="loadItem({{$item->id}})"                                            
                                        wire:cahnge="loadItem({{$item->id}})"
                                        wire:change="setLote($event.target.value, {{$item->id}})"
                                        value="{{ $item->lote }}"
                                    />
                                    {{$item->producto->nombre}}
                                    <p class="text-gray-400 text-xxs">Marca:{{$item->producto->marca}} / Presentación:{{$item->producto->presentacion}} </p>  
                                </td>
                                    <td class="px-2 py-1 bg-white text-left text-xs leading-4 text-gray-600 tracking-wider w-5"> 
                                    <input  type="number" 
                                            class="mt-1 block w-full form-input rounded-md shadow-sm text-xs px-1" 
                                            wire:focus ="loadItem({{$item->id}})"                                            
                                            wire:cahnge="loadItem({{$item->id}})"
                                            wire:change="setCantidad($event.target.value, {{$item->id}})"                                            
                                            value="{{$item->cantidad}}" 
                                    />
                                </td>
                                <td class="px-2 py-1 bg-white text-left text-xs leading-4 text-gray-600 tracking-wider"> 
                                    <input  type="text" 
                                            class="mt-1 block w-full form-input rounded-md shadow-sm text-xs px-1" 
                                            wire:focus ="loadItem({{$item->id}})"                                            
                                            wire:cahnge="loadItem({{$item->id}})"
                                            wire:change="setLote($event.target.value, {{$item->id}})"
                                            value="{{ $item->lote }}"
                                    />
                                </td>

                                <td class="p-l-2 py-1 bg-white text-right text-xs leading-4 ">
                                    <div class="inline-flex ">
                                        <x-button-light type="button" class="bg-transparent text-teal-600 hover:border-teal-500 col-span-1 mx-1 w-10 px-2" wire:click="editarItem({{ $item->id }}, {{ $item->producto->id }})">
                                            <i class="las la-pencil-alt" style="font-size: 24px"></i>
                                        </x-button>
                                       
                                        <x-button-light type="button"  wire:click="confirmarEliminarItem({{$item->id}})" class="bg-transparent hover:border-red-600 text-red-600 col-span-1 mx-1 w-10 px-2">
                                            <i class="las la-times" style="font-size: 24px"></i>
                                        </x-button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr> </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </x-slot>

        <x-slot name="totales">
            <div class="grid grid-cols-3 gap-4 w-full ">
                <div class="col-span-3">
                    <textarea wire:model.defer="observaciones" class="border border-gray-300 px-3 py-2 text-sm focus:outline-none rounded overflow-hidden shadow-lg " style="width:100%" rows="5" value=""></textarea>
                </div>
             
            </div>
           
        </x-slot>
        <x-slot name="observaciones"> </x-slot>
        <x-slot name="actions">
            @if (session()->has('mensaje'))
                <div class="text-success text-bold mr-4 pr-4" id="message" style="block" >
                    {{ session('mensaje') }}
                </div>
            @endif
            <x-link wire:loading.attr="disabled" link="remisiones/pdf/{{$remision->id}}" target="_blank" class="bg-orange-500 hover:bg-orange-400 py-2 px-4 border text-white hover:text-white" >
				<i class="las la-file-pdf text-2xl pr-2"></i> {{ __('Ver pdf') }}
			</x-link>
            <x-jet-button wire:loading.attr="disabled" type="submit" class="mx-4 bg-indigo-600 ">
                {{ __('Guardar') }} <i class="las la-save text-2xl pl-2"></i>
            </x-jet-button>
        </x-slot>
    </x-form-table>
    @if($confirmar)
        <x-modal-confirmacion itemid="{{$item_id}}" producto="{{$producto_eliminar}}"> </x-modal-confirmacion>
    @endif
</div>
