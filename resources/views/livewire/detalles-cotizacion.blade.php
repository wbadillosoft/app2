<div>
    <x-loading></x-loading>

    <x-jet-dialog-modal wire:model="modalProducto" maxWidth="max-w-4xl">
        <x-slot name="title"> {{ __('Producto') }} </x-slot>

        <x-slot name="content">
            
            <!-- Producto -->
            <div class="min-w-full" wire:ignore >
                <x-jet-label for="producto" value="{{ __('Seleccione un producto') }}" />
                <x-select class="w-full" id="selectProducto" wire:model="producto_id" name="producto">
                    <option value=""></option>
                    @foreach ($productos as $producto)
                        <option @if($producto->id === $producto_id) selected="selected" @endif value="{{$producto->id}}">{{$producto->nombre}} (${{number_format($producto->precio,0,',','.')}} )</option>
                    @endforeach
                </x-select>
            </div> 
        </x-slot>
        
        <x-slot name="footer">
            <x-jet-input class="hidden" type="text" wire:model="producto_id" />
            <x-jet-button type="button" class="bg-red-600 hover:bg-red-800 m-x4 text-center" wire:click="cerrarModal">
                Cancelar</x-jet-button>
            @if (!$editarProducto)
                <x-jet-button type="submmit" class="bg-blue-600 hover:bg-blue-800" wire:click="agregarProducto">Agregar
                </x-jet-button>
            @elseif($editarProducto)
                <x-jet-button type="button" class="bg-blue-600 hover:bg-blue-800" wire:click="modificarProducto">
                    Modificar</x-jet-button>
            @endif
        </x-slot>
    </x-jet-dialog-modal>

    <x-form-table class="md:grid-cols-6" submit="updateDetalles" cols="md:col-span-6" id="itemsform">
        <x-slot name="form">
            <div class="col-span-11 md:col-span-11 sm:col-span-11 w-full">
                <div class="mt-5 md:mt-0 md:col-span-6">
                    <div class="table w-full divide-y divide-gray-200 bg-gray-50">
                        <div class="table-caption">
                            <div class="table-row">
                                <div class="table-cell text-left py-3">
                                    <x-jet-button type="button" class="button bg-blue-900 py-1 px-1 w-full"
                                        wire:click="buscarProductos">
                                        Agregar producto <i class="las la-plus-square text-2xl pl-2"></i>
                                    </x-jet-button>
                                </div>
                            </div>
                        </div>
                        <div class="table-header-group">
                            {{-- HEADER QUE SE MUESTRA EN PANTALLAS GRANDES --}}
                            <div class="hidden md:table-row bg-gray-200">
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-6"> Item #</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-6"> Código</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-64"> Descripción</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-20"> Precio Unit.</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-5"> Cantidad</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-5"> % Desc.</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-5"> T. Entrega</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-20"> Total</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-1"> IVA</div>
                                <div class="table-cell px-2 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-6"></div>
                            </div>
                            {{-- HEADER QUE SE MUESTRA EN PANTALLAS PEQUEÑAS --}}
                            <div class="md:hidden table-row bg-gray-200 ">
                                <div class="table-cell px-1 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider w-6">Item</div>
                                <div class="table-cell px-1 py-3 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider"> Descripción</div>
                            </div>
                        </div>
                            <div class="table-row-group">
                                @foreach ($cotizacion->detalles as $item)
                                {{-- DETALLES QUE SE MUESTRAN EN PANTALLAS GRANDES --}}
                                <div class="hidden md:table-row">
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider">{{$loop->index + 1}}</div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider">{{$item->producto->codigo}} </div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider w-48">{{$item->producto->nombre}} </div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider">${{number_format($item->producto->precio,0,',','.')}} </div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider w-5"><input  type="text" 
                                        class="mt-1 block w-full form-input rounded-md shadow-sm text-xs px-1" 
                                        wire:keydown="$set('item_id', {{$item->id}})"
                                        wire:change="setCantidad($event.target.value, {{$item->id}})"                                            
                                        wire:key="{{$item->id}}" 
                                        value="{{$item->cantidad}}" 
                                        onkeyup="numero(this)" 
                                        onchange="numero(this)"
                                        />
                                    </div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider w-5">
                                        <input  type="text" 
                                                class="mt-1 block w-full form-input rounded-md shadow-sm text-xs px-1" 
                                                wire:keydown="$set('item_id', {{$item->id}})"
                                                wire:change="setDescuento($event.target.value)"
                                                value="{{ $item->porc_descuento }}"
                                                onkeyup="numero(this)" 
                                                onchange="numero(this)"
                                        />
                                    </div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider">
                                        <input 
                                                type="text" 
                                                class="mt-1 block w-full form-input rounded-md text-xs shadow-sm" 
                                                value="{{ $item->tiempo}}" 
                                                wire:keydown="$set('item_id', {{$item->id}})"
                                                wire:change="setTiempo($event.target.value)"
                                                onkeyup="numero(this)" 
                                                onchange="numero(this)"
                                        />
                                    </div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider">$ {{ number_format($item->total,0,',','.')}}</div>
                                    <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-700 tracking-wider">{{ $item->producto->iva }}%</div>
                                    <div class="table-cell p-l-2 py-1 bg-white text-right text-xs leading-4 "> 
                                        <div class="inline-flex ">
                                            <x-button-light type="button" class="bg-transparent text-teal-600 hover:border-teal-500 col-span-1 mx-1 w-10 px-2" wire:click="editarItem({{ $item->id }}, {{ $item->producto->id }})">
                                                <i class="las la-pencil-alt" style="font-size: 24px"></i>
                                            </x-button>
                                            <x-button-light type="button" wire:click="$emit('eliminar', {{ $item->id }})" class="bg-transparent hover:border-red-600 text-red-600 col-span-1 mx-1 w-10 px-2">
                                                <i class="las la-times" style="font-size: 24px"></i>
                                            </x-button>
                                        </div>
                                    </div>
                                </div>
                                 {{-- DETALLES QUE SE MUESTRAN EN PANTALLAS PEQUEÑAS --}}
                                <div class="md:hidden table-row">
                                    <div class="table-cell border-gray-600 border-b px-1 py-1 bg-white text-left text-xs leading-4 text-gray-700 align-middle">{{$loop->index + 1}}</div>
                                    <div class="table-cell border-gray-600 border-b ">
                                        <div class="table-cell table-caption px-1 py-1 bg-white text-left text-xs leading-4 text-gray-800 font-bold">{{$item->producto->nombre}} <span style="font-size:x-small"> (${{number_format($item->producto->precio,0,',','.')}}) </span> 
                                          <div class="table-row">
                                            <div class="table-cell text-gray-500 font-thin w-1/3 px-1">Cantidad</div>
                                            <div class="table-cell text-gray-500 font-thin w-1/3 px-1">%Desc.</div>
                                            <div class="table-cell text-gray-500 font-thin w-1/3 px-1">T. Entrega</div>
                                          </div>
                                          <div class="table-row">
                                            <div class="table-cell w-1/3 px-1">
                                                <input type="text" 
                                                class="mt-0 block w-full form-input rounded-md shadow-sm text-xs px-1 py-1" 
                                                wire:keydown="$set('item_id', {{$item->id}})"
                                                wire:change="setCantidad($event.target.value, {{$item->id}})"                                            
                                                wire:key="{{$item->id}}" 
                                                value="{{$item->cantidad}}" 
                                                onkeyup="numero(this)" 
                                                onchange="numero(this)"
                                                />
                                            </div>
                                            <div class="table-cell w-1/3 px-1">
                                                <input type="text" 
                                                    class="mt-0 block w-full form-input rounded-md shadow-sm text-xs px-1 py-1" 
                                                    wire:keydown="$set('item_id', {{$item->id}})"
                                                    wire:change="setDescuento($event.target.value)"
                                                    value="{{ $item->porc_descuento }}"
                                                    onkeyup="numero(this)" 
                                                    onchange="numero(this)"
                                                />
                                            </div>
                                            <div class="table-cell w-1/3 px-1">
                                                <input type="text" 
                                                    class="mt-0 block w-full form-input rounded-md shadow-sm text-xs px-1 py-1" 
                                                    wire:keydown="$set('item_id', {{$item->id}})"
                                                    wire:change="setDescuento($event.target.value)"
                                                    value="{{ $item->porc_descuento }}"
                                                    onkeyup="numero(this)" 
                                                    onchange="numero(this)"
                                                />
                                            </div>
                                          </div>
                                          <div class="table-row">
                                            <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-800 tracking-wider">${{ number_format($item->total,0,',','.')}}</div>
                                            <div class="table-cell px-2 py-1 bg-white text-left text-xs leading-4 text-gray-800 tracking-wider"></div>
                                            <div class="table-cell p-l-2 py-1 bg-white text-right text-xs leading-4 "> 
                                                <div class="inline-flex ">
                                                    <x-button-light type="button" class="bg-transparent text-teal-600 hover:border-teal-500 col-span-1 mx-1 w-10 px-2" wire:click="editarItem({{ $item->id }}, {{ $item->producto->id }})">
                                                        <i class="las la-pencil-alt" style="font-size: 24px"></i>
                                                    </x-button>
                                                    <x-button-light type="button" wire:click="eliminarItem({{ $item->id }})" class="bg-transparent hover:border-red-600 text-red-600 col-span-1 mx-1 w-10 px-2">
                                                        <i class="las la-times" style="font-size: 24px"></i>
                                                    </x-button>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
            </div>
        </x-slot>
        <x-slot name="totales">
            <div class="grid grid-cols-12 gap-4 w-full ">
                <div class="col-span-12 md:col-span-9 hidden md:block">
                    <x-jet-label for="condiciones" value="{{ __('Condiciones comerciales:') }}"
                        class="text-left" />
                    <textarea wire:model.defer="condiciones"
                        class="border border-gray-300 px-3 py-2 text-sm focus:outline-none rounded overflow-hidden shadow-md "
                        style="width:100%" rows="5"></textarea>
                </div>
                <div class="col-span-12 md:col-span-3">
                    <span>&nbsp;</span>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="max-w-full rounded overflow-hidden shadow-md border border-gray-300">
                            <div class="px-6 py-4">
                                <div class="flex w-56 text-sm mb-2 col-span-2">
                                    <div class="flex-1 w-24 inline font-bold text-left col-span-1">SUBTOTAL:</div>
                                    <div class="flex-1 w-24 inline "><span class="text-right ">$
                                            {{ number_format($subtotal, 0, ',', '.') }} </span></div>
                                </div>
                                <div class="flex w-56 text-sm mb-2 col-span-2">
                                    <div class="flex-1 w-24 inline font-bold text-left col-span-1">IVA:</div>
                                    <div class="flex-1 w-24 inline "><span class="text-right ">$
                                            {{ number_format($ivatotal, 0, ',', '.') }} </span></div>
                                </div>
                                <div class="flex w-56 text-sm mb-2 col-span-2">
                                    <div class="flex-1 w-24 inline font-bold text-left col-span-1">TOTAL:</div>
                                    <div class="flex-1 w-24 inline"><span class="text-right ">$
                                            {{ number_format($grantotal, 0, ',', '.') }} </span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 md:hidden">
                    <x-jet-label for="condiciones" value="{{ __('Condiciones comerciales:') }}"
                        class="text-left" />
                    <textarea wire:model.defer="condiciones"
                        class="border border-gray-300 px-3 py-2 text-sm focus:outline-none rounded overflow-hidden shadow-md "
                        style="width:100%" rows="5"></textarea>
                </div>
                <div class="col-span-12 md:col-span-9">
                    <x-jet-label for="observaciones" value="{{ __('Observaciones:') }}" class="text-left" />
                    <textarea wire:model.defer="observaciones"
                        class="border border-gray-300 px-3 py-2 text-sm focus:outline-none rounded overflow-hidden shadow-md "
                        style="width:100%" rows="5"></textarea>
                </div>
            </div>
        </x-slot>
        <x-slot name="observaciones"> </x-slot>
        <x-slot name="actions">
            <x-jet-action-message class="mr-3" on="saved">
                {{ __('Datos actualizados ...') }}
            </x-jet-action-message>
              
            <x-link wire:loading.attr="disabled" link="cotizaciones/pdf/{{ $cotizacion->id }}" target="_blank"
                class="bg-orange-500 hover:bg-orange-400 py-2 px-4 border text-white hover:text-white">
                <i class="las la-file-pdf text-2xl pr-2"></i> {{ __('Ver pdf') }}
            </x-link>
            <x-jet-button wire:loading.attr="disabled" type="submit" class="mx-4 bg-indigo-600"
                wire:click="updateDetalles">
                {{ __('Guardar') }} <i class="las la-save text-2xl pl-2"></i>
            </x-jet-button>
        </x-slot>
    </x-form-table>

</div>

<script>
	$(document).ready(function() {
    	$('#selectProducto').on('change', function(){
			@this.set('producto_id', this.value)
		});
	});
</script>
<script>
    window.addEventListener('modalProductos', event => {
        $('#selectProducto').select2({
			width: '100%',
		});
    })
</script>