<form wire:submit.prevent="guardarEquipo">

    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4 ">
        <div class="sm:flex sm:items-start">
            <div
                class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-indigo-800 sm:mx-0 sm:h-10 sm:w-10">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round"
                        d="M9 13h6m-3-3v6m-9 1V7a2 2 0 012-2h6l2 2h6a2 2 0 012 2v8a2 2 0 01-2 2H5a2 2 0 01-2-2z" />
                </svg>
            </div>
            <div class="flex-1 w-full mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                <h3 class="text-lg leading-6 font-bold text-gray-900" id="modal-title">Equipo Para Servicio</h3>
            
                    <div class="mt-2">
                        <h3 class="mt-2 text-md leading-6 font-medium text-gray-900" id="modal-title">
                            Identificación del equipo
                        </h3>

                        <div class="pt-3 grid xl:grid-cols-3 md:grid-cols-2 xl:gap-4 gap-x-4 shadow-md px-4 rounded-sm">
                            <div class="relative z-0 mb-4 w-full group">
                                <input type="text" name="floating_equipo" list="equipos"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    wire:model.defer="detalles.equipo"
                                    >
                                    
                                <label for="floating_equipo"
                                    class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Equipo
                                </label>
                                <datalist id="equipos">
                                    <option value="Balanza electrónica">
                                    <option value="Báscula electrónica">
                                    <option value="Termómetro digital de punzón">
                                    <option value="Termómetro análogo de punzón">
                                    <option value="Indicador digital de temperatura">
                                    <option value="Indicador análogo de temperatura">
                                    <option value="Refractómetro digital">
                                    <option value="Refractómetro análogo">
                                </datalist>
                            </div>

                            <div class="relative z-0 mb-4 w-full group">
                                <input type="marca" name="floating_marca" id="floating_marca"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    wire:model.defer="detalles.marca" >
                                <label for="floating_marca"
                                    class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Marca
                                </label>
                            </div>

                            <div class="relative z-0 mb-4 w-full group">
                                <input type="text" name="floating_modelo" id="floating_modelo"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    wire:model.defer="detalles.modelo" >
                                <label for="floating_modelo"
                                    class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Modelo
                                </label>
                            </div>

                            <div class="relative z-0 mb-4 w-full group">
                                <input type="text" name="floating_serial"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    wire:model.defer="detalles.serial" >
                                <label for="floating_serial"
                                    class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Serial
                                </label>
                            </div>

                            <div class="relative z-0 mb-4 w-full group">
                                <input type="text" name="floating_codigo_interno" id="floating_codigo_interno"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    wire:model.defer="detalles.codigo_interno" >
                                <label for="floating_codigo_interno"
                                    class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Código Interno
                                </label>
                            </div>

                            <div class="relative z-0 mb-4 w-full group">
                                <input type="text" name="floating_ubicacion" id="floating_ubicacion"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    wire:model.defer="detalles.ubicacion" >
                                <label for="floating_ubicacion"
                                    class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Ubicación / Proceso
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mt-2" x-data="{ magnitud: @entangle('magnitud'), openMantenimiento: @entangle('mantenimiento'), openMetrologia: @entangle('metrologia'), conformidad:'no'}">
                        <h3 class="mt-2 text-md leading-6 font-medium text-gray-900">Servicio(s)</h3>
                        
                        <div class="pt-3 xl:gap-4 gap-x-4 shadow-md px-4 rounded-sm pb-4">
                            {{-- Toggle de mantenimiento --}}
                            <div class="relative z-0 mb-4 w-full group">
                                <div class="mb-4 form-check form-switch cursor-pointer">
                                    <input 
                                      wire:model="mantenimiento"
                                      id="toggle-mantenimiento" 
                                      class="form-check-input appearance-none w-11 h-6 -ml-10 rounded-full float-left align-top bg-no-repeat bg-contain bg-gray-200 focus:outline-none cursor-pointer shadow-sm" 
                                      type="checkbox" 
                                      role="switch" >
                                    <label class="ml-3 text-sm font-medium form-check-label inline-block text-gray-900" for="toggle-mantenimiento">Mantenimiento</label>
                                </div>

                                <fieldset x-show="openMantenimiento" class="flex flex-row gap-6">
                                    <legend class="sr-only">Mantenimiento</legend>
                                    <div class="flex items-center mb-4">
                                        <input id="diagnostico" 
                                            type="radio" 
                                            name="mantenimiento_tipo" 
                                            wire:model.defer="mantenimiento_tipo"
                                            value="diagnostico"
                                            @if($mantenimiento_tipo==='diagnostico') checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="diagnostico" aria-describedby="diagnostico" />
                                        <label for="diagnostico"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Diagnostico
                                        </label>
                                    </div>

                                    <div class="flex items-center mb-4">
                                        <input id="preventivo" 
                                            type="radio" 
                                            name="mantenimiento_tipo"
                                            wire:model.defer="mantenimiento_tipo"
                                            value="mantenimiento preventivo"
                                            @if($mantenimiento_tipo==='mantenimiento preventivo' || $mantenimiento_tipo===null) checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="preventivo" aria-describedby="preventivo" />
                                        <label for="preventivo"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Mantenimiento preventivo
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4">
                                        <input id="correctivo" 
                                            type="radio" 
                                            name="mantenimiento_tipo"
                                            wire:model.defer="mantenimiento_tipo"
                                            value="mantenimiento correctivo"
                                            @if($mantenimiento_tipo==='mantenimiento correctivo') checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="correctivo" aria-describedby="correctivo" />
                                        <label for="correctivo"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Mantenimiento correctivo
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="relative z-0 w-full group" x-show="openMantenimiento">
                                <textarea type="text" 
                                    name="floating_descripcion" 
                                    id="floating_descripcion"
                                    wire:model.defer="detalles.descripcion"
                                    class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    placeholder=""></textarea>
                                <label for="floating_descripcion"
                                    class="absolute text-smd text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                    Estado actual del equipo (En caso de falla, descríbala brevemente)
                                </label>
                            </div>

                            <div x-show="openMantenimiento" class="border-0 border-b-2 border-dotted mb-4 mt-4 w-full"></div>

                            {{-- Toggle de metrologia --}}
                            <div class="relative z-0 mb-4 w-full group">
                                <div class="mb-4 form-check form-switch cursor-pointer">
                                    <input 
                                      wire:model="metrologia"
                                      id="toggle-metrologia" 
                                      class="form-check-input appearance-none w-11 h-6 -ml-10 rounded-full float-left align-top bg-no-repeat bg-contain bg-gray-200 focus:outline-none cursor-pointer shadow-sm" 
                                      type="checkbox" 
                                      role="switch" >
                                    <label class="ml-3 text-sm font-medium form-check-label inline-block text-gray-900" for="toggle-metrologia">Metrología </label>
                                </div>
                               
                                <fieldset x-show="openMetrologia" class="flex flex-row gap-6">
                                    <legend class="sr-only">Metrología</legend>
                                    <div class="flex items-center mb-4">
                                        <input id="calibracion" 
                                            type="radio" 
                                            name="metrologia_tipo" 
                                            wire:model.defer="metrologia_tipo" 
                                            value="calibración"
                                            @if($metrologia_tipo==='calibración' || $metrologia_tipo===null) checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="calibracion" aria-describedby="calibracion">
                                        <label for="calibracion"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Calibración
                                        </label>
                                    </div>

                                    <div class="flex items-center mb-4">
                                        <input id="verificacion" 
                                            type="radio" 
                                            name="metrologia_tipo"
                                            wire:model.defer="metrologia_tipo"
                                            value="verificación"
                                            @if($metrologia_tipo==='verificación') checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="verificacion" aria-describedby="verificacion">
                                        <label for="verificacion"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Verificación                                     
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4">
                                        <input id="calificacion" 
                                            type="radio" 
                                            name="metrologia_tipo"
                                            wire:model.defer="metrologia_tipo"
                                            value="calificación"
                                            @if($metrologia_tipo==='calificación') checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="calificacion" aria-describedby="calificacion">
                                        <label for="calificacion"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Calificación
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4">
                                        <input id="validacion" 
                                            type="radio" 
                                            name="metrologia_tipo"
                                            wire:model.defer="metrologia_tipo"
                                            value="validación"
                                            @if($metrologia_tipo==='validación') checked @endif
                                            class="cursor-pointer w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 bg-gray-300"
                                            aria-labelledby="validacion" aria-describedby="validacion">
                                        <label for="validacion"
                                            class="block ml-2 text-sm font-medium text-gray-900 cursor-pointer">
                                            Validación
                                        </label>
                                    </div>
                                </fieldset>
                                <div x-show="openMetrologia" class="pt-3 grid xl:grid-cols-3 md:grid-cols-2 xl:gap-4 gap-x-4">
                                    <div class="flex flex-col relative z-0 mb-4 group">
                                        <label 
                                            for="magnitud" 
                                            class="block mb-2 text-sm font-medium text-gray-500">
                                            Magnitud
                                        </label>
                                        <select id="magnitud" 
                                            x-on:change="magnitud = $event.target.value" 
                                            wire:model.defer="magnitud" 
                                            
                                            class="border-0 border-b  text-gray-900 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer text-sm focus:border-b block w-full p-2">
                                            <option value="masa">Masa</option>
                                            <option value="temperatura">Temperatura</option>
                                            <option value="pH">pH</option>
                                            <option value="volumen">Pequeños volumenes</option>
                                            <option value="presión">Presión</option>
                                            <option value="refractometría">Refractometría</option>
                                            <option value="otra">Otra</option>
                                        </select>
                                    </div>

                                    <div class="flex flex-col relative z-0 mb-4 group">
                                        <label for="rango" class="block mb-2 text-sm font-medium text-gray-500">
                                            <span x-show="magnitud==='temperatura'" >Puntos de calibración (separados por coma)</span> 
                                            <span x-show="magnitud!=='temperatura'" >Rango máximo</span> 
                                        </label>
                                        <input 
                                            type="text" 
                                            name="rango" 
                                            id="rango"
                                            wire:model.defer="detalles.rango"
                                            class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                            placeholder="" >
                                    </div>

                                    <div class="flex flex-col relative z-0 mb-4 group">
                                        <label for="magnitud" class="block mb-2 text-sm font-medium text-gray-500">
                                            Requiere declaración de conformidad
                                        </label>
                                        <select 
                                            id="conformidad" 
                                            x-on:change="conformidad = $event.target.value" 
                                            wire:model.defer="detalles.conformidad" 
                                            class="border-0 border-b  text-gray-900 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer text-sm focus:border-b block w-full p-2">
                                            <option value="no">No</option>
                                            <option value="si">Si</option>
                                        </select>
                                    </div>

                                    <div x-show="conformidad==='si'" class="flex flex-col col-span-2 relative z-0 mb-4 group">
                                        <label for="regla" class="block mb-2 text-sm font-medium text-gray-500">
                                            Regla de desición 
                                        </label>
                                        <input 
                                            type="text" 
                                            name="regla" 
                                            id="regla"
                                            wire:model.defer="detalles.regla"
                                            class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                            placeholder="" >
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
            
            </div>
        </div>
    </div>

    <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
        <button type="submit"
            class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm pl-1 pr-4 py-2 bg-indigo-800 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:ml-3 sm:w-auto sm:text-sm">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
            Guardar
        </button>
        
        <button type="button" 
            wire:click="$emit('closeModal')"
            class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm pl-1 pr-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
            Cancelar
        </button>
    </div>
</form>