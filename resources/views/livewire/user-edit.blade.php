
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Editar datos de usuario ') }}
        </h2>
    </x-slot>
    
<div>
    <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
        @include('users.perfil')
        <x-jet-section-border />
                  
        <div class="mt-10 sm:mt-0">
            @include('users.password')
        </div>
        <x-jet-section-border />
    </div>
</div>
    