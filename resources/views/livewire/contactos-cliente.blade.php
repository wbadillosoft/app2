
<x-jet-form-section submit="updateContacto">
    <x-slot name="title">
        {{ __('Contactos') }}
    </x-slot>

    <x-slot name="description">
        {{ __('') }}
    </x-slot>
   
    <x-slot name="form">
        
        <div class = "col-span-11 md:col-span-11 sm:col-span-11 w-full">

            <div class="mt-5 md:mt-0 md:col-span-6">
                <table class="min-w-full divide-y divide-gray-200 bg-gray-50 " style="width:100%">
                    <tr>
                        <thead>
                            <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Nombre</th>
                            {{--<th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Cargo</th>--}}
                            <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Email</th>
                            <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Teléfono</th>
                            <th class="px-6 py-3 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"></th>
                        </thead>
                    </tr>
                    <tbody>
                        @foreach ($contactos as $contacto)
                            <tr>
                                
                                <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">
                                    <div class="ml-4">
                                        <div class="text-sm leading-5 font-medium text-gray-900">
                                            {{$contacto->nombre}}
                                        </div>
                                        <div class="text-xs leading-5 text-gray-500">
                                            {{$contacto->cargo}}
                                        </div>
                                    </div>
                                </td>
                                {{--<td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->cargo}}</td>--}}
                                <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->email}}</td>
                                <td class="px-3 py-2 whitespace-normal font-light text-sm border-bottom">{{$contacto->telefono}}</td>
                                <td class=" whitespace-no-wrap text-sm border-bottom">
                                    <x-jet-button type="button" class="button bg-blue-600" wire:click="editarContacto({{ $contacto->id }})">
                                        <i class="las la-lg la-pen"></i>
                                    </x-jet-button>
                                </td>
                            </tr> 
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        
        <!-- Nombre -->
        <div class="col-span-4 sm:col-span-4 w-full">
            <x-jet-label for="name" value="{{ __('Nombre') }}" />
            <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model.defer="nombre" id="nombre" autocomplete="nombre" />
            <x-jet-input-error for="name" class="mt-2" />
        </div>
         <!-- Cargo -->
        <div class="col-span-4 sm:col-span-4 w-full">
            <x-jet-label for="cargo" value="{{ __('Cargo') }}" />
            <x-jet-input id="cargo" type="text" class="mt-1 block w-full" wire:model.defer="cargo" autocomplete="cargo" />
            <x-jet-input-error for="cargo" class="mt-2" />
        </div>
         <!-- Teléfono -->
        <div class="col-span-4 sm:col-span-4 w-full">
            <x-jet-label for="email" value="{{ __('Email') }}" />
            <x-jet-input id="email" type="text" class="mt-1 block w-full" wire:model.defer="email" autocomplete="email" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
         <!-- Email -->
        <div class="col-span-4 sm:col-span-4 w-full">
            <x-jet-label for="telefono" value="{{ __('Teléfono') }}" />
            <x-jet-input id="telefono" type="text" class="mt-1 block w-full" wire:model.defer="telefono" autocomplete="telefono" />
            <x-jet-input-error for="telefono" class="mt-2" />
        </div>
        
    </x-slot>
   
   
    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Guardado...') }}
        </x-jet-action-message>
       
        <x-jet-button wire:loading.attr="disabled" type="button" wire:click="cancelar" class="mx-4 bg-red-600">
            {{ __('Cancelar') }}
        </x-jet-button>
        
        @if($agregar_enable)
        <x-jet-button wire:loading.attr="disabled" wire:click="agregarContacto" class="bg-indigo-600 ">
            {{ __('Agregar') }}
        </x-jet-button>
        @endif
        @if(!$agregar_enable)
        <x-jet-button wire:loading.attr="disabled" >
            {{ __('Guardar') }}
        </x-jet-button>
        @endif
        
    </x-slot>
    
</x-jet-form-section>

