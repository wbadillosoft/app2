<div>
    <x-slot name="header">   
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Remisiones') }}
        </h2>  
    </x-slot>

    <div class="container mx-auto py-6" >
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            @if($mostrar_lista)
                <div>
                    @include('remisiones.lista')
                </div>
            @endif

            @if($mostrar_editar)
                <div>
                    @include('remisiones.editar', [
                        'clientes'=>$clientes,
                    ])
                </div>
                <x-jet-section-border />
                <div>
                    @livewire('detalles-remision', ['remision' => $remision])
                </div>       
            @endif

            @if($mostrar_crear)
                <div>
                    @include('remisiones.crear', [
                        'clientes'=>$clientes,
                    ])
                </div>
            @endif

            @if($mostrar_show)
                <div>
                    @include('remisiones.show')
                </div>
            @endif             
        </div>
    </div>

    <script>
     

        function formato(input)
        {
            var numero = input.value.replace(/\D/g,'');
                numero = numero
                    .replace(/([0-9])([0-9]{3})$/, '$1.$2')  
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".")
                input.value = numero;
        }

    </script>
</div>