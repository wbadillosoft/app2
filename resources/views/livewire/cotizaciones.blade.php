<div>
    <x-slot name="header">   
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Cotizaciones') }}
        </h2>  
    </x-slot>

    <div class="py-6" >
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            @if($mostrar_lista)
                <div>
                    @include('cotizaciones.lista')
                </div>
            @endif

            @if($mostrar_editar)
                <div>
                    @include('cotizaciones.editar', [
                        'clientes'=>$clientes,
                    ])
                </div>
                <x-jet-section-border />
                <div>
                    @livewire('detalles-cotizacion', ['cotizacion' => $cotizacion])
                </div>       
            @endif

            @if($mostrar_crear)
                <div>
                    @include('cotizaciones.crear', [
                        'clientes'=>$clientes,
                    ])
                </div>
            @endif

            @if($mostrar_show)
                <div>
                    @include('cotizaciones.show')
                </div>
            @endif             
        </div>
    </div>
    
@push('js')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"></script>
	<script>
		$(document).ready( function () {
			$('#myTable').DataTable({
                "language": {
                    "url": "////cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json"
                },
                ordering:  false,
            });
		} );
	</script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            Livewire.on('saved', () => {
                ToastInfo.fire({
                background:'#059669',
                icon: 'success',
                iconColor: '#FFF',
                color: '#FFF',
                title: 'Los datos han sido guardados',
                })
            })


        Livewire.on('saved1', () => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Los datos han sido guardados',
                showConfirmButton: false,
                timer: 1500
            })
        })

        Livewire.on('eliminar', itemId => {
            Swal.fire({
                title: 'Está seguro?',
                text: "Va a eliminar un item de la lista",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, eliminar!'
                }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('detalles-cotizacion', 'eliminarItem', itemId);
                    Swal.fire(
                    'Eliminado!',
                    'El Item seleccionado se eliminó.',
                    'success'
                    )
                }
            })      
        })
    })
    </script>
  
@endpush
    <script>
        function formato(input)
        {
            var numero = input.value.replace(/\D/g,'');
                numero = numero
                    .replace(/([0-9])([0-9]{3})$/, '$1.$2')  
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".")
                input.value = numero;
        }
        function numero(input)
        {
            var numero = input.value.replace(/\D/g,'');
                input.value = numero;
        }
    </script>
</div>