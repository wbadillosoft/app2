<div class="col-span-9 sm:col-span-8 mb-1" >
	<x-jet-button name="nuevo_proveedor" id="nuevo_proveedor" class="mt-1 inline" wire:click="formCrear">
		Nuevo proveedor
	</x-jet-button>
</div>

<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
	<table class="table-auto" style="width:100%" id="proveedoresTable"  data-page-length='25'>
		<thead>
		  <tr>
			<th class="px-6 py-4 bg-gray-100 h-7 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Nombre / Razón social</th>
			<th class="px-6 py-4 bg-gray-100 h-7 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Nit</th>
			<th class="px-6 py-4 bg-gray-100 h-7 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Ciudad</th>
			<th class="px-6 py-4 bg-gray-100 h-7 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider">Dirección</th>
			<th class="px-6 py-4 bg-gray-100 h-7 text-left text-xs leading-4 font-large text-gray-600 uppercase tracking-wider"></th>
		  </tr>
		</thead>

		<tbody class="bg-white divide-y divide-gray-100">
			@foreach ($proveedores as $proveedor)    
			<tr class="hover:bg-gray-50">
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">{{$proveedor->nombre}}</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">{{$proveedor->nit}}</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">{{$proveedor->ciudad}}</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom ">
					<div class="text-sm leading-5 text-gray-500">{{$proveedor->direccion}}</div>
				</td>
				<td class="px-4 py-2 whitespace-no-wrap border-bottom text-center">
					<div class="leading-0 text-gray-500">
						<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-300 cursor-pointer" wire:click="show({{ $proveedor->id }})">
							<svg 
								class="p-1"
								fill="rgb(161 161 170)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M144.3 32.04C106.9 31.29 63.7 41.44 18.6 61.29c-11.42 5.026-18.6 16.67-18.6 29.15l0 357.6c0 11.55 11.99 19.55 22.45 14.65c126.3-59.14 219.8 11 223.8 14.01C249.1 478.9 252.5 480 256 480c12.4 0 16-11.38 16-15.98V80.04c0-5.203-2.531-10.08-6.781-13.08C263.3 65.58 216.7 33.35 144.3 32.04zM557.4 61.29c-45.11-19.79-88.48-29.61-125.7-29.26c-72.44 1.312-118.1 33.55-120.9 34.92C306.5 69.96 304 74.83 304 80.04v383.1C304 468.4 307.5 480 320 480c3.484 0 6.938-1.125 9.781-3.328c3.925-3.018 97.44-73.16 223.8-14c10.46 4.896 22.45-3.105 22.45-14.65l.0001-357.6C575.1 77.97 568.8 66.31 557.4 61.29z"/>
							</svg>
						</div>
						<div class="w-9 h-9 p-1 inline-flex items-center rounded-full hover:bg-gray-300 cursor-pointer" wire:click="formEditar({{ $proveedor->id }})">
							<svg 
								class="p-1"
								fill="rgb(161 161 170)" 
								xmlns="http://www.w3.org/2000/svg" 
								viewBox="0 0 576 512">
								<path d="M0 64C0 28.65 28.65 0 64 0H224V128C224 145.7 238.3 160 256 160H384V299.6L289.3 394.3C281.1 402.5 275.3 412.8 272.5 424.1L257.4 484.2C255.1 493.6 255.7 503.2 258.8 512H64C28.65 512 0 483.3 0 448V64zM256 128V0L384 128H256zM564.1 250.1C579.8 265.7 579.8 291 564.1 306.7L534.7 336.1L463.8 265.1L493.2 235.7C508.8 220.1 534.1 220.1 549.8 235.7L564.1 250.1zM311.9 416.1L441.1 287.8L512.1 358.7L382.9 487.9C378.8 492 373.6 494.9 368 496.3L307.9 511.4C302.4 512.7 296.7 511.1 292.7 507.2C288.7 503.2 287.1 497.4 288.5 491.1L303.5 431.8C304.9 426.2 307.8 421.1 311.9 416.1V416.1z"/>
							</svg>
						<div>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	  </table>
</div>
