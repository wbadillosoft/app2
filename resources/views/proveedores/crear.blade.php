<x-jet-form-section submit="store">
    <x-slot name="title">
        {{ __('Crear proveedor') }}
    </x-slot>

    <x-slot name="description">
		{{ __('') }}
		<div class="py-5">
			<x-jet-nav-link href="{{ route('proveedores') }}" wire:loading.attr="disabled" >
				<x-jet-button wire:loading.attr="disabled" >
					{{ __('Volver a la lista') }}
				</x-jet-button>
			</x-jet-nav-link>
		</div>
    </x-slot>
	<x-slot name="form">
		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nombre" value="{{ __('Nombre') }}" />
			<x-jet-input id="nombre" type="text" class="mt-1 block w-full" wire:model.defer="nombre" autocomplete="nombre" required  />
			<x-jet-input-error for="nombre" class="mt-2" />
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="nit" value="{{ __('Nit') }}" />
			<x-jet-input id="nit" type="text" class="mt-1 block w-full" wire:model.defer="nit" autocomplete="nit" required />
			<x-jet-input-error for="nit" class="mt-2" />
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="telefono" value="{{ __('Teléfono') }}" />
			<x-jet-input id="telefono" type="text" class="mt-1 block w-full" wire:model.defer="telefono" autocomplete="telefono" required />
			<x-jet-input-error for="telefono" class="mt-2" />
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="email" value="{{ __('Email') }}" />
			<x-jet-input id="email" type="text" class="mt-1 block w-full" wire:model.defer="email" autocomplete="email" />
			<x-jet-input-error for="email" class="mt-2" />
		</div>                    

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="direccion" value="{{ __('Dirección') }}" />
			<x-jet-input id="direccion" type="text" class="mt-1 block w-full" wire:model.defer="direccion" autocomplete="direccion" />
			<x-jet-input-error for="direccion" class="mt-2" />
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="ciudad" value="{{ __('Ciudad') }}" />
			<x-jet-input id="ciudad" list="municipios" type="text" class="mt-1 block w-full" wire:model.defer="ciudad" autocomplete="ciudad" required />
			<x-jet-input-error for="ciudad" class="mt-2" />
			<datalist id="municipios">
				@foreach ($municipios as $municipio)
					<option value="{{$municipio->municipio}}">	
				@endforeach
			</datalist>
		</div>  

		<div class="col-span-6 sm:col-span-6">
			<x-jet-label for="notas" value="{{ __('Notas sobre el proveedor') }}" />
			<x-textarea id="notas" class="mt-1 block w-full" wire:model.defer="notas" autocomplete="notas" rows="4" />
			<x-jet-input-error for="notas" class="mt-2" />
		</div>  
		
		<x-slot name="actions">
			<x-jet-action-message class="mr-3" on="saved">
				{{ __('Guardado...') }}
			</x-jet-action-message>
	
			<x-jet-button wire:loading.attr="disabled" wire:target="photo">
				{{ __('Guardar') }}
			</x-jet-button>
		</x-slot>		

	</x-slot>

</x-jet-form-section>
