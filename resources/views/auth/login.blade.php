<x-guest-layout>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="relative min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 ">
            <div class="z-10 mt-2">
                <div class="rounded-full border-4 border-gray-500 bg-gray-200 px-1 py-1 h-50 w-50">
                    <div class="bg-white rounded-full border border-gray-500 px-1 py-6 h-50 w-50">
                        <x-authentication-card-logo />
                    </div>
                </div>
            </div>            

            <div class="absolute w-96 z-0 -mt-72 border-0 border-b-4 border-gray-500 rounded-full"></div>
            
            <x-login-errors class="absolute mb-20" />
            
            @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
            @endif

            <div class="w-full sm:max-w-md mt-6 px-6 py-4 overflow-hidden sm:rounded-lg">
                {{-- <div class="border-0 border-b border-gray-500 rounded-full"></div> --}}
                
                <div class="py-4">
                    <div class="mt-4 relative rounded-full border border-gray-500">
                        <div class="absolute inset-y-0.5 left-0.5 pl-0 flex items-center pointer-events-none border bg-gray-400 border-white rounded-full h-11 w-11">
                            <span class="text-gray-300 sm:text-sm w-10 px-2"> 
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">                           
                                    <path fill="#FFF" d="M207.8 20.73c-93.45 18.32-168.7 93.66-187 187.1c-27.64 140.9 68.65 266.2 199.1 285.1c19.01 2.888 36.17-12.26 36.17-31.49l.0001-.6631c0-15.74-11.44-28.88-26.84-31.24c-84.35-12.98-149.2-86.13-149.2-174.2c0-102.9 88.61-185.5 193.4-175.4c91.54 8.869 158.6 91.25 158.6 183.2l0 16.16c0 22.09-17.94 40.05-40 40.05s-40.01-17.96-40.01-40.05v-120.1c0-8.847-7.161-16.02-16.01-16.02l-31.98 .0036c-7.299 0-13.2 4.992-15.12 11.68c-24.85-12.15-54.24-16.38-86.06-5.106c-38.75 13.73-68.12 48.91-73.72 89.64c-9.483 69.01 43.81 128 110.9 128c26.44 0 50.43-9.544 69.59-24.88c24 31.3 65.23 48.69 109.4 37.49C465.2 369.3 496 324.1 495.1 277.2V256.3C495.1 107.1 361.2-9.332 207.8 20.73zM239.1 304.3c-26.47 0-48-21.56-48-48.05s21.53-48.05 48-48.05s48 21.56 48 48.05S266.5 304.3 239.1 304.3z"/>
                                </svg>
                            </span>
                        </div>
                        <input 
                            type="email" 
                            name="email" 
                            :value="old('email')" 
                            required 
                            autofocus
                            id="email" 
                            class="p-2 h-12 focus:ring-white focus:border-white block w-full pl-14 pr-4 border-gray-300 rounded-full" 
                            placeholder="e-mail" />
                    </div>
                
                    <div class="mt-4 relative rounded-full border border-gray-500">
                        <div class="absolute inset-y-0.5 left-0.5 pl-0 flex items-center pointer-events-none border bg-gray-400 border-white rounded-full h-11 w-11">
                            <span class="text-gray-300 sm:text-sm w-10 px-2"> 
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path fill="#FFF" d="M282.3 343.7L248.1 376.1C244.5 381.5 238.4 384 232 384H192V424C192 437.3 181.3 448 168 448H128V488C128 501.3 117.3 512 104 512H24C10.75 512 0 501.3 0 488V408C0 401.6 2.529 395.5 7.029 391L168.3 229.7C162.9 212.8 160 194.7 160 176C160 78.8 238.8 0 336 0C433.2 0 512 78.8 512 176C512 273.2 433.2 352 336 352C317.3 352 299.2 349.1 282.3 343.7zM376 176C398.1 176 416 158.1 416 136C416 113.9 398.1 96 376 96C353.9 96 336 113.9 336 136C336 158.1 353.9 176 376 176z"/>
                                </svg>
                            </span>
                        </div>
                        <input 
                            required 
                            type="password" 
                            name="password" 
                            id="password" 
                            class="p-2 h-12 focus:ring-gray-500 focus:border-gray-500 block w-full pl-14 pr-4 border-gray-300 rounded-full" 
                            placeholder="contraseña" />
                    </div>
                </div>

                <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-3 px-4 rounded-full w-full active:bg-blue-500 focus:outline-none focus:border-blue-500 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                    Login
                </button>
                
                <div class="flex items-center justify-end mt-4">
                    @if (Route::has('password.request'))
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                            {{ __('Olvidó su contraseña?') }}
                        </a>
                    @endif              
                </div>
            </div>
        </div>
    </form>
</x-guest-layout>
