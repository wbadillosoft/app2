@props(['querySearch'])

<div  x-data="{ expandir: false }" class="w-full col-span-10">       
	<span class="select-container w-full absolute">
		<span class="seleccion border form-input shadow-sm  border-gray-500 rounded-md block cursor-pointer"  @click="expandir = ! expandir" >
			<span class="seleccionado mt-1 text-gray-600 pl-2 pr-4 overflow-hidden overflow-ellipsis whitespace-nowrap  inline ">
				<x-slot name="seleccionado">{{ $seleccionado }}</x-slot> 
			</span>
			<span :class="{'select-flecha-expande': expandir, 'select-flecha': ! expandir}" class="select-flecha absolute inset-y-0 right-0 ">
				<b  class="flecha"></b>
			</span>          
		</span>
		
		<span :class="{'block': expandir, 'hidden': ! expandir}" class="search hidden ">
			<input type="text" class="border border-gray-300 mt-1 mb-1 w-full autofocus px-2 py-1 rounded-sm" wire:model="{{$querySearch}}">
			<span class="resultados border border-gray-300 bg-white">
				<ul class="bg-gray-50 overflow-y-auto h-32 border border-gray-300 shadow-sm">
					{{$slot}}
				</ul>
			</span>
		</span>
	</span>
</div>
