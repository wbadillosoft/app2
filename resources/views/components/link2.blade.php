@props(['link'])
<a href="{{$link}}" {{ $attributes->merge(['class' => 'p-4 cursor-pointer inline-flex items-center border border-transparent rounded-md text-xs tracking-widest focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150']) }}>
	{{ $slot }}
</a>

