@props(['submit', 'cols'])

<div {{ $attributes->merge(['class' => 'md:grid md:grid-cols-3 md:gap-6']) }}>
    <div class="text-lg font-medium text-gray-900 "> {{$btn}} </div>
    <span class="text-lg font-medium text-gray-900 text-center"> {{$fecha}} </span>
    <h3 class="text-lg font-medium text-blue-700 text-right">{{ $title }}</h3>


    <div class="mt-5 md:mt-0 md:col-span-2 {{$cols}}">
        <form wire:submit.prevent="{{ $submit }}">
            <div class="shadow overflow-hidden sm:rounded-md">
                <div class="px-4 py-5 bg-white sm:p-6">
                    <div class="grid grid-cols-6 gap-6">
                        {{ $form }}
                    </div>
                </div>

                @if (isset($actions))
                    <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                        {{ $actions }}
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>
