@props(['submit', 'cols', 'id'])

<div {{ $attributes->merge(['class' => 'md:grid md:grid-cols-3 md:gap-6']) }}>

    <div class="mt-5 md:mt-0 md:col-span-2 {{$cols}} ">
        {{--<form wire:submit.prevent="{{ $submit }}" id="{{ $id }}">--}}
            <div class="shadow overflow-hidden sm:rounded-md">
                <div class="px-4 py-5 bg-white sm:p-6">
                    <div class="grid grid-cols-6 gap-6">
                        {{ $form }}
                    </div>
                </div>

                @if (isset($totales))
                    <div class="flex items-center justify-end px-4 py-3 bg-white text-right sm:px-6">
                        {{ $totales }}
                    </div>
                @endif
              
                @if (isset($observaciones))
                    <div class="flex items-center min-w-full justify-start px-4 py-3 bg-white text-right sm:px-6">
                        {{ $observaciones }}
                    </div>
                @endif
              
                @if (isset($actions))
                    <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                        {{ $actions }}
                    </div>
                @endif
            </div>
        {{--</form>--}}
    </div>
</div>
