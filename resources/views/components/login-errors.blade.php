@if ($errors->any())
    <div {{ $attributes }}>
        
        <ul class="list-disc list-inside text-sm text-red-600">
            Error de autenticación, inténtelo de nuevo
        </ul>
    </div>
@endif
