@props(['link'])
<a href="{{$link}}" {{ $attributes->merge(['class' => 'cursor-pointer inline-flex items-center border border-transparent rounded-md text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150']) }}>
	{{ $slot }}
</a>

