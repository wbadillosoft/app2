@props(['value'])

<label {{ $attributes->merge(['class' => 'inline font-medium text-sm text-gray-700']) }}>
    {{ $value ?? $slot }}
</label>
