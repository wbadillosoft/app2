@props(['disabled' => false])

<select {!! $attributes->merge(['class' => 'form-input rounded-md shadow-sm']) !!}>
	{{$slot}}
</select>
