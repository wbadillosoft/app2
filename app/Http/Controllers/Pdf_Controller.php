<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Prefactura;
use App\Models\Cotizacion;
use App\Models\Contactos_cliente;
use App\Models\Orden;
use App\Models\Remision;
use App\Models\User;

//use Barryvdh\DomPDF\Facade as PDF;
use Dompdf\Adapter\CPDF;      
use Dompdf\Dompdf;
use Dompdf\Exception;

class Pdf_Controller extends Controller
{
    public function ordenPdf($id)
    {
        $logo   = asset('/imgs/Logo_color.jpg');
        $orden  = Orden::find($id);
        $aprobo = User::find($orden->aprobo);
  
        $data   = [
            'orden' => $orden,
            'logo' => $logo,
            'aprobo' => $aprobo,
        ];
       
        return PDF::loadView('ordenes.pdf', $data)
            ->setPaper('Letter', 'P')
            ->stream($orden->codigo.'-(MBL-'.$orden->proveedor->nombre.').pdf');
    }
    
    public function ordenPreview($id)
    {
        $logo   = asset('/imgs/Logo_color.jpg');
        $orden  = Orden::find($id);
        $aprobo = User::find($orden->aprobo);

        $page = '
        .body{
            display:flex;
            justify-content: center;
            align-items: center;
            padding:0px 0px;
        }
        .contenedor{
            background:#fff;
            width:792px;
            padding:50px 20px;
            border:1px solid #fff;
            font-family:Helvetica;
        }
        body{
            background:#555;
        }
            #header { position: relative; left: 0px; top: -30px; right: 0px; height: 100px; background-color: white; text-align: center; }
        ';

        $data   = [
            'orden'     => $orden,
            'logo'      => $logo,
            'elaboro'   => $elaboro,
            'page'      => $page,
        ];
        return view('ordenes.pdf', $data);
    }

    public function prefacturaPdf($id)
    {
        $dompdf = new Dompdf(array('enable_remote' => true));
        $logo       = asset('/imgs/logo_esisco_bn.png');
        $prefactura = Prefactura::find($id);
        $elaboro    = User::find($prefactura->usuario_id);
        
        $dompdf->setPaper('letter', 'portrait');
        $dompdf->loadHtml(view('prefacturas.pdf', compact('prefactura', 'logo', 'elaboro')));

        $dompdf->render();
            // Parametros numeracion de paginas 
                $x_page_num = 495;
                $x_code     = 40;
                $y          = 80;
                $text       = "Págna {PAGE_NUM} de {PAGE_COUNT}";     
                $font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');   
                $size       = 8;    
                $color      = array(0,0,0);
                $word_space = 0.0;
                $char_space = 0.0;
                $angle      = 0.0;

        $dompdf->getCanvas()->page_text($x_page_num, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        $dompdf->stream($prefactura->codigo.' ('.$prefactura->cliente->nombre.').pdf',array("Attachment"=>0));
/*
        $logo       = asset('/imgs/logo_esisco_bn.png');
        $prefactura = Prefactura::find($id);
        $elaboro    = User::find($prefactura->usuario_id);
        $page = '';
        $data   = [
            'prefactura'=> $prefactura,
            'logo'      => $logo,
            'elaboro'   => $elaboro,
            'page'  => $page,
        ];
       
        return PDF::loadView('prefacturas.pdf', $data)
            ->setPaper('Letter', 'P')
            ->stream($prefactura->codigo.' ('.$prefactura->cliente->nombre.').pdf');
            */
    }

    public function cotizacionPdf($id)
    {
        $dompdf = new Dompdf(array('enable_remote' => true));
        $logo       = asset('/imgs/logo_esisco.png');
        $cotizacion = Cotizacion::find($id);
        $contacto   = Contactos_cliente::find($cotizacion->contacto_id);
        
        $dompdf->setPaper('letter', 'portrait');
        $dompdf->loadHtml(view('cotizaciones.pdf', compact('cotizacion', 'logo', 'contacto')));

        $dompdf->render();
            // Parametros numeracion de paginas 
                $x_page_num = 495;
                $x_code     = 40;
                $y          = 80;
                $text       = "Págna {PAGE_NUM} de {PAGE_COUNT}";     
                $font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');   
                $size       = 8;    
                $color      = array(0,0,0);
                $word_space = 0.0;
                $char_space = 0.0;
                $angle      = 0.0;

        $dompdf->getCanvas()->page_text($x_page_num, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        $dompdf->stream($cotizacion->codigo.' ('.$cotizacion->cliente->nombre.').pdf',array("Attachment"=>0));
    }

    public function prefacturaPreview($id)
    {
        $logo       = asset('/imgs/logo_esisco_bn.jpg');
        $prefactura = Prefactura::find($id);
        $elaboro    = User::find($prefactura->usuario_id);
        
        $page = '
        .body{
            display:flex;
            justify-content: center;
            align-items: center;
            padding:0px 0px;
        }
        .contenedor{
            background:#fff;
            width:792px;
            padding:50px 20px;
            border:1px solid #fff;
            font-family:Helvetica;
        }
        body{
            background:#555;
        }
            #header { position: relative; left: 0px; top: -30px; right: 0px; height: 100px; background-color: white; text-align: center; }
        ';

        
        $data   = [
            'prefactura'=> $prefactura,
            'logo'      => $logo,
            'elaboro'   => $elaboro,
            'page'  => $page,
        ];
       
        return view('prefacturas.pdf', $data);
            
    }
}
