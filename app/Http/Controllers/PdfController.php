<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Prefactura;
use App\Models\Cotizacion;
use App\Models\Contactos_cliente;
use App\Models\Orden;
use App\Models\Remision;
use App\Models\User;

use PDF;

class PdfController extends Controller
{

    public function prefacturaPdf($id)
    {

        $logo = asset('/imgs/logo_bn.jpg');
        PDF::setPrintHeader(true);
        PDF::setPrintFooter(true);

        // set header and footer fonts
        PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

         // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, 32, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        PDF::setJPEGQuality(75);

        $prefactura = Prefactura::find($id);
        define ('CODIGO', $prefactura->codigo);
        
        PDF::setHeaderCallback(function($headerpdf) {
            #$prefactura = Prefactura::find($id);
            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');
            $logo   = asset('/imgs/logo_bn.jpg');
            $codigo = CODIGO;
            $w      = 25;
            $h      = $w/1.5;

            $headerpdf->Image($logo,15,10, $w, $h);
            $headerpdf->SetFont('helvetica', 'B', 14);
            $headerpdf->Cell(0, 20, 'Prefactura #   '.$codigo. '  ', 0, false, 'R', 0, '', 0, false, 'T', 'R');
            $headerpdf->Line(14, 30, 202, 30, $style);
            $headerpdf->Line(14, 30.5, 202, 30.5, $style);

            $headerpdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $headerpdf->RoundedRect(165, 10, 36, 10, 1.50, '1111');
            $headerpdf->SetFont('helvetica', 'R', 9);
            $headerpdf->Text(177, 25,'Página: '.PDF::getAliasNumPage().' de '.PDF::getAliasNbPages() );
        });

        $prefactura = Prefactura::find($id);
        
        PDF::AddPage();

        #Cell(w, h = 0, txt = '', border = 0, ln = 0, align = '', fill = 0, link = nil, stretch = 0, ignore_min_height = false, calign = 'T', valign = 'M')
        #MultiCell(w, h, txt, border = 0, align = 'J', fill = 0, ln = 1, x = '', y = '', reseth = true, stretch = 0, ishtml = false, autopadding = true, maxh = 0)
        
        PDF::SetFont('helvetica', 'B', 8);
        $y = 33;
        $x = 15;
        $space = 4;
        PDF::MultiCell(0, 0, 'CLIENTE:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'NIT:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'TELÉFONO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'DIRECCIÓN:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'CIUDAD:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'EMAIL FACT.:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        $y = 33;
        $x = 35;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $prefactura->cliente->nombre, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $prefactura->cliente->nit, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $prefactura->cliente->telefono, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $prefactura->cliente->direccion, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $prefactura->cliente->ciudad, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $prefactura->email_factura, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        PDF::SetFont('helvetica', 'B', 8);
        $y = 33;
        $x = 140;
        PDF::MultiCell(0, 0, 'FECHA:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'VENCIMIENTO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'ELABORÓ:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'OC:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'FACTURA ELECTRÓNICA:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
       
        $elaboro = explode(" ", $prefactura->usuario->name)[0];
        $y = 33;
        $x = 180;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $prefactura->fecha, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $prefactura->vencimiento, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $elaboro, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, '', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, '___________', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        $styleLine = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');

        PDF::Line(14, 57, 202, 57, $styleLine);
        PDF::Line(14, 57.5, 202, 57.5, $styleLine);
        PDF::SetFont('helvetica', '', 7);
 

        $tbl = <<<EOD
        <style>
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
            .text-centro{
                text-align:center;
            }
            .w-7{
                width:7%;
            }
         
            .w-14{
                width:14%;
            }
            .w-44{
                width:44%;
            }
            .w-72{
                width:72%;
            }
        </style>

        <table cellspacing="0" cellpadding="3" border="1">
            <tr>
                <th class="w-7">Código</th>
                <th class="w-44">Descripción</th>
                <th class="w-14">Precio unit.</th>
                <th class="w-7">Cant.</th>
                <th class="w-7">%Desc.</th>
                <th class="w-7">% IVA</th>
                <th class="w-14">Subtotal</th>
            </tr>
        EOD;  
        foreach($prefactura->detalles as $detalle)
        {
            $producto   = $detalle->producto;
            $precio     = number_format($producto->precio,0,',', '.');
            $subtotal   = number_format($detalle->total,0,',', '.');
            $tbl .= <<<EOD
            <tr>
                <td>$producto->codigo</td>
                <td>$producto->nombre</td>
                <td class="text-derecha">$ $precio </td>
                <td class="text-centro">$detalle->cantidad</td>
                <td class="text-derecha">$detalle->porc_descuento %</td>
                <td class="text-derecha">$detalle->iva %</td>
                <td class="text-derecha">$ $subtotal</td>
               
            </tr>
            EOD;
        }
        $tbl .= <<<EOD
            </table>
        EOD;
        
        $prefactura_subtotal= number_format($prefactura->subtotal,0,',', '.');
        $prefactura_iva     = number_format($prefactura->iva,0,',', '.');
        $prefactura_total   = number_format($prefactura->total,0,',', '.');
        
        $tbl_total = <<<EOD
        <style>
            .w-14{
                width:14%;
            }
            .w-72{
                width:72%;
            }
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .border{
                border:1px solid #CCC;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
        </style>
            <table cellpadding="4">
                <tr>
                    <td class="w-72" rowspan="3" ><b>Observaciones:</b> $prefactura->observaciones </td>
                    <th class="w-14 text-derecha border">Subtotal:</th>
                    <td class="w-14 border" >$ $prefactura_subtotal </td>
                </tr>
                <tr>
                    <th class="text-derecha border">IVA:</th>
                    <td class="border">$ $prefactura_iva </td>
                </tr>
                <tr>
                    
                    <th class="text-derecha border">Total:</th>
                    <td class="border">$ $prefactura_total </td>
                </tr>
            </table>
        EOD;
        
        PDF::ln(8);
        PDF::writeHTML($tbl, true, false, false, false, '');
        
        PDF::writeHTML($tbl_total, true, false, false, false, '');
        

        PDF::SetTitle('Prefactura');
        PDF::Output('prefactura.pdf');
    }

    public function cotizacionPdf($id)
    {
        $logo = asset('/imgs/logo_color.jpg');
        PDF::setPrintHeader(true);
        PDF::setPrintFooter(true);

        // set header and footer fonts
        PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

         // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, 32, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        PDF::setJPEGQuality(75);
       
        $cotizacion = Cotizacion::find($id);
        define('CODIGO', $cotizacion->codigo);
       
        PDF::setHeaderCallback(function($headerpdf) {
            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');
            $logo   = asset('/imgs/logo_color.jpg');
            $codigo = CODIGO;
            $w      = 25;
            $h      = $w/1.5;

            $headerpdf->Image($logo,15,10, $w, $h);
            $headerpdf->SetFont('helvetica', 'B', 14);
            $headerpdf->Cell(0, 20, 'Cotizacion #   '.$codigo. '  ', 0, false, 'R', 0, '', 0, false, 'T', 'R');
            $headerpdf->Line(14, 30, 202, 30, $style);
            $headerpdf->Line(14, 30.5, 202, 30.5, $style);

            $headerpdf->SetFont('helvetica', 'B', 8);
            $headerpdf->MultiCell(100, 4, 'DISTRIBUCIONES MBL', 0, 'C', 0, 1,'45', '10', true,  0, false, true, 0);
            $headerpdf->SetFont('helvetica', 'R', 7);
            $headerpdf->MultiCell(100, 4, 'Operado por CSG S.A.S', 0, 'C', 0, 1,'45', '14', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Nit: 900 976 766-6', 0, 'C', 0, 1,'45', '18', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Tel: 300 4525982 / distribuciones.mbl@gmail.com / Medellín', 0, 'C', 0, 1,'45', '22', true,  0, false, true, 0);

            $headerpdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $headerpdf->RoundedRect(178 , 10, 23, 10, 1.50, '1111');
            $headerpdf->SetFont('helvetica', 'R', 9);
            $headerpdf->Text(177, 25,'Página: '.PDF::getAliasNumPage().' de '.PDF::getAliasNbPages() );
        });
        
        PDF::AddPage();
        //$usuario = $cotizacion->usuario->nombre;
        #Cell(w, h = 0, txt = '', border = 0, ln = 0, align = '', fill = 0, link = nil, stretch = 0, ignore_min_height = false, calign = 'T', valign = 'M')
        #MultiCell(w, h, txt, border = 0, align = 'J', fill = 0, ln = 1, x = '', y = '', reseth = true, stretch = 0, ishtml = false, autopadding = true, maxh = 0)
        
        PDF::SetFont('helvetica', 'B', 8);
        $y = 33;
        $x = 15;
        $space = 4;
        PDF::MultiCell(0, 0, 'CLIENTE:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'NIT:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'TELÉFONO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'DIRECCIÓN:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'CIUDAD:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
       
        $y = 33;
        $x = 35;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $cotizacion->cliente->nombre, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $cotizacion->cliente->nit, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $cotizacion->cliente->telefono, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $cotizacion->cliente->direccion, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $cotizacion->cliente->ciudad, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $cotizacion->cliente->email, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        PDF::SetFont('helvetica', 'B', 8);
        $y = 37;
        $x = 110;
        PDF::MultiCell(0, 0, 'FECHA:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'VALIDEZ:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'CONTACTO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'EMAIL:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
       
        //$elaboro = explode(" ", $cotizacion->usuario->name)[0];
        
        if( $cotizacion->contacto_id !== 0 && !is_null($cotizacion->contacto_id) )
        {
            $contacto_c = Contactos_cliente::whereId($cotizacion->contacto_id)->first();
            $contacto = $contacto_c->nombre;
            $email    = $contacto_c->email;
        }
        else
        {
            $contacto   = NUll;
            $email      = NULL;
        }

        $y = 37;
        $x = 130;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $cotizacion->fecha, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $cotizacion->validez.' días', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $contacto, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $email, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        $styleLine = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');

        PDF::Line(14, 57, 202, 57, $styleLine);
        PDF::Line(14, 57.5, 202, 57.5, $styleLine);
        PDF::SetFont('helvetica', '', 7);
 
        $tbl = <<<EOD
        <style>
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
            .text-centro{
                text-align:center;
            }
            .text-gray{
                color:#252f3f;
                font-size:6pt;
            }
            .w-7{
                width:7%;
            }
         
            .w-10{
                width:10%;
            }
            .w-11{
                width:11%;
            }
            .w-44{
                width:44%;
            }
            .w-72{
                width:72%;
            }
        </style>

        <table cellspacing="0" cellpadding="3" border="1">
            <tr>
                <th class="w-7">Código</th>
                <th class="w-44">Descripción</th>
                <th class="w-7">Tiempo Entrega</th>
                <th class="w-10">Precio unit.</th>
                <th class="w-7">Cantidad</th>
                <th class="w-7">%Desc.</th>
                <th class="w-7">% IVA</th>
                <th class="w-11">Subtotal</th>
            </tr>
        EOD;  
        foreach($cotizacion->detalles as $detalle)
        {
            $producto   = $detalle->producto;
            $precio     = number_format($producto->precio,0,',', '.');
            $subtotal   = number_format($detalle->total,0,',', '.');
            $tbl .= <<<EOD
            <tr>
                <td>$producto->codigo</td>
                <td>$producto->nombre <br /><span class="text-gray"><b>Marca:</b>$producto->marca / <b>Presentación:</b>$producto->presentacion</span>
                </td>
                <td>$detalle->tiempo días</td>
                <td class="text-derecha">$ $precio </td>
                <td class="text-centro">$detalle->cantidad</td>
                <td class="text-derecha">$detalle->porc_descuento %</td>
                <td class="text-derecha">$detalle->iva %</td>
                <td class="text-derecha">$ $subtotal</td>
               
            </tr>
            EOD;
        }
        $tbl .= <<<EOD
            </table>
        EOD;
        
        $cotizacion_subtotal= number_format($cotizacion->subtotal,0,',', '.');
        $cotizacion_iva     = number_format($cotizacion->iva,0,',', '.');
        $cotizacion_total   = number_format($cotizacion->total,0,',', '.');
        
        $tbl_total = <<<EOD
        <style>
            .w-14{
                width:14%;
            }
            .w-72{
                width:72%;
            }
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .border{
                border:1px solid #161e2e;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
        </style>
            <table cellpadding="4" >
                <tr>
                    <th class="text-derecha border">Subtotal:</th>
                    <td class=" border" >$ $cotizacion_subtotal </td>
                </tr>
                <tr>
                    <th class="text-derecha border">IVA:</th>
                    <td class="border">$ $cotizacion_iva </td>
                </tr>
                <tr>
                    
                    <th class="text-derecha border">Total:</th>
                    <td class="border">$ $cotizacion_total </td>
                </tr>
            </table>
        EOD;
        
        
        $tbl_total_container = <<<EOD
        <style>
            .w-28{
                width:28%;
            }
            .w-72{
                width:72%;
            }
            .text-8{
                font-size:8pt ;
            }
        </style>
            <table class="text-8">
                <tr>
                    <td class="w-72"><b>CONDICIONES COMERCIALES:</b><br /> $cotizacion->condiciones</td>
                    <td class="w-28" >$tbl_total</td>
                </tr>
            </table>
            <table class="text-8">
                <tr>
                    <td><b>OBSERVACIONES:</b> <br/> $cotizacion->observaciones</td>                   
                </tr>
            </table>
        EOD;

        $tbl_notas = <<<EOD
        <style>
            .w-28{
                width:28%;
            }
            .w-72{
                width:72%;
            }
        </style>
            <table>
                <tr>
                    <td><b class="text-8">NOTAS:</b> 
                        <p>*** EL TIEMPO DE ENTREGA ESTÁ DADO EN DÍAS HÁBILES.
                        <br/>*** PAGO: TRANSFERENCIA ELECTRÓNICA Y/O CONSIGNACIÓN NACIONAL. 
                        <br/>*** EN CASO DE CONSIGNACIÓN CON CHEQUE, DEBE ESPERAR AL CANJE DE ESTE.
                        <br/>*** LA VIGENCIA DE PRECIOS SE MANTIENE POR EL TIEMPO ESTIPULADO EN LA VALIDEZ DE ESTA OFERTA Ó HASTA QUE EL INVENTARIO DE ESTE SE AGOTE.
                        <br/>*** LOS PEDIDOS DEBEN REALIZARSE MEDIANTE UNA ORDEN DE COMPRA.
                        <br/>*** PRECIOS SUJETOS A CAMBIO.
                        <br/>*** CUALQUIER INQUIETUD SOBRE LA PRESENTE OFERTA SERÁ ATENDIDA POR SU ASESOR COMERCIAL.
                        </p>
                    </td>
                </tr>
            </table>
        EOD;

        PDF::ln(10);
        PDF::writeHTML($tbl, true, false, false, false, '');
                    
        PDF::writeHTML($tbl_total_container, true, false, false, false, '');
        PDF::writeHTML($tbl_notas, true, false, false, false, '');

        $usuario = $cotizacion->usuario;
        PDF::SetFont('helvetica', 'B', 9);
        PDF::Cell(0, 0, 'Cordialmente:', 0, 1, '', 0, null, 0, false, 'T', 'M');
        PDF::ln(4);
        PDF::SetFont('helvetica', 'I', 9);
        PDF::Cell(0, 0, $usuario->name, 0, 1, '', 0, null, 0, false, 'T', 'M');
        PDF::Cell(0, 0, $usuario->cargo, 0, 1, '', 0, null, 0, false, 'T', 'M');
        PDF::Cell(0, 0, $usuario->telefono, 0, 1, '', 0, null, 0, false, 'T', 'M');
        PDF::Cell(0, 0, $usuario->email, 0, 1, '', 0, null, 0, false, 'T', 'M');
              
        PDF::SetTitle('Cotizacion-'.$cotizacion->codigo.' ('.$cotizacion->cliente->nombre.')');
        PDF::Output('Cotizacion-'.$cotizacion->codigo.' ('.$cotizacion->cliente->nombre.').pdf');
    }

    public function ordenPdf($id)
    {
        $logo = asset('/imgs/logo_color.jpg');
        PDF::setPageOrientation( 'P');
        PDF::unhtmlentities('ISO-8859-1');
        PDF::setPrintHeader(true);
        PDF::setPrintFooter(true);

        // set header and footer fonts
        PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

         // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, 32, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        PDF::setJPEGQuality(75);

        $orden = Orden::find($id);
        define ('CODIGO', $orden->codigo);
        
        PDF::setHeaderCallback(function($headerpdf) {
            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');
            $logo   = asset('/imgs/logo_color.jpg');
            $codigo = CODIGO;
            $w      = 25;
            $h      = $w/1.5;

            $headerpdf->Image($logo,15,10, $w, $h);
            $headerpdf->SetFont('helvetica', 'B', 14);
            $headerpdf->Cell(0, 20, 'Orden de compra #   '.$codigo. '  ', 0, false, 'R', 0, '', 0, false, 'T', 'R');
            $headerpdf->Line(14, 34, 202, 34, $style);
            $headerpdf->Line(14, 34.5, 202, 34.5, $style);

            $headerpdf->SetFont('helvetica', 'B', 8);
            $headerpdf->MultiCell(100, 4, 'ESISCO', 0, 'C', 0, 1,'40', '10', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Evaluación en Sistemas de Control ', 0, 'C', 0, 1,'40', '14', true,  0, false, true, 0);
            $headerpdf->SetFont('helvetica', 'R', 7);
            $headerpdf->MultiCell(100, 4, 'Nit: 900 976 766-6', 0, 'C', 0, 1,'40', '18', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Tel:(4) 5901247 ', 0, 'C', 0, 1,'40', '22', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'servicios@esisco.com.co', 0, 'C', 0, 1,'40', '26', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Medellín', 0, 'C', 0, 1,'40', '30', true,  0, false, true, 0);

            $headerpdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $headerpdf->RoundedRect(175, 10, 25, 10, 1.50, '1111');
            $headerpdf->SetFont('helvetica', 'R', 9);
            $headerpdf->Text(177, 25,'Página: '.PDF::getAliasNumPage().' de '.PDF::getAliasNbPages() );
        });

        $orden = Orden::find($id);
        
        PDF::AddPage();

        #Cell(w, h = 0, txt = '', border = 0, ln = 0, align = '', fill = 0, link = nil, stretch = 0, ignore_min_height = false, calign = 'T', valign = 'M')
        #MultiCell(w, h, txt, border = 0, align = 'J', fill = 0, ln = 1, x = '', y = '', reseth = true, stretch = 0, ishtml = false, autopadding = true, maxh = 0)
        
        PDF::SetFont('helvetica', 'B', 8);
        $y = 37;
        $x = 15;
        $space = 4;
        PDF::MultiCell(0, 0, 'PROVEEDOR:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'NIT:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'TELÉFONO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'DIRECCIÓN:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'CIUDAD:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'EMAIL:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        $y = 37;
        $x = 35;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $orden->proveedor->nombre, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->proveedor->nit, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->proveedor->telefono, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->proveedor->direccion, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->proveedor->ciudad, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->proveedor->email, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        PDF::SetFont('helvetica', 'B', 8);
        $y = 37;
        $x = 120;
        PDF::MultiCell(0, 0, 'FECHA:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'FORMA DE PAGO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'APROBADO POR:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'COTIZACIÓN:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'DIRECCIÓN:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'EMAIL:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
       
        $aprobo = User::find($orden->aprobo);
        $y = 37;
        $x = 160;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $orden->fecha, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->forma_pago, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $aprobo->name, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $orden->cotizacion, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'Calle 49AA#77C-01', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'servicioas@esisco.com.co', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        $styleLine = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');

        PDF::Line(14, 63, 202, 63, $styleLine);
        PDF::Line(14, 63.5, 202, 63.5, $styleLine);
        PDF::SetFont('helvetica', '', 7);
 

        $tbl = <<<EOD
        <style>
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
            .text-centro{
                text-align:center;
            }
            .w-7{
                width:7%;
            }
         
            .w-12{
                width:12%;
            }
            .w-40{
                width:40%;
            }
            .w-72{
                width:72%;
            }
        </style>

        <table cellspacing="0" cellpadding="3" border="1">
            <tr>
                <th class="w-14">Referencia</th>
                <th class="w-40">Descripción</th>
                <th class="w-12">Precio unit.</th>
                <th class="w-7">Cant.</th>
                <th class="w-7">%Desc.</th>
                <th class="w-7">% IVA</th>
                <th class="w-12">Subtotal</th>
            </tr>
        EOD;  
        foreach($orden->detalles as $detalle)
        {
            $producto   = $detalle->producto;
            $precio     = number_format($detalle->costo,0,',', '.');
            $subtotal   = number_format($detalle->total,0,',', '.');
            $tbl .= <<<EOD
            <tr>
                <td>$producto->referencia</td>
                <td>$producto->nombre
                    <br/><span style="font-size:6pt" >Marca:$producto->marca --- Presentación:$producto->presentacion </span>
                </td>
                <td class="text-derecha">$ $precio </td>
                <td class="text-centro">$detalle->cantidad</td>
                <td class="text-derecha">$detalle->porc_descuento %</td>
                <td class="text-derecha">$detalle->iva %</td>
                <td class="text-derecha">$ $subtotal</td>
               
            </tr>
            EOD;
        }
        $tbl .= <<<EOD
            </table>
        EOD;
        
        $orden_subtotal= number_format($orden->subtotal,0,',', '.');
        $orden_iva     = number_format($orden->iva,0,',', '.');
        $orden_total   = number_format($orden->total,0,',', '.');
        
        $tbl_total = <<<EOD
        <style>
            .w-14{
                width:14%;
            }
            .w-72{
                width:72%;
            }
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .border{
                border:1px solid #CCC;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
        </style>
            <table cellpadding="4">
                <tr>
                    <td class="w-72" rowspan="3" ><b>Observaciones:</b> $orden->observaciones </td>
                    <th class="w-14 text-derecha border">Subtotal:</th>
                    <td class="w-14 border" >$ $orden_subtotal </td>
                </tr>
                <tr>
                    <th class="text-derecha border">IVA:</th>
                    <td class="border">$ $orden_iva </td>
                </tr>
                <tr>
                    
                    <th class="text-derecha border">Total:</th>
                    <td class="border">$ $orden_total </td>
                </tr>
            </table>
        EOD;
        
        PDF::ln(8);
        PDF::writeHTML($tbl, true, false, false, false, '');
        
        PDF::writeHTML($tbl_total, true, false, false, false, '');
        

        PDF::SetTitle('Orden de compra-'.$orden->codigo);
        PDF::Output($orden->codigo.'-('.$orden->proveedor->nombre.').pdf');
    }

    public function remisionPdf($id)
    {

        $logo = asset('/imgs/logo_bn.jpg');
        PDF::setPrintHeader(true);
        PDF::setPrintFooter(true);

        // set header and footer fonts
        PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

         // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, 32, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        PDF::setJPEGQuality(75);

        $remision = Remision::find($id);
        define ('CODIGO', $remision->codigo);
        
        PDF::setHeaderCallback(function($headerpdf) {
            #$remision = Remision::find($id);
            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');
            $logo   = asset('/imgs/logo_bn.jpg');
            $codigo = CODIGO;
            $w      = 25;
            $h      = $w/1.5;

            $headerpdf->Image($logo,15,10, $w, $h);
          
            $headerpdf->SetFont('helvetica', 'B', 7);
            $headerpdf->MultiCell(100, 4, 'ESISCO', 0, 'C', 0, 1,'40', '10', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Evaluación en Sistemas de Control ', 0, 'C', 0, 1,'40', '14', true,  0, false, true, 0);
            $headerpdf->SetFont('helvetica', 'R', 7);
            $headerpdf->MultiCell(100, 4, 'Nit: 900 976 766-6', 0, 'C', 0, 1,'40', '18', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'Tel: (4) 5901247', 0, 'C', 0, 1,'40', '22', true,  0, false, true, 0);
            $headerpdf->MultiCell(100, 4, 'servicios@esisco.com.co / Medellín', 0, 'C', 0, 1,'40', '26', true,  0, false, true, 0);
            
            $headerpdf->SetFont('helvetica', 'B', 14);

            $headerpdf->MultiCell(0, 0, 'Remisión #', 0, 'L', 0, 1, 130, 12 , true, 0, false, true, 0);
            $headerpdf->MultiCell(0, 0, $codigo, 0, 'L', 0, 1, 160, 12 , true, 0, false, true, 0);

        
            $headerpdf->Line(14, 30, 202, 30, $style);
            $headerpdf->Line(14, 30.5, 202, 30.5, $style);

            $headerpdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $headerpdf->RoundedRect(158, 10, 45, 10, 1.50, '1111');
            $headerpdf->SetFont('helvetica', 'R', 9);
            $headerpdf->Text(177, 25,'Página: '.PDF::getAliasNumPage().' de '.PDF::getAliasNbPages() );
        });

        $remision = Remision::find($id);
        
        PDF::AddPage();

        #Cell(w, h = 0, txt = '', border = 0, ln = 0, align = '', fill = 0, link = nil, stretch = 0, ignore_min_height = false, calign = 'T', valign = 'M')
        #MultiCell(w, h, txt, border = 0, align = 'J', fill = 0, ln = 1, x = '', y = '', reseth = true, stretch = 0, ishtml = false, autopadding = true, maxh = 0)
        
        PDF::SetFont('helvetica', 'B', 8);
        $y = 33;
        $x = 15;
        $space = 4;
        PDF::MultiCell(0, 0, 'CLIENTE:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'NIT:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'TELÉFONO:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'DIRECCIÓN:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'CIUDAD:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        $y = 33;
        $x = 35;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $remision->cliente->nombre, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $remision->cliente->nit, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $remision->cliente->telefono, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $remision->cliente->direccion, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $remision->cliente->ciudad, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        PDF::SetFont('helvetica', 'B', 8);
        $y = 33;
        $x = 140;
        PDF::MultiCell(0, 0, 'FECHA:', 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'ELABORÓ:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'APROBÓ:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, 'FACTURA:', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
       
        $elaboro = explode(" ", $remision->usuario->name)[0];
        $y = 33;
        $x = 156;
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(0, 0, $remision->fecha, 0, 'L', 0, 1, $x, $y, true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $remision->reviso, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, $remision->aprobo, 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);
        PDF::MultiCell(0, 0, '___________', 0, 'L', 0, 1, $x, ($y=$y+$space), true, 0, false, true, 0);

        $styleLine = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '3,1');

        PDF::Line(14, 53, 202, 53, $styleLine);
        PDF::Line(14, 53.5, 202, 53.5, $styleLine);
        PDF::SetFont('helvetica', '', 7);
 

        $tbl = <<<EOD
        <style>
            th{
                background-color:#EEE;
                font-weight: bold;
            }
            .text-derecha{
                text-align:right;
            }
            .text-izquierda{
                text-align:left;
            }
            .text-centro{
                text-align:center;
            }
            .w-5{
                width:5%;
            }
         
            .w-7{
                width:7%;
            }
         
            .w-12{
                width:12%;
            }
         
            .w-14{
                width:14%;
            }
            .w-28{
                width:28%;
            }
            .w-40{
                width:41%;
            }
        </style>

        <table cellspacing="0" cellpadding="3" border="1">
            <tr>
                <th class="w-5">Item</th>
                <th class="w-7">Código</th>
               
                <th class="w-40">Descripción</th>
                <th class="w-7">Cantidad</th>
                <th class="w-40">Observaciones</th>
            </tr>
        EOD;  
        $item =0;
        foreach($remision->detalles as $detalle)
        {
            $producto   = $detalle->producto;
            $item++;
            $tbl .= <<<EOD
            <tr>
                <td class="text-centro">$item</td>
                <td>$producto->codigo</td>
                
                <td>$producto->nombre
                    <br /><span style="font-size:6pt">Marca:$producto->marca / Presentación:$producto->presentacion </span>
                </td>
                <td class="text-centro">$detalle->cantidad</td>
                <td class="">$detalle->lote</td>
            </tr>
            EOD;
        }
        $tbl .= <<<EOD
            </table>
        EOD;
        
        $firmas = <<<EOD
        <style>
            
            .w-7{
                width:7%;
            }
         
            .w-12{
                width:12%;
            }
         
            .w-20{
                width:20%;
            }
            .w-32{
                width:32%;
            }
            .w-40{
                width:40%;
            }
        </style>

        <table cellspacing="0" cellpadding="3">
            <tr>
                <td class="w-20">_________________________</td>
                <td class="w-20">_________________________ </td>
                <td class="w-40"></td>
            </tr>
            <tr>
                <th class="w-20" style="font-size:10pt">Firma despacho</th>
                <th class="w-20" style="font-size:10pt">Firma de recibido </th>
                <th class="w-40" style="font-size:10pt">Observaciones del cliente</th>
            </tr>
        EOD;

        PDF::ln(8);
        PDF::writeHTML($tbl, true, false, false, false, '');
     
        PDF::ln(4);
        PDF::writeHTML($firmas, true, false, false, false, '');
     
        PDF::SetTitle('Remision'.$remision->codigo);
        PDF::Output($remision->codigo.'-'.$remision->cliente->nombre.'.pdf');
    }
}
