<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Solicitud;

class MenuPrincipal extends Component
{
    public $solicitudesNuevas;

    protected $listeners = ['nuevaSolicitud'=>'$refresh'];

    public function render()
    {   
        $this->solicitudesNuevas = Solicitud::where('estado', 1)->count();
        return view('livewire.menu-principal');
    }

}
