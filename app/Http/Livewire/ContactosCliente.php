<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Contactos_cliente;
use Illuminate\Support\Facades\Schema;


class ContactosCliente extends Component
{
    public $cliente_id;
    public $cliente;
    public $contacto;
    public $contacto_id;
    public $nombre, $cargo, $telefono, $email;
    public $agregar_enable;
    public $agregar;

    public function mount()
    {
        $this->cliente_id       = $this->cliente->id;
        $this->agregar_enable   = true;
        $this->agregar          = false;
    }

    public function render()
    {
        $contactos = Contactos_cliente::where('cliente_id', $this->cliente_id)->get();
        return view('livewire.contactos-cliente', [
            'contactos'=>$contactos,
        ]);
    }

    public function listar($cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }
    
    public function editarContacto($contacto_id)
    {
        $contacto = Contactos_cliente::find($contacto_id);
        $this->contacto         = $contacto;
        $this->nombre           = $contacto->nombre;
        $this->cargo            = $contacto->cargo;
        $this->telefono         = $contacto->telefono;
        $this->email            = $contacto->email;
        $this->agregar_enable   = false;
    }
    
    public function updateContacto()
    {
        Schema::disableForeignKeyConstraints();
        if($this->agregar)
        {
            Contactos_cliente::create([
                'cliente_id'=> $this->cliente_id,
                'nombre'    => $this->nombre,
                'telefono'  => $this->telefono,
                'cargo'     => $this->cargo,
                'email'     => $this->email,
            ]);  
        }
        else
        {
            $contacto       = $this->contacto;
            $contacto->update([
                'nombre'    => $this->nombre,
                'telefono'  => $this->telefono,
                'cargo'     => $this->cargo,
                'email'     => $this->email,
            ]);
        }
        $this->agregar_enable   = true;
        $this->nombre   = '';
        $this->telefono = '';
        $this->cargo    = '';
        $this->email    = '';
        $this->contacto = '';
        $this->agregar  = false;
        Schema::enableForeignKeyConstraints();

    }

    public function agregarContacto()
    {
        $this->agregar=true;
       /* Contactos_cliente::create([
            'cliente_id'=> $this->cliente_id,
            'nombre'    => $this->nombre,
            'telefono'  => $this->telefono,
            'cargo'     => $this->cargo,
            'email'     => $this->email,
        ]);*/
       // $this->agregar_enable   = true;
       /* 
        $this->nombre   = '';
        $this->telefono = '';
        $this->cargo    = '';
        $this->email    = '';
        $this->contacto = '';
        */

    }
    public function cancelar()
    {
        $this->agregar_enable   = true;
        $this->nombre   = '';
        $this->telefono = '';
        $this->cargo    = '';
        $this->email    = '';
        $this->contacto = '';

    }
}
