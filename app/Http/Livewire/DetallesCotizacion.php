<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Producto;
use App\Models\Cotizacion;
use App\Models\Detalles_cotizacion as Detalles;

class DetallesCotizacion extends Component
{
    public $modalProducto;
    public $productos;
    public $producto_sel;
    public $producto_id;
    public $producto;
    public $query;
    public $size;
    public $cotizacion;
    public $detalles;
    public $item_id;
    public $grantotal;
    public $subtotal;
    public $ivatotal;
    public $observaciones;
    public $editarProducto;
    public $item;
    public $condiciones;

   // protected $queryString = ['search'=>['except'=>''], 'searchCliente'=>['except'=>'']];
   protected $listeners = ['eliminarItem'];

    public function mount(Cotizacion $cotizacion)
    {
        $this->modalProducto= false;
        $this->productos    = Producto::all(['id', 'nombre', 'precio']);
        $this->producto     = null;
        $this->producto_sel = null;
        $this->producto_id  = null;
        $this->query        = '';
        $this->size         = 0;
        $this->cotizacion   = $cotizacion;
        $this->observaciones= $this->cotizacion->observaciones;
        $this->condiciones  = $this->cotizacion->condiciones;
        $this->item_id      = null;
        $this->grantotal    = 0;
        $this->ivatotal     = 0;
        $this->subtotal     = 0;
        $this->editarProducto= false;
        $this->item         = null;
    }

    public function render()
    {
        $this->cotizacion = Cotizacion::find($this->cotizacion->id);
        $this->granTotal();
        return view('livewire.detalles-cotizacion', 
            ['cotizacion'=>$this->cotizacion]
        );
    }

    public function updatedQuery()
    {
        $this->productos    = Producto::where('nombre', 'like', '%'.$this->query.'%')->get();
        $this->size         = $this->productos->count();
        if ($this->size > 5) $this->size = 5;
    }

    public function buscarProductos()
    {
        $this->query            = '';
        $this->size             = 0;
        $this->producto_sel     = null;
        $this->editarProducto   = false;
        $this->producto_id      = null;
        $this->modalProducto    = true;
        $this->dispatchBrowserEvent('modalProductos');
    }

    public function editarItem($item, $producto_id)
    {
        $this->item          = $item;
        $this->producto_id   = $producto_id;
        $this->editarProducto= true;
        $this->modalProducto = true;
        $this->dispatchBrowserEvent('modalProductos');

    }

    public function modificarProducto()
    {
        $this->query            = '';
        $this->size             = 0;
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::whereId($this->item)->update([
            'cotizacion_id' => $this->cotizacion->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'porc_descuento'=> 0,
            'descuento'     => 0,
            'iva'           => $this->producto->iva,
            'total_iva'     => $this->producto->precio * ($this->producto->iva) / 100,
            'tiempo'        => $this->producto->tiempo,
            'total'         => $this->producto->precio,
        ]);
        $this->granTotal();
        $this->cotizacion = Cotizacion::find($this->cotizacion->id);
        $this->producto_id = null;
    }

    public function seleccionarProducto($producto_id)
    {
        $this->producto_sel = Producto::find($producto_id);
        $this->query    = '';
        $this->size     = 0;
        $this->producto_id = $producto_id;
    }
    
    public function agregarProducto()
    {
        $this->query            = '';
        $this->size             = 0;
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::create([
            'cotizacion_id' => $this->cotizacion->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'porc_descuento'=> 0,
            'descuento'     => 0,
            'iva'           => $this->producto->iva,
            'total_iva'     => $this->producto->precio * ($this->producto->iva) / 100,
            'tiempo'        => $this->producto->tiempo,
            'total'         => $this->producto->precio,
        ]);
        $this->granTotal();
        $this->cotizacion = Cotizacion::find($this->cotizacion->id);
    }

    public function updateDetalles()
    {
        $this->cotizacion->update([
            'observaciones' => $this->observaciones,
            'condiciones'   => $this->condiciones,
            'subtotal'      => $this->subtotal,
            'iva'           => $this->ivatotal,
            'total'         => $this->grantotal,
        ]); 
        $this->cotizacion = Cotizacion::find($this->cotizacion->id);
        $this->emit('saved', $this->cotizacion->id);
    }

    public function eliminarItem($item)
    {
        Detalles::destroy($item);
        $this->granTotal();
        $this->cotizacion = Cotizacion::find($this->cotizacion->id);
    }
    
    public function cerrarModal()
    {
        $this->modalProducto    = false;
        $this->editarProducto   = true;
        $this->producto_sel     = null;
        $this->query            = '';
        $this->size             = 0;
    }

    public function setDescuento($descuento)
    {
        $item = Detalles::find($this->item_id); 
        $item->update([
            'porc_descuento'=>$descuento,
            'descuento'=>$descuento,
        ]);
        $this->producto = Producto::find($item->producto_id);
        $this->totalItem($item->cantidad, $descuento);
        $this->granTotal();
    }

    public function setCantidad($cantidad)
    {
        $cantidad       = $cantidad;
        $cantidad       = empty($cantidad) ? 0: $cantidad;
        $item           = Detalles::find($this->item_id);
        $item->update([
            'cantidad'=>$cantidad,
        ]);
        $this->producto = Producto::find($item->producto_id);
        $this->totalItem($cantidad, $item->descuento);
        $this->granTotal();
    }

    public function setTiempo($tiempo)
    {
        $tiempo         = empty($tiempo) ? 0:$tiempo;
        $item           = Detalles::find($this->item_id);
        $item->update([
            'tiempo'=>$tiempo,
        ]);
        $this->producto = Producto::find($item->producto_id);       
    }

    public function totalItem($cantidad, $desc)
    {
        $descuento  = $desc/100;
        $precio     = $this->producto->precio;
        $totalItem  = $cantidad * ($precio*(1-$descuento));
        $iva        = $totalItem * ($this->producto->iva) / 100;
        $item       = Detalles::find($this->item_id); 
        $item->update([
            'total'=>$totalItem,
            'total_iva'=>$iva,
        ]);
    }

    public function granTotal()
    {
        $detalles       = Detalles::where('cotizacion_id', $this->cotizacion->id)->get();
        $this->subtotal = $detalles->sum('total'); 
        $this->ivatotal = $detalles->sum('total_iva'); 
        $this->grantotal=$this->subtotal + $this->ivatotal; 

        $this->cotizacion->update([
            'subtotal'      => $this->subtotal,
            'iva'           => $this->ivatotal,
            'total'         => $this->grantotal,
        ]); 
        $this->cotizacion = Cotizacion::find($this->cotizacion->id);
    }
}

