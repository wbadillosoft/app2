<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\UpdatesUserPasswords;
use Actions\Fortify\PasswordValidationRules;

class UserEdit extends Component
{
    public $name, $email, $cargo, $rol, $status, $user, $password, $email_cotizacion, $telefono;
    
    protected $rules = [
        'password' => 'required|min:8',
        'name' => 'required',
        'email' => 'required|email',
    ];
    protected $messages = [
        'password.required' => 'Este campo no debe estar vacío.',
        'name.required'     => 'Este campo no debe estar vacío.',
        'email.required'    => 'Este campo no debe estar vacío.',
        'email.email'       => 'Este campo.',
        
    ];
    public function mount($id)
    {
        $this->user = User::find( $id );
    }

    public function render()
    {
        $this->name     = $this->user->name;
        $this->email    = $this->user->email;
        $this->email_cotizacion    = $this->user->email_cotizacion;
        $this->telefono = $this->user->telefono;
        $this->cargo    = $this->user->cargo;
        $this->rol      = $this->user->rol;
        $this->status   = $this->user->status;
        return view('livewire.user-edit');
    }

    public function updateProfileInformation()
    {
        $user = User::find( $this->user->id );

        $user->update([
            'name'  => $this->name,
            'email' => $this->email,
            'email_cotizacion' => $this->email_cotizacion,
            'telefono' => $this->telefono,
            'cargo' => $this->cargo,
            'rol'   => $this->rol,
            'status'=> $this->status,
        ]);
        
        $this->load($user->id);
    }    
    
    public function load($user_id)
    {
        $this->user = User::find( $user_id );
        $this->name     = $this->user->name;
        $this->email    = $this->user->email;
        $this->email_cotizacion    = $this->user->email_cotizacion;
        $this->cargo    = $this->user->cargo;
        $this->telefono = $this->user->telefono;
        $this->rol      = $this->user->rol;
        $this->status   = $this->user->status;
        session()->flash('mensaje-data', 'Datos actualizados');
        
    }
    public function updatePass()
    {   
        $this->saved = '';
        $this->validate();
        $this->user->forceFill([
            'password' => Hash::make($this->password),
            'copy_pas' => $this->password,
        ])->save();

        $this->password = '';
        session()->flash('mensaje-pass', 'Contraseña actualizada');
       
    }
   
}
