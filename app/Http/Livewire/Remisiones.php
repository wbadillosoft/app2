<?php

namespace App\Http\Livewire;

use App\Models\Remision;
use App\Models\Detalles_Remision;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Contactos_cliente;
use App\Models\User;

use Livewire\WithPagination;

use Livewire\Component;

use Illuminate\Support\Facades\Auth;

class Remisiones extends Component
{
    use WithPagination;

    public $search;
    public $searchCliente;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $remision;
    public $detalles;
    public $clientes;
    public $cliente;
    public $cliente_data;
    public $queryCliente;
    public $cliente_sel;
    public $visible;
    public $hselect;

    protected $queryString = ['search'=>['except'=>''], 'searchCliente'=>['except'=>'']];

    public $cliente_id, $reviso, $usuario, $fecha, $observaciones, $estado, $codigo;
    public $aprobo, $factura;

    protected $rules = [
        'cliente_id' => 'required',
    ];

    protected $validationAttributes = [
        'cliente_id' => 'cliente',
    ];

    protected $messages = [
        'cliente_id.required' => 'Por favor seleccione un cliente',
    ];

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->cliente_id       = null;
        $this->cliente_sel      = '';
        $this->visible          = false;
        $this->hselect          = 0;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        if(!empty($this->cliente_id)){
            $this->cliente_data     = Cliente::find($this->cliente_id); 
        }
        
        return view('livewire.remisiones', [
            'usuarios'    => User::all(),
            'remisiones'  => Remision::where('codigo', 'like', '%'.$this->search.'%')
            ->orWhere('fecha', 'like', '%'. $this->search.'%')
            ->orderBy('id', 'asc')
            ->paginate(30),
        ]);
    }

    public function updatedQueryCliente()
    {
        $this->clientes    = Cliente::where('nombre', 'like', '%'.$this->queryCliente.'%')->get();
        if( $this->clientes->count() > 5 )
            $this->hselect = 'h-32';
        else
            $this->hselect = 'h-'.(8 * $this->clientes->count());
    }

    public function seleccionarCliente($cliente_id)
    {
        $this->cliente          =  Cliente::find($cliente_id);
        $this->queryCliente     = '';
        $this->cliente_id       = $cliente_id;
        $this->visible          = false;  
        $this->reviso    = $this->cliente->reviso; 
    }

    public function editarCliente()
    {
        $this->visible = true;     
    }

    public function formEditar($remision_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $this->clientes         = Cliente::all();
        $remision               = Remision::find($remision_id);
        
        $this->remision         = $remision;
        $this->factura          = $remision->factura;
        $this->aprobo           = $remision->aprobo;
        $this->cliente_id       = $remision->cliente_id; 
        $this->reviso           = $remision->reviso; 
    }

    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
        $this->clientes         = Cliente::all();

        $this->factura          = null;
        $this->aprobo           = null;
        $this->cliente_id       = null; 
        $this->reviso           = null; 

    }

    public function store()
    {
        $this->validate();

        $remision = Remision::create([
            'cliente_id'    => $this->cliente_id,
            'usuario_id'    => Auth::user()->id,
            'fecha'         => date('Y-m-d h:m:i'),
            'reviso'        => User::find($this->reviso)->name,
            'aprobo'        => User::find($this->aprobo)->name,
            'estado'        => 0,
            'factura'       => $this->factura,
            'observaciones' => $this->observaciones,
        ]);
        $remision->update(['codigo' => 'REM'.date('y').str_pad($remision->id, 3, "000", STR_PAD_LEFT)]);
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = false;
        $this->remision         = $remision;
    }
    
    public function updateEnviar()
    {
        $remision = $this->remision;
        $remision->update([
            'estado'   => !$this->remision->estado,
        ]); 
    }

    public function updateEncabezado()
    {
        $this->validate();

        $remision = $this->remision;
        $remision->update([
            'cliente_id'    => $this->cliente_id,
            'reviso'        => User::find($this->reviso)->name,
            'aprobo'        => User::find($this->aprobo)->name,
            'factura'       => $this->factura,
        ]);
        session()->flash('mensaje', 'Datos actualizados');
    }

    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;    
        $this->render();
    }
}
