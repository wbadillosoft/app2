<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Proveedores_por_producto;
use App\Models\Proveedor;


class ProveedoresProducto extends Component
{
    public $producto;
    public $nuevoProveedor;
    public $verProveedor;
    public $proveedor_sel;
   
    public function mount($producto)
    {
        $this->producto=$producto;
        $this->nuevoProveedor = null;
        $this->verProveedor   = false;
        
    }

    public function render()
    {
        //$proveedores_producto = Proveedores_por_producto::where('producto_id', $this->producto)->get();
        return view('livewire.proveedores-producto', [
            'proveedores_producto' => Proveedores_por_producto::where('producto_id', $this->producto)->get(),
            'proveedores' => Proveedor::all(),
        ]);
    }

    public function quitarProveedor($id)
    {
        Proveedores_por_producto::whereId($id)->delete(); 
        session()->flash('mensaje', 'Proveedor removido');
    }
 
    public function agregarProveedor($producto_id)
    {
        $nulo = false;
        if($this->nuevoProveedor==null)
            $nulo = true;
        elseif($this->nuevoProveedor=='0')
            $nulo = true;
        if(!$nulo)
        {
            Proveedores_por_producto::create([
                'producto_id'   => $producto_id,
                'proveedor_id'  => $this->nuevoProveedor,
                'deleted'       => 0,
            ]);
            $this->nuevoProveedor = null;
        }
        else{
            session()->flash('error', 'Seleccione primero un proveedor');
        }
    }

    public function ver_Proveedor($prov_id)
    {
        $this->proveedor_sel= Proveedor::find($prov_id);
        $verProveedor   = true;
    }
}
