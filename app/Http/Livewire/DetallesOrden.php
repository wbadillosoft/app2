<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Producto;
use App\Models\Orden;
use App\Models\Detalles_orden as Detalles;

class DetallesOrden extends Component
{
    public $modalProducto = false;
    public $productos;
    public $producto_sel;
    public $producto_id;
    public $producto;
    public $queryProducto;

    public $orden;
    public $detalles;
    public $item_id;
    public $grantotal;
    public $subtotal;
    public $ivatotal;
    public $observaciones;
    public $editarProducto = false;
    public $item;
    public $producto_eliminar;
    public $costo;
    public $confirmar = false;

    public $item_load;
    
    public function mount()
    {
        $this->item    = null;
        $this->item_id = null;
        $this->item_load=null;
        $this->granTotal();
    }

    public function render()
    {
        $this->observaciones = $this->orden->observaciones;
        //$this->detalles      = Detalles::where('orden_id', $this->orden->id)->get();
        return view('livewire.detalles-orden');
    }
    public function updatedQueryProducto()
    {
        $this->productos    = Producto::where('nombre', 'like', '%'.$this->queryProducto.'%')->get();
    }

    public function buscarProductos()
    {
        $this->queryProducto    = '';
        $this->producto_sel     = null;
        $this->editarProducto   = false;
        $this->modalProducto    = true;
    }

    public function editarItem($item, $producto_id)
    {
        $this->item          = $item;
        $this->producto_sel  = Producto::find($producto_id);
        $this->editarProducto= true;
        $this->modalProducto = true;
    }

    public function modificarProducto()
    {
        $this->queryProducto    = '';
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::whereId($this->item)->update([
            'orden_id' => $this->orden->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'porc_descuento'=> 0,
            'descuento'     => 0,
            'iva'           => $this->producto->iva,
            'total_iva'     => $this->producto->precio * ($this->producto->iva) / 100,
            'total'         => $this->producto->precio,
        ]);
        $this->granTotal();
        $this->orden = Orden::find($this->orden->id);
    }

    public function seleccionarProducto($producto_id)
    {
        $this->producto_sel     = Producto::find($producto_id);
        $this->queryProducto    = '';
        $this->producto_id      = $producto_id;
    }
    
    public function agregarProducto()
    {
        $this->queryProducto    = '';
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::create([
            'orden_id' => $this->orden->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'porc_descuento'=> 0,
            'descuento'     => 0,
            'costo'         => 0,
            'iva'           => $this->producto->iva,
            'total_iva'     => $this->costo * ($this->producto->iva) / 100,
            'total'         => $this->costo,
        ]);
        $this->granTotal();
        $this->orden = Orden::find($this->orden->id);
    }

    public function updateDetalles()
    {
        $this->orden->update([
            'observaciones' => $this->observaciones,
            'subtotal'      => $this->subtotal,
            'iva'           => $this->ivatotal,
            'total'         => $this->grantotal,
            'costo'         => $this->costo,
        ]);   
        $this->orden = Orden::find($this->orden->id);
        session()->flash('mensaje', 'Datos actualizados');
    }

    public function closeConfirmar()
    {
        $this->confirmar = false;   
    }

    public function eliminarItem($item_id)
    {
        $item = Detalles::find($item_id);
        $item->delete();
        $this->granTotal();
        $this->orden    = Orden::find($this->orden->id)->refresh('detalles');
        //$this->detalles = Detalles::where('orden_id', $this->orden->id)->get();
        //return redirect()->to('/compras', ['mostrar_editar'=> true]);
    }
    
    public function cerrarModal()
    {
        $this->modalProducto    = false;
        $this->editarProducto   = true;
        $this->producto_sel     = null;
        $this->queryProducto    = '';
    }

    public function loadItem($item_id)
    {
        $this->item     = Detalles::find($item_id);
        $this->producto = Producto::find($this->item->producto_id);
    }
    
    public function setCosto($costo)
    {
        $costo = empty($costo) ? 0: $costo;
        $this->item->update([
            'costo'=>$costo,
        ]);
        $this->totalItem();
        $this->granTotal();
    }

    public function setDescuento($descuento)
    {
        $descuento = empty($descuento) ? 0: $descuento;
        $this->item->update([
            'porc_descuento'=>$descuento,
            'descuento'=>$descuento,
        ]);
        $this->totalItem();
        $this->granTotal();
    }

    public function setCantidad($cantidad)
    {
        $cantidad = empty($cantidad) ? 0: $cantidad;
        $this->item->update([
            'cantidad'=>$cantidad,
        ]);
        $this->totalItem();
        $this->granTotal();
    }

    public function totalItem()
    {
        $descuento  = ($this->item->descuento)/100;
        $costo      = $this->item->costo;
        $totalItem  = ($this->item->cantidad) * ($costo*(1-$descuento));
        $iva        = $totalItem * ($this->producto->iva) / 100;
        $this->total = $costo;
        $this->item->update([
            'total'=>$totalItem,
            'total_iva'=>$iva,
        ]);
    }

    public function granTotal()
    {
        $detalles       = Detalles::where('orden_id', $this->orden->id)->get();
        $this->subtotal = $detalles->sum('total'); 
        $this->ivatotal = $detalles->sum('total_iva'); 
        $this->grantotal= $this->subtotal + $this->ivatotal;

        $this->orden->update([
            'subtotal'      => $this->subtotal,
            'iva'           => $this->ivatotal,
            'total'         => $this->grantotal,
        ]);   
        $this->orden    = Orden::find($this->orden->id);
    }    

}
