<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Costo;
use App\Models\Proveedor;   

class CostosProducto extends Component
{
    public $producto;
    public $costo;
    public $proveedor;

    public function mount()
    {
        $this->proveedor = null;
        $this->costo     = null;
    }

    public function render()
    {
        return view('livewire.costos-producto',[
            'costos' => Costo::where('producto_id', $this->producto->id)->get(),
            'proveedores' => Proveedor::all(),
        ]);
    }
    public function quitarCosto($id)
    {
        Costo::whereId($id)->delete(); 
        session()->flash('mensaje', 'Costo removido');
    }
 
    public function agregarCosto($producto_id)
    {
        $nulo = false;
        if($this->proveedor==null)
        {
            $nulo = true;
            session()->flash('errorproveedor', 'Seleccione primero un proveedor');
        }
           
        elseif($this->proveedor=='0')
        {
            $nulo = true;
            session()->flash('errorproveedor', 'Seleccione primero un proveedor');
        }
        if($this->costo == null)
        {
            $nulo = true;
            session()->flash('errorcosto', 'Escriba un valor');
        }
        if(!$nulo)
        {
            Costo::create([
                'producto_id'   => $producto_id,
                'costo'         => str_replace('.', '', $this->costo) ,
                'proveedor'     => $this->proveedor,
                'deleted'       => 0,
            ]);
            $this->proveedor = null;
        }
        else{
           // session()->flash('errorcosto', 'Seleccione primero un proveedor');
        }
   
    }
}
