<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Producto;
use App\Models\Remision;
use App\Models\Detalles_Remision as Detalles;

class DetallesRemision extends Component
{
    public $modalProducto;
    public $productos;
    public $producto_sel;
    public $producto_id;
    public $producto;
    public $queryProducto;
    public $remision;
    public $detalles;
    public $item_id;
    public $lote;
    public $observaciones;
    public $editarProducto;
    public $item;
    public $producto_eliminar;
    public $confirmar;

    public $pdf1;

    public function mount(Remision $remision)
    {
        $this->modalProducto    = false;
        $this->productos        = null;
        $this->producto         = null;
        $this->producto_sel     = null;
        $this->producto_id      = null;
        $this->queryProducto    = '';
        $this->remision         = $remision;
        $this->observaciones    = $this->remision->observaciones;
        $this->item_id          = null;
        $this->lote             = null;
        $this->editarProducto   = false;
        $this->item             = null;
        $this->producto_eliminar= null;

        $this->confirmar        = false;
    }
    public function render()
    {
        $this->remision = Remision::find($this->remision->id);
        return view('livewire.detalles-remision',
            ['remision'=>$this->remision]
        );      
    }

    public function updatedQueryProducto()
    {
        $this->productos    = Producto::where('nombre', 'like', '%'.$this->queryProducto.'%')->get();
    }

    public function buscarProductos()
    {
        $this->queryProducto    = '';
        $this->producto_sel     = null;
        $this->editarProducto   = false;
        $this->modalProducto    = true;
    }

    public function editarItem($item, $producto_id)
    {
        $this->item          = $item;
        $this->producto_sel  = Producto::find($producto_id);
        $this->editarProducto= true;
        $this->modalProducto = true;
    }

    public function modificarProducto()
    {
        $this->queryProducto    = '';
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::whereId($this->item)->update([
            'remision_id'   => $this->remision->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'lote'          => null,
 
        ]);
        $this->remision = Remision::find($this->remision->id);
    }

    public function seleccionarProducto($producto_id)
    {
        $this->producto_sel     = Producto::find($producto_id);
        $this->queryProducto    = '';
        $this->producto_id      = $producto_id;
    }
    
    public function agregarProducto()
    {
        $this->queryProducto    = '';
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::create([
            'remision_id' => $this->remision->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'lote'          => null,
        ]);
        $this->remision = Remision::find($this->remision->id);
    }


    public function updateDetalles()
    {
        $this->remision->update([
            'observaciones' => $this->observaciones,
        ]);   
        $this->remision = Remision::find($this->remision->id);
        session()->flash('mensaje', 'Datos actualizados');
    }

    public function closeConfirmar()
    {
        $this->confirmar = false;   
    }
    public function confirmarEliminarItem($item)
    {
        $this->item_id = $item;
        $item = Detalles::find($item);
        $this->producto_eliminar=  $item->producto->nombre;
        $this->confirmar        = true;   
    }

    public function eliminarItem($item)
    {
        $this->confirmar = false; 
        Detalles::destroy($item);
        $this->remision = Remision::find($this->remision->id);
    }
    
    public function cerrarModal()
    {
        $this->modalProducto    = false;
        $this->editarProducto   = true;
        $this->producto_sel     = null;
        $this->queryProducto    = '';
    }

    public function loadItem($item_id)
    {
        $this->item     = Detalles::find($item_id);
    }

    public function setLote($lote)
    {
        $this->lote = $lote;
        $this->item->update([
            'lote'=>$this->lote,
        ]);
    }

    public function setCantidad($cantidad)
    {
        $cantidad = empty($cantidad) ? 0: $cantidad;
        $this->item->update([
            'cantidad'=>$cantidad,
        ]);
    }
  
}
