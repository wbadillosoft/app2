<?php

namespace App\Http\Livewire;

#use Livewire\Component;
use App\Models\Detalles_solicitud as Detalles;

use LivewireUI\Modal\ModalComponent;

class NuevoEquipo extends ModalComponent
{
    public $mantenimiento;
    public $mantenimiento_tipo;
    public $metrologia;
    public $metrologia_tipo;
    public $magnitud;
    public $conformidad;
    public $regla;
    
    public $equipo;
    public $marca;
    public $modelo;
    public $serial;
    public $codigo_interno;
    public $ubicacion;
    public $rango;

    public $descripcion;
    public $solicitudId;
    public $solicitud;
    public $detalles;

    public function render()
    {
        return view('livewire.nuevo-equipo');
    }

    public function agregarEquipo()
    {
        $this->magnitud = $this->magnitud === null ? 'masa':$this->magnitud ;
        $this->conformidad = $this->conformidad === null ? 'no':$this->conformidad ;
        $this->metrologia_tipo = $this->metrologia_tipo === null ? 'calibración':$this->metrologia_tipo ;
        $this->mantenimiento_tipo = $this->mantenimiento_tipo === null ? 'mantenimiento preventivo':$this->mantenimiento_tipo ;

        Detalles::create([
            'solicitud_id'          => $this->solicitudId,
            'equipo'                => $this->equipo, 
            'marca'                 => $this->marca,
            'modelo'                => $this->modelo,
            'serial'                => $this->serial,
            'codigo_interno'        => $this->codigo_interno,
            'ubicacion'             => $this->ubicacion,
            'mantenimiento'         => $this->mantenimiento,
            'mantenimiento_tipo'    => $this->mantenimiento_tipo,
            'descripcion'           => $this->descripcion,
            'metrologia'            => $this->metrologia,
            'metrologia_tipo'       => $this->metrologia_tipo,
            'conformidad'           => $this->conformidad,
            'rango'                 => $this->rango,
            'magnitud'              => $this->magnitud,
            ]);
        $this->emit('closeModal');
        $this->emit('refreshDetalles');
    }
}
