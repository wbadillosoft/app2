<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Orden;
use App\Models\Detalles_orden;
use App\Models\Producto;
use App\Models\Proveedor;
use App\Models\User;
use App\Models\Contactos_proveedor;
use Livewire\WithPagination;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Ordenes extends Component
{
    use WithPagination;

    public $search;
    public $searchProveedor;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $orden;
    public $detalles;
    public $proveedores;
    public $proveedor;
    public $proveedor_data;
    public $queryProveedor;
    public $proveedor_sel;
    public $visible;
    public $hselect;

    protected $rules = [
        'proveedor_id' => 'required',
    ];

    protected $validationAttributes = [
        'proveedor_id' => 'proveedor',
    ];

    protected $messages = [
        'proveedor_id.required' => 'Por favor seleccione un proveedor',
    ];
    protected $queryString = ['search'=>['except'=>''], 'searchProveedor'=>['except'=>'']];

    public $proveedor_id, $email, $usuario, $fecha, $subtotal, $total, $iva, $observaciones, $estado, $codigo, $cotizacion;
    public $aprobo, $forma_pago;
    
    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->proveedor_id     = null;
        $this->proveedor_sel    = '';
        $this->visible          = false;
        $this->hselect          = 0;
    }
 
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {      
        if(!empty($this->proveedor_id)){
            $this->proveedor_data     = Proveedor::find($this->proveedor_id); 
        }
       return view('livewire.ordenes', [
            'usuarios' => User::all(),
            'ordenes'  => Orden::where('codigo', 'like', '%'.$this->search.'%')
            ->orWhere('fecha', 'like', '%'. $this->search.'%')
            ->orderBy('id', 'desc')
            ->paginate(30),
        ]);
    }

    public function updatedQueryProveedor()
    {
        $this->proveedores = Proveedor::where('nombre', 'like', '%'.$this->queryProveedor.'%')->get();
        if( $this->proveedores->count() > 5 )
            $this->hselect = 'h-32';
        else
            $this->hselect = 'h-'.(8 * $this->proveedores->count());
    }

    public function seleccionarProveedor($proveedor_id)
    {
        $this->proveedor        =  Proveedor::find($proveedor_id);
        $this->queryProveedor   = '';
        $this->proveedor_id     = $proveedor_id;
        $this->visible          = false;  
        $this->email            = $this->proveedor->email; 
    }

    public function editarProveedor()
    {
        $this->visible = true;     
    }

    public function formEditar($orden_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $this->proveedores      = Proveedor::all();
        $orden                  = Orden::find($orden_id);
        
        $this->orden            = $orden;
        $this->cotizacion       = $orden->cotizacion;
        $this->aprobo           = $orden->aprobo;
        $this->proveedor_id     = $orden->proveedor_id; 
        $this->forma_pago       = $orden->forma_pago; 
        $this->observaciones    = $orden->observaciones; 
    }

    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
        $this->proveedor_sel    = null;
        $this->proveedor_data   = null;
        $this->proveedores      = Proveedor::all();
    }

    public function store()
    {
        $this->validate();

        $orden = Orden::create([
            'proveedor_id'  => $this->proveedor_id,
            'usuario_id'    => Auth::user()->id,
            'fecha'         => date('Y-m-d h:m:i'),
            'email'         => $this->email,
            'forma_pago'    => $this->forma_pago,
            'cotizacion'    => is_null($this->cotizacion) ? 'N/A':$this->cotizacion,
            'estado'        => 0,
            'aprobo'        => is_null($this->aprobo)? 1: $this->aprobo,
        ]);
        $orden->update(['codigo' => 'OC'.date('y').str_pad($orden->id, 3, "000", STR_PAD_LEFT)]);
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = false;
        $this->orden            = $orden;
    }
    
    public function updateRecibir()
    {
        $orden = $this->orden;
        $orden->update([
            'estado'   => !$this->orden->estado,
        ]); 
    }

    public function updateEncabezado()
    {
        $orden = $this->orden;
        $orden->update([
            'proveedor_id'  => $this->proveedor_id,
            'forma_pago'    => $this->forma_pago,
            'cotizacion'    => is_null($this->cotizacion)?'N/A':$this->cotizacion,
            'aprobo'        => is_null($this->aprobo)? 1: $this->aprobo,
        ]);
        session()->flash('mensaje', 'Datos actualizados');
    }

    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;    
        $this->proveedor        =  null;
        $this->queryProveedor   = '';
        $this->proveedor_id     = null;
        $this->email            = null; 
        $this->render();
    }

}

