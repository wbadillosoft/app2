<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Cotizacion;/* 
use App\Models\Detalles_cotizacion;
use App\Models\Producto; */
use App\Models\Cliente;
use App\Models\Contactos_cliente;
use Livewire\WithPagination;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;

class Cotizaciones extends Component
{
    use WithPagination;

    public $search;
    public $searchCliente;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $cotizacion;
    public $detalles;
    public $clientes;
    public $cliente;
    public $cliente_data;
    public $contactos;
    public $queryCliente;
    public $visible;
    public $hselect;

    protected $queryString = ['search'=>['except'=>''], 'searchCliente'=>['except'=>'']];

    public $cliente_id, $contacto_id, $usuario, $fecha, $subtotal, $total, $iva, $observaciones, $estado, $codigo, $validez;

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->cliente_id       = null;
        $this->contacto_id      = null;
        $this->queryCliente     = null;
        $this->visible          = false;
        $this->hselect          = 0;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        if(!empty($this->cliente_id)){
            $this->cliente_data = Cliente::find($this->cliente_id);    
        }
        $this->contactos    = Contactos_cliente::where('cliente_id', $this->cliente_id)->get();
        /*return view('livewire.cotizaciones', [
            'cotizaciones'  => Cotizacion::where('codigo', 'like', '%'.$this->search.'%')
            ->orWhere('fecha', 'like', '%'. $this->search.'%')
            ->orderBy('id', 'desc')
            ->paginate(30),
        ]);*/
        return view('livewire.cotizaciones', [
            'cotizaciones'  => Cotizacion::where('estado','0')
            ->orderBy('id', 'desc')
            ->get(),
        ]);
    }

/*     public function updatedQueryCliente()
    {
        $this->clientes    = Cliente::where('nombre', 'like', '%'.$this->queryCliente.'%')->get();
        if( $this->clientes->count() > 5 )
            $this->hselect = 'h-32';
        else
            $this->hselect = 'h-'.(8 * $this->clientes->count());
    }

    public function seleccionarCliente($cliente_id)
    {
        $this->cliente          =  Cliente::find($cliente_id);
        $this->queryCliente     = '';
        $this->cliente_id       = $cliente_id;
        $this->visible          = false;  
        $this->email_factura    = $this->cliente->email_factura; 
    }

    public function editarCliente()
    {
        $this->visible = true;     
    } */

    public function formEditar($cotizacion_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $this->clientes         = Cliente::all();
        $cotizacion             = Cotizacion::find($cotizacion_id);
        
        $this->cotizacion       = $cotizacion;
        $this->cliente_id       = $cotizacion->cliente_id; 
        $this->validez          = $cotizacion->validez; 
        $this->contacto_id      = $cotizacion->contacto_id; 
        $this->condiciones      = $cotizacion->condiciones; 
    }

    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
        $this->clientes         = Cliente::all();
        $this->cliente_id       = null;
        $this->validez          = null; 
    }

    public function store()
    {  
        Schema::disableForeignKeyConstraints();
        $cotizacion = Cotizacion::create([
            'cliente_id'    => $this->cliente_id,
            'contacto_id'   => $this->contacto_id,
            'usuario_id'    => Auth::user()->id,
            'fecha'         => date('Y-m-d'),
            'validez'       => $this->validez,
            'estado'        => 0
        ]);
        $cotizacion->update(['codigo' => 'C'.date('y').str_pad($cotizacion->id, 3, "000", STR_PAD_LEFT)]);
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = false;
        $this->cotizacion       = $cotizacion;
        Schema::enableForeignKeyConstraints();
    }
    
    public function updateEncabezado()
    {
        $cotizacion = $this->cotizacion;
        Schema::disableForeignKeyConstraints();
        $cotizacion->update([
            'cliente_id'    => $this->cliente_id,
            'contacto_id'   => $this->contacto_id,
            'validez'       => $this->validez,
        ]);
        Schema::enableForeignKeyConstraints();
        $this->emit('saved', $this->cotizacion->id);
    }

    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;    
        $this->render();
    }
}
