<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Producto;
use App\Models\Prefactura;
use App\Models\Detalles_prefactura as Detalles;


class DetallesPrefactura extends Component
{
    public $modalProducto;
    public $productos;
    public $producto_sel;
    public $producto_id;
    public $producto;
    public $queryProducto;
    public $size;
    public $prefactura;
    public $detalles;
    public $item_id;
    public $grantotal;
    public $subtotal;
    public $ivatotal;
    public $observaciones;
    public $editarProducto;
    public $item;
    public $producto_eliminar;

    public $confirmar;

    public $pdf1;

    public function mount(Prefactura $prefactura)
    {
        $this->modalProducto    = false;
        $this->productos        = null;
        $this->producto         = null;
        $this->producto_sel     = null;
        $this->producto_id      = null;
        $this->queryProducto    = '';
        $this->size             = 0;
        $this->prefactura       = $prefactura;
        $this->observaciones    = $this->prefactura->observaciones;
        $this->item_id          = null;
        $this->grantotal        = 0;
        $this->ivatotal         = 0;
        $this->subtotal         = 0;
        $this->editarProducto   = false;
        $this->item             = null;
        $this->producto_eliminar= null;

        $this->confirmar        = false;
    }

    public function render()
    {
        $this->prefactura = Prefactura::find($this->prefactura->id);
        $this->granTotal();
        return view('livewire.detalles-prefactura',
            ['prefactura'=>$this->prefactura]
        );        
    }

    public function updatedQueryProducto()
    {
        $this->productos    = Producto::where('nombre', 'like', '%'.$this->queryProducto.'%')->get();
    }

    public function buscarProductos()
    {
        $this->queryProducto    = '';
        $this->size             = 0;
        $this->producto_sel     = null;
        $this->editarProducto   = false;
        $this->modalProducto    = true;
    }

    public function editarItem($item, $producto_id)
    {
        $this->item          = $item;
        $this->producto_sel  = Producto::find($producto_id);
        $this->editarProducto= true;
        $this->modalProducto = true;
    }

    public function modificarProducto()
    {
        $this->queryProducto    = '';
        $this->size             = 0;
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::whereId($this->item)->update([
            'prefactura_id' => $this->prefactura->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'porc_descuento'=> 0,
            'descuento'     => 0,
            'iva'           => $this->producto->iva,
            'total_iva'     => $this->producto->precio * ($this->producto->iva) / 100,
            'total'         => $this->producto->precio,
        ]);
        $this->granTotal();
        $this->prefactura = Prefactura::find($this->prefactura->id);
    }

    public function seleccionarProducto($producto_id)
    {
        $this->producto_sel     = Producto::find($producto_id);
        $this->queryProducto    = '';
        $this->size             = 0;
        $this->producto_id      = $producto_id;
    }
    
    public function agregarProducto()
    {
        $this->queryProducto    = '';
        $this->size             = 0;
        $this->modalProducto    = false;
        $this->editarProducto   = false;
        $this->producto_sel     = null;
        $this->producto         = Producto::find($this->producto_id);

        Detalles::create([
            'prefactura_id' => $this->prefactura->id,
            'producto_id'   => $this->producto->id,
            'cantidad'      => 1,
            'porc_descuento'=> 0,
            'descuento'     => 0,
            'iva'           => $this->producto->iva,
            'total_iva'     => $this->producto->precio * ($this->producto->iva) / 100,
            'total'         => $this->producto->precio,
        ]);
        $this->granTotal();
        $this->prefactura = Prefactura::find($this->prefactura->id);
    }

    public function updateDetalles()
    {
        $this->prefactura->update([
            'observaciones' => $this->observaciones,
            'subtotal'      => $this->subtotal,
            'iva'           => $this->ivatotal,
            'total'         => $this->grantotal,
        ]);   
        $this->prefactura = Prefactura::find($this->prefactura->id);
        session()->flash('mensaje', 'Datos actualizados');
    }

    public function closeConfirmar()
    {
        $this->confirmar = false;   
    }
    public function confirmarEliminarItem($item)
    {
        $this->item_id = $item;
        $item = Detalles::find($item);
        $this->producto_eliminar=  $item->producto->nombre;
        $this->confirmar        = true;   
    }

    public function eliminarItem($item)
    {
        $this->confirmar = false; 
        Detalles::destroy($item);
        $this->granTotal();
        $this->prefactura = Prefactura::find($this->prefactura->id);
    }
    
    public function cerrarModal()
    {
        $this->modalProducto    = false;
        $this->editarProducto   = true;
        $this->producto_sel     = null;
        $this->queryProducto    = '';
        $this->size             = 0;
    }

    public function setDescuento($descuento)
    {
        $item = Detalles::find($this->item_id); 
        $item->update([
            'porc_descuento'=>$descuento,
            'descuento'=>$descuento,
        ]);
        $this->producto = Producto::find($item->producto_id);
        $this->totalItem($item->cantidad, $descuento);
        $this->granTotal();
    }

    public function setCantidad($cantidad)
    {
        $cantidad       = $cantidad;
        $cantidad       = empty($cantidad) ? 0: $cantidad;
        $item           = Detalles::find($this->item_id);
        $item->update([
            'cantidad'=>$cantidad,
        ]);
        $this->producto = Producto::find($item->producto_id);
        $this->totalItem($cantidad, $item->descuento);
        $this->granTotal();
    }

    public function totalItem($cantidad, $desc)
    {
        $descuento  = $desc/100;
        $precio     = $this->producto->precio;
        $totalItem  = $cantidad * ($precio*(1-$descuento));
        $iva        = $totalItem * ($this->producto->iva) / 100;
        $item       = Detalles::find($this->item_id); 
        $item->update([
            'total'=>$totalItem,
            'total_iva'=>$iva,
        ]);
    }

    public function granTotal()
    {
        $detalles       = Detalles::where('prefactura_id', $this->prefactura->id)->get();
        $prefactura     = Prefactura::find($this->prefactura->id);
        $this->subtotal = $detalles->sum('total'); 
        $this->ivatotal = $detalles->sum('total_iva'); 
        $this->grantotal=$this->subtotal + $this->ivatotal; 
        
        $this->prefactura->update([
            'observaciones' => $this->observaciones,
            'subtotal'      => $this->subtotal,
            'iva'           => $this->ivatotal,
            'total'         => $this->grantotal,
        ]);   
    }    

}