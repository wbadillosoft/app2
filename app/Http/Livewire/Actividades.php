<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Proceso;
use App\Models\Actividad;

use Illuminate\Support\Facades\Auth;

class Actividades extends Component
{
    public $proceso;
    public $actividad;
   
    public function mount(Proceso $proceso)
    {
        $this->actividad   = null;
        $this->proceso     = $proceso;
    }

    public function render()
    {
        //$this->proceso = find( $this->proceso->id );
        return view('livewire.actividades',[
            'proceso' => $this->proceso,
        ]);
    }

    public function agregarActividad()
    {
        $id=$this->proceso->id;
        Actividad::create([
            'actividad' => $this->actividad,
            'proceso_id'=> $this->proceso->id, 
            'usuario'   => Auth::user()->name,
            'fecha'     => date('Y-m-d h:m:i'),
        ]);
        $this->actividad  = null;
        $this->cliente_id = null;
        $this->proceso = Proceso::find($id);
        session()->flash('mensaje', 'Actividad agregada');
       // return redirect()->to('/procesos');

    }
}
