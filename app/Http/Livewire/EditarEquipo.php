<?php

namespace App\Http\Livewire;

use App\Models\Detalles_solicitud as Detalles;
use LivewireUI\Modal\ModalComponent;

class EditarEquipo extends ModalComponent
{    
    public $mantenimiento;
    public $mantenimiento_tipo;
    public $metrologia;
    public $metrologia_tipo;
    public $magnitud;
    public $conformidad;
    public $regla;
    
    public $equipo;
    public $marca;
    public $modelo;
    public $serial;
    public $codigo_interno;
    public $ubicacion;
    public $rango;

    public $descripcion;
    public $solicitudId;
    public $solicitud;
    public $detalles;
    public $equipoId;
    public $equiposId;

    public function mount()
    {
        $this->detalles = Detalles::find($this->equipoId)->toArray();         
        $this->mantenimiento = $this->detalles['mantenimiento'] === '0' ? false:true ;
        $this->metrologia = $this->detalles['metrologia'] === '0' ? false:true ;
        $this->magnitud = $this->detalles['magnitud'] === null ? 'masa':$this->detalles['magnitud'] ;
        $this->conformidad = $this->detalles['conformidad'] === null ? 'no':$this->detalles['conformidad'] ;
        $this->metrologia_tipo = $this->detalles['metrologia_tipo'] === null ? 'calibración':$this->detalles['metrologia_tipo'] ;
        $this->mantenimiento_tipo = $this->detalles['mantenimiento_tipo'] === null ? 'mantenimiento preventivo':$this->detalles['mantenimiento_tipo'] ;
    }
    
    public function render()
    {
        return view('livewire.editar-equipo');
    }

    public function guardarEquipo()
    {
        $detalles = Detalles::find($this->equipoId); 
        $detalles->update([
            'equipo'                => $this->detalles['equipo'], 
            'marca'                 => $this->detalles['marca'],
            'modelo'                => $this->detalles['modelo'],
            'serial'                => $this->detalles['serial'],
            'codigo_interno'        => $this->detalles['codigo_interno'],
            'ubicacion'             => $this->detalles['ubicacion'],
            'mantenimiento'         => $this->mantenimiento,
            'mantenimiento_tipo'    => $this->mantenimiento_tipo,
            'descripcion'           => $this->detalles['descripcion'],
            'metrologia'            => $this->metrologia,
            'metrologia_tipo'       => $this->metrologia_tipo,
            'conformidad'           => $this->conformidad,
            'rango'                 => $this->detalles['rango'],  
            'magnitud'              => $this->magnitud,  
        ]);
        
        $this->emit('closeModal');
        $this->emit('refreshDetalles');
    }

}
