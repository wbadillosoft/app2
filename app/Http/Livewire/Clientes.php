<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Cliente;
use App\Models\Contactos_cliente;
use App\Models\Municipio;
use App\Models\ClienteFacturacion;
use Illuminate\Support\Facades\Auth;

class Clientes extends Component
{
    public $search;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $nombre, $nit, $telefono, $direccion, $ciudad, $notas, $email_factura;
    public $cliente;
    public $contactos;

    protected $queryString = ['search' => ['except' => '']];

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;
    }

    public function render()
    {
        $municipios = Municipio::all();
        return view('livewire.clientes', [
            'clientes'  => Cliente::orderBy('nombre', 'asc')->get(),
            'municipios' => $municipios,
        ]);
    }

    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;
        // $this->render();
    }

    public function actualizarClientes()
    {
        $simbolos = [',', '.', ' '];
        $reemplazos = ['', '', ''];
        $clientes = Cliente::all();
        foreach ($clientes as $key => $cliente) {
            $nit = $cliente->nit;
            $cliente->nit = str_replace($simbolos, $reemplazos, $nit);
            $cliente->nombre = strtolower($cliente->nombre);
            $sa = explode(' ', $cliente->nombre);
            $ca = [];
            foreach ($sa as $s) {
                $ca[] = ucfirst($s);
            }
            $cliente->nombre = implode(' ', $ca);
            $cliente->nombre = str_replace('S.a.s', 'S.A.S', $cliente->nombre);
            $cliente->save();
        }
        $this->render();
    }

    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $this->nombre       = null;
        $this->nit          = null;
        $this->telefono     = null;
        $this->email_factura = null;
        $this->direccion    = null;
        $this->ciudad       = null;
        $this->notas        = null;
    }
    public function formEditar($cliente_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $cliente            = Cliente::find($cliente_id);

        $this->cliente      = $cliente;

        $this->nombre       = $cliente->nombre;
        $this->nit          = $cliente->nit;
        $this->telefono     = $cliente->telefono;
        $this->email_factura = $cliente->email_factura;
        $this->direccion    = $cliente->direccion;
        $this->ciudad       = $cliente->ciudad;
        $this->notas        = $cliente->notas;
    }

    public function store()
    {
        Cliente::create([
            'nombre'    => $this->nombre,
            'nit'       => $this->nit,
            'telefono'  => $this->telefono,
            'direccion' => $this->direccion,
            'ciudad'    => $this->ciudad,
            'notas'     => $this->notas,
            'email_factura'     => $this->email_factura,
        ]);

        ClienteFacturacion::create([
            'name'    => $this->nombre,
            'rut'       => $this->nit,
            'web'  => $this->telefono,
            'address' => $this->direccion,
            'city'    => $this->ciudad,
            'notes'     => $this->notas,
            'email_factura'     => $this->email_factura,
            'user'     => Auth::user()->name,
        ]);

        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = true;
        $this->emit('saved');
        //return redirect()->to('/clientes');
    }

    public function update()
    {
        $cliente = $this->cliente;
        $clienteFacturacion = ClienteFacturacion::where('rut', $this->cliente->nit)->where('name', $this->cliente->nombre)->first();

        $cliente->update([
            'nombre'        => $this->nombre,
            'nit'           => $this->nit,
            'telefono'      => $this->telefono,
            'email_factura' => $this->email_factura,
            'direccion'     => $this->direccion,
            'ciudad'        => $this->ciudad,
            'notas'         => $this->notas,
        ]);
        if($clienteFacturacion!==null)
        {
            $clienteFacturacion->update([
                'name'          => $this->nombre,
                'rut'           => $this->nit,
                'web'           => $this->telefono,
                'email_factura' => $this->email_factura,
                'address'       => $this->direccion,
                'city'          => $this->ciudad,
                'notes'         => $this->notas,
            ]);
        }
        else{
            ClienteFacturacion::create([
                'name'          => $this->nombre,
                'rut'           => $this->nit,
                'web'           => $this->telefono,
                'address'       => $this->direccion,
                'city'          => $this->ciudad,
                'notes'         => $this->notas,
                'email_factura' => $this->email_factura,
                'user'          => Auth::user()->name,
            ]);
        }

        $this->emit('saved');
    }

    public function show($cliente_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = true;

        $cliente         = Cliente::find($cliente_id);
        $this->contactos = Contactos_cliente::where('cliente_id', $cliente_id)->get();

        $this->cliente   = $cliente;
        $this->nombre    = $cliente->nombre;
        $this->nit       = $cliente->nit;
        $this->telefono  = $cliente->telefono;
        $this->direccion = $cliente->direccion;
        $this->ciudad    = $cliente->ciudad;
        $this->notas     = $cliente->notas;
    }
}
