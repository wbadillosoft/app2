<?php

namespace App\Http\Livewire;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Solicitud;
use App\Models\Detalles_solicitud as Detalles;

class Solicitudes extends Component
{
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $solicitudes;
    public $detalles;

    public $cliente;
    public $verDetalles = true;

    use WithPagination;

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;
    }
    
    protected $listeners = ['nuevaSolicitud'=>'$refresh'];

    public function render()
    {
        $this->solicitudes = Solicitud::where('estado', 1)->orWhere('estado', 2)->orderBy('id', 'desc')->get();
        return view('livewire.solicitudes');
    }

    public function formEditar($solicitud_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $solicitud              = Solicitud::find($solicitud_id);
        $this->detalles         = Detalles::where('solicitud_id', $solicitud_id)->get();
        
        $this->solicitud        = $solicitud;
        $this->nombre           = $solicitud->nombre; 
        $this->nit              = $solicitud->nit; 
        $this->ciudad           = $solicitud->ciudad; 
        $this->contacto         = $solicitud->contacto; 
        $this->telefono         = $solicitud->telefono; 
        $this->email            = $solicitud->email; 
    }
    
}
