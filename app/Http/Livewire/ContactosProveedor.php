<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Contactos_proveedor;

class ContactosProveedor extends Component
{

    public $proveedor_id;
    public $proveedor;
    public $contacto;
    public $contacto_id;
    public $nombre, $cargo, $telefono, $email;
    public $agregar_enable;
    public $agregar;

    public function mount()
    {
        $this->proveedor_id       = $this->proveedor->id;
        $this->agregar_enable   = true;
        $this->agregar          = false;
    }

    public function render()
    {
        $contactos = Contactos_proveedor::where('proveedor_id', $this->proveedor_id)->get();
        return view('livewire.contactos-proveedor', [
            'contactos'=>$contactos,
        ]);
    }
  
    public function listar($proveedor_id)
    {
        $this->proveedor_id = $proveedor_id;
    }
    
    public function editarContacto($contacto_id)
    {
        $contacto = Contactos_proveedor::find($contacto_id);
        $this->contacto         = $contacto;
        $this->nombre           = $contacto->nombre;
        $this->cargo            = $contacto->cargo;
        $this->telefono         = $contacto->telefono;
        $this->email            = $contacto->email;
        $this->agregar_enable   = false;
    }
    
    public function updateContacto()
    {
        if($this->agregar)
        {
            Contactos_proveedor::create([
                'proveedor_id'=> $this->proveedor_id,
                'nombre'    => $this->nombre,
                'telefono'  => $this->telefono,
                'cargo'     => $this->cargo,
                'email'     => $this->email,
            ]);  
        }
        else
        {
            $contacto               = $this->contacto;
            $contacto->update([
                'nombre'    => $this->nombre,
                'telefono'  => $this->telefono,
                'cargo'     => $this->cargo,
                'email'     => $this->email,
            ]);
            }
        $this->agregar_enable   = true;
        $this->nombre   = '';
        $this->telefono = '';
        $this->cargo    = '';
        $this->email    = '';
        $this->contacto = '';
        $this->agregar  = false;

    }

    public function agregarContacto()
    {
        $this->agregar=true;
    }
    public function cancelar()
    {
        $this->agregar_enable   = true;
        $this->nombre   = '';
        $this->telefono = '';
        $this->cargo    = '';
        $this->email    = '';
        $this->contacto = '';

    }    
}
