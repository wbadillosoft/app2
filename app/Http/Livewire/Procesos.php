<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Proceso;
use App\Models\Cliente;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Procesos extends Component
{
    use WithPagination;

    public $search;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;
    public $filtro;
    public $users;

    public $proceso;
    public $activiades;

    public $abierto_por;
    public $responsable;
    public $asignado;

    public $cliente;
    public $clientes;
    public $searchCliente = null;
    public $searchFecha = null;
    public $searchResponsable = null;
    public $searchAsignadoA = null;
    public $searchAbiertoPor = null;

    public $tipo = 0;

    public $edit=false;

    public $titulo, $cliente_id, $usuario, $fecha_apertura, $fecha_cierre, $estado, $dias;

    protected $queryString = [
        'search'=>['except'=>''],
        'searchCliente'=>['except'=>''],
        'searchFecha'=>['except'=>''],
        'searchResponsable'=>['except'=>''],
        'searchAsignadoA'=>['except'=>''],
        'searchAbiertoPor'=>['except'=>''],
    ];

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;
        $this->titulo           = null;
        $this->cliente_id       = null;
        $this->cliente_sel      = null;
        $this->abierto_por      = null;

        $this->searchCliente    = null;
        $this->searchFecha      = null;
        $this->searchResponsable= null;
        $this->searchAsignadoA  = null;
        $this->searchAbiertoPor = null;

        $this->clientes  = Cliente::all();
 
        $usuarios = User::where('status', 1)->get();
        $this->users =  $usuarios->map(function ($item, $key) {
            $usuario = [
                'id'        => $item->name,
                'title'     => $item->name,
                'subtitle'  => $item->cargo
            ];
            return $usuario;
        });
    }

    public function render()
    { 
        $query  = DB::table('procesos');  
        $pag    = 30;
        if($this->tipo === 0)
        {
            $pag = 100;
            $query =  $query->where('estado', 0);
        }           
        elseif($this->tipo === 1)
        {
            $query =  $query->where('estado', 1);
        }            

        if($this->searchCliente || $this->searchFecha || $this->searchResponsable || $this->searchAbiertoPor || $this->searchAsignadoA)
        {
            $pag = 50;
            if ($this->searchCliente) {
                $query = $query->where('cliente_id', $this->searchCliente);
            }
            if ($this->searchFecha) {
                $query = $query->where('fecha_apertura', 'like', '%' . $this->searchFecha . '%');
            }
            if ($this->searchResponsable) {
                $query = $query->where('responsable',$this->searchResponsable );
            }
            if ($this->searchAbiertoPor) {
                $query = $query->where('abierto_por', $this->searchAbiertoPor);
            }
            if ($this->searchAsignadoA) {
                $query =  $query->where('asignado', 'like', '%' . $this->searchAsignadoA . '%');
            }
        }
        $procesos = $query->select('procesos.*', 'clientes.nombre')
                            ->join('clientes', 'clientes.id', '=', 'procesos.cliente_id')
                            ->orderBy('procesos.id', 'desc')->paginate($pag);

        return view('livewire.procesos', [
            'procesos'      => $procesos,
            'usuarios'      => User::where('status', 1)->get(),
        ]);
    }

    public function listado($tipo)
    {
        $this->tipo = $tipo;
    }
    public function openFiltro()
    {
        $this->filtro = true;
        $this->clear();
    }
    public function filtrar()
    {
        $this->filtro = false;
    }
    public function clear()
    {
        $this->searchAbiertoPor     = '';
        $this->searchCliente        = '';
        $this->searchAsignadoA      = '';
        $this->searchFecha          = '';
        $this->searchResponsable    = '';
        $this->abierto_por          = null; 
        $this->titulo               = null; 
        $this->dias                 = null; 
        $this->cliente_id           = null; 
    }

    public function volver()
    {
        if($this->mostrar_editar){
          $this->mostrar_editar   = false;  
        }
        if($this->mostrar_crear){
            $this->mostrar_crear= false;
            $this->tipo         = 0;
        }
        $this->mostrar_lista    = true;
        $this->mostrar_show     = false;    
    }

    public function formCrear()
    {
        $this->clear();
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
        $this->titulo           = null;
        $this->cliente_id       = null;
    }

    public function formEditar($proceso_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $this->proceso          = Proceso::find($proceso_id);
        
        $this->abierto_por      = $this->proceso->abierto_por; 
        $this->responsable      = $this->proceso->responsable; 
        $this->asignado         = $this->proceso->asignado; 
        $this->titulo           = $this->proceso->titulo; 
        $this->dias             = $this->proceso->dias; 
        $this->cliente_id       = $this->proceso->cliente_id; 
    }

    public function store()
    {
        $error = false;
        if($this->titulo === null)
        {
            session()->flash('errorTitulo', 'Escribir título del proceso');
            $error = true;
        }
        
        if($this->cliente_id === 0)
        {
            $this->emit('errorCliente');
            $error = true;
        }
         
        if(!$error)
        {
            $this->cliente_id = ($this->cliente_id=== null) ? 1:$this->cliente_id; 
            Schema::disableForeignKeyConstraints();
            $proceso = Proceso::create([
                    'titulo'        => $this->titulo,
                    'cliente_id'    => $this->cliente_id,
                    'abierto_por'   => Auth::user()->name,
                    'responsable'   => $this->responsable,
                    'asignado'      => $this->asignado,
                    'usuario'       => Auth::user()->name,
                    'fecha_apertura'=> date('Y-m-d h:m:i'),
                    'dias'          => $this->dias,
                    'estado'        => 0,
            ]);
            Schema::enableForeignKeyConstraints();
            $this->mostrar_crear    = false;
            $this->mostrar_editar   = false;
            $this->mostrar_show     = false;
            $this->mostrar_lista    = false;
            $this->formEditar($proceso->id);
            $this->emit('saved');
        }
    }

    public function update()
    {
        $proceso = $this->proceso;
        Schema::disableForeignKeyConstraints();
        $proceso->update([
            'titulo'        => $this->titulo,
            'dias'          => $this->dias,
            'cliente_id'    => $this->cliente_id,    
            'responsable'   => $this->responsable,       
            'asignado'      => $this->asignado,      
        ]);
        Schema::enableForeignKeyConstraints();
        $this->emit('updated');
    }

    public function show($proceso_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = true;

        $proceso                = Proceso::find($proceso_id);
        
        $this->proceso          = $proceso;
        $this->titulo           = $proceso->titulo; 
        $this->usuario          = $proceso->usuario; 
        $this->fecha_apertura   = $proceso->fecha_apertura; 
        $this->estado           = $proceso->estado; 
    } 

    public function cerrar()
    {
        $proceso = $this->proceso;
        $proceso->update([
            'estado'        => 1,
            'fecha_cierre'  => date('Y-m-d h:m:i'),
        ]);
        return redirect()->to('/procesos');
    }
 
    public function edit()
    {
        $this->edit = true;     
    }
    
}
