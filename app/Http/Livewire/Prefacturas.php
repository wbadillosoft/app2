<?php

namespace App\Http\Livewire;

use App\Models\Prefactura;
use App\Models\Detalles_prefactura;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Contactos_cliente;
use Livewire\WithPagination;

use Livewire\Component;

use Illuminate\Support\Facades\Auth;

class Prefacturas extends Component
{
    use WithPagination;

    public $search;
    public $searchCliente;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $prefactura;
    public $detalles;
    public $clientes;
    public $cliente;
    public $cliente_data;
    public $queryCliente;
    public $cliente_sel;
    public $visible;
    public $hselect;

    protected $queryString = ['search'=>['except'=>''], 'searchCliente'=>['except'=>'']];

    public $cliente_id, $email_factura, $usuario, $fecha, $subtotal, $total, $iva, $observaciones, $estado, $codigo;
    public $vencimiento, $factura;

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->cliente_id       = null;
        $this->cliente_sel      = '';
        $this->visible          = false;
        $this->hselect          = 0;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        if(!empty($this->cliente_id)){
            $this->cliente_data     = Cliente::find($this->cliente_id); 
            if(empty($this->email_factura))   
            {
                $this->email_factura = $this->cliente_data->email_factura;
            }
        }
        
        return view('livewire.prefacturas', [
            'prefacturas'  => Prefactura::where('codigo', 'like', '%'.$this->search.'%')
            ->orWhere('fecha', 'like', '%'. $this->search.'%')
            ->orderBy('id', 'asc')
            ->paginate(30),
        ]);
    }

    public function updatedQueryCliente()
    {
        $this->clientes    = Cliente::where('nombre', 'like', '%'.$this->queryCliente.'%')->get();
        if( $this->clientes->count() > 5 )
            $this->hselect = 'h-32';
        else
            $this->hselect = 'h-'.(8 * $this->clientes->count());
    }

    public function seleccionarCliente($cliente_id)
    {
        $this->cliente          =  Cliente::find($cliente_id);
        $this->queryCliente     = '';
        $this->cliente_id       = $cliente_id;
        $this->visible          = false;  
        $this->email_factura    = $this->cliente->email_factura; 
    }

    public function editarCliente()
    {
        $this->visible = true;     
    }

    public function formEditar($prefactura_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $this->clientes         = Cliente::all();
        $prefactura             = Prefactura::find($prefactura_id);
        
        $this->prefactura       = $prefactura;
        $this->factura          = $prefactura->factura;
        $this->vencimiento      = $prefactura->vencimiento;
        $this->cliente_id       = $prefactura->cliente_id; 
        $this->email_factura    = $prefactura->email_factura; 
    }

    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
        $this->clientes         = Cliente::all();

    }

    public function store()
    {
        $prefactura = Prefactura::create([
            'cliente_id'    => $this->cliente_id,
            'usuario_id'    => Auth::user()->id,
            'fecha'         => date('Y-m-d h:m:i'),
            'email_factura' => $this->email_factura,
            'vencimiento'   => $this->vencimiento,
            'estado'        => 0,
            'factura'       => $this->factura,
            'observaciones' => $this->observaciones,
        ]);
        $prefactura->update(['codigo' => 'E-'.date('y').'-'.str_pad($prefactura->id, 4, "000", STR_PAD_LEFT)]);
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = false;
        $this->prefactura       = $prefactura;
    }
    
    public function updateEnviar()
    {
        $prefactura = $this->prefactura;
        $prefactura->update([
            'estado'   => !$this->prefactura->estado,
        ]); 
    }

    public function updateEncabezado()
    {
        $prefactura = $this->prefactura;
        $prefactura->update([
            'cliente_id'    => $this->cliente_id,
            'email_factura' => $this->email_factura,
            'factura'       => $this->factura,
            'vencimiento'   => $this->vencimiento,
        ]);
        session()->flash('mensaje', 'Datos actualizados');
    }

    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;    
        $this->render();
    }

}
