<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Proveedor;
use App\Models\Contactos_proveedor;
use App\Models\Municipio;

class Proveedores extends Component
{
    public $search;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $nombre, $nit, $telefono, $direccion, $ciudad, $notas, $email;
    public $proveedor;
    public $contactos;
   
    protected $queryString = ['search'=>['except'=>'']];

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;
    }

    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;    
    }
    
    public function render()
    {
        $municipios = Municipio::all();

        return view('livewire.proveedores', [
            'proveedores'   => Proveedor::all(),
            'municipios'    => $municipios,
        ]);
    }
    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
    }
    public function formEditar($proveedor_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $proveedor        = Proveedor::find($proveedor_id);
        
        $this->proveedor  = $proveedor;

        $this->nombre   = $proveedor->nombre; 
        $this->nit      = $proveedor->nit; 
        $this->telefono = $proveedor->telefono; 
        $this->email    = $proveedor->email; 
        $this->direccion= $proveedor->direccion; 
        $this->ciudad   = $proveedor->ciudad; 
        $this->notas    = $proveedor->notas; 
    }
    
    public function store()
    {
        Proveedor::create([
            'nombre'    => $this->nombre,
            'nit'       => $this->nit,
            'telefono'  => $this->telefono,
            'email'     => $this->email,
            'direccion' => $this->direccion,
            'ciudad'    => $this->ciudad,
            'notas'     => $this->notas,
        ]);
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = true;
        $this->emit('saved');
    }

    public function update()
    {
        $proveedor = $this->proveedor;
        $proveedor->update([
            'nombre'    => $this->nombre,
            'nit'       => $this->nit,
            'telefono'  => $this->telefono,
            'email'     => $this->email,
            'direccion' => $this->direccion,
            'ciudad'    => $this->ciudad,
            'notas'     => $this->notas,
        ]);
        $this->emit('saved');
    }

    public function show($proveedor_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = true;

        $proveedor          = Proveedor::find($proveedor_id);
        $this->contactos    = Contactos_proveedor::where('proveedor_id', $proveedor_id)->get();
        
        $this->proveedor    = $proveedor;
        $this->nombre       = $proveedor->nombre; 
        $this->nit          = $proveedor->nit; 
        $this->telefono     = $proveedor->telefono; 
        $this->email        = $proveedor->email; 
        $this->direccion    = $proveedor->direccion; 
        $this->ciudad       = $proveedor->ciudad; 
        $this->notas        = $proveedor->notas; 
    }    
}
