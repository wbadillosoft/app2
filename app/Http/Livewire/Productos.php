<?php

namespace App\Http\Livewire;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Producto;
use App\Models\Proveedores_por_producto;
use App\Models\Proveedor;
use App\Models\Costo;

class Productos extends Component
{
    public $search;
    public $mostrar_lista;
    public $mostrar_editar;
    public $mostrar_crear;
    public $mostrar_show;

    public $proveedores;
    public $producto;
    public $costos;
    public $verProveedor;
    public $proveedor;
    public $proveedor_sel;

    public $nombre, $codigo, $descripcion, $marca, $referencia, $categoria, $presentacion, $observaciones, $precio, $iva, $tiempo;
   
    protected $queryString = ['search'=>['except'=>'']];

    use WithPagination;

    public function mount()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;
        $this->verProveedor     = false;
        $this->proveedor        = null;
        $this->proveedor_sel    = [
            'nombre'=>'',
            'nit'=>'',
            'telefono'=>'',
            'direccion'=>'',
            'email'=>'',
        ];
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        /*
     return view('livewire.productos', [
            'productos' => Producto::where('nombre', 'like', '%'.$this->search.'%')
                ->orWhere('codigo', 'like', '%'.$this->search.'%')
                ->orderBy('nombre', 'asc')
                ->paginate(30),
        ]);
        */

        return view('livewire.productos', [
            'productos' => Producto::orderBy('nombre', 'asc')->get(),
        ]);
    }

    public function actualizarProducto()
    {
        $simbolos = [',','.',' '];
        $reemplazos = ['','',''];
        $producto = Producto::all();
        foreach ($producto as $key => $producto) {
            $producto->nombre = strtolower($producto->nombre);
            $sa = explode(' ', $producto->nombre);
            $ca = [];
            foreach ($sa as $s) {
                $ca[] = ucfirst($s);
            }
            $producto->nombre = implode(' ', $ca);
            $producto->save();
        }
        $this->render();
    }


    public function verProveedor($id)
    {
        $this->proveedor= Proveedor::find($id);
        $verProveedor   = true;
    }
    public function volver()
    {
        $this->mostrar_lista    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_crear    = false;
        $this->mostrar_show     = false;    
    }

    public function formCrear()
    {
        $this->mostrar_crear    = true;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;
        
        $this->nombre           = null; 
        $this->codigo           = null; 
        $this->descripcion      = null; 
        $this->marca            = null;
        $this->referencia       = null; 
        $this->precio           = 0; 
        $this->iva              = 19 ;       
        $this->tiempo           = 0; 
        $this->observaciones    = null; 
        $this->presentacion     = null; 
        $this->categoria        = '0'; 
    }
    
    public function formEditar($producto_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = true;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = false;

        $producto        = Producto::find($producto_id);
        
        $this->producto         = $producto;

        $this->nombre           = $producto->nombre; 
        $this->codigo           = $producto->codigo; 
        $this->descripcion      = $producto->descripcion; 
        $this->marca            = $producto->marca; 
        $this->referencia       = $producto->referencia; 
        $this->precio           = number_format($producto->precio,0,'','.'); 
        $this->iva              = $producto->iva; 
        $this->tiempo           = $producto->tiempo; 
        $this->observaciones    = $producto->observaciones; 
        $this->presentacion     = $producto->presentacion; 
        $this->categoria        = $producto->categoria; 
    }
    
    public function store()
    {
        $producto = Producto::create([
            'nombre'        => $this->nombre,
            'codigo'        => $this->codigo,
            'marca'         => $this->marca,
            'referencia'    => $this->referencia,
            'precio'        => str_replace('.', '', $this->precio),
            'categoria'     => $this->categoria, 
            'tiempo'        => $this->tiempo, 
            'iva'           => $this->iva, 
            'observaciones' => $this->observaciones,
            'presentacion'  => $this->presentacion,
            'descripcion'   => $this->descripcion,
        ]);
        $codigo = 'M'.str_pad($producto->id, 3, 0, STR_PAD_LEFT);
        $producto->update(['codigo' => $codigo]);

        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_show     = false;
        $this->mostrar_lista    = true;
        $this->formEditar($producto->id);
        $this->emit('saved');
    }

    public function update()
    {
        $producto = $this->producto;
        $producto->update([
            'nombre'        => $this->nombre,
            'marca'         => $this->marca,
            'referencia'    => $this->referencia,
            'precio'        => str_replace('.', '', $this->precio),
            'tiempo'        => $this->tiempo,
            'categoria'     => $this->categoria,
            'iva'           => $this->iva,
            'observaciones' => $this->observaciones,
            'presentacion'  => $this->presentacion,
            'descripcion'   => $this->descripcion,
        ]);
        $this->emit('saved');
    }

    public function show($producto_id)
    {
        $this->mostrar_crear    = false;
        $this->mostrar_editar   = false;
        $this->mostrar_lista    = false;
        $this->mostrar_show     = true;

        $producto           = Producto::find($producto_id);
        $this->proveedores  = Proveedores_por_producto::where('producto_id', $producto_id)->get();
        $this->costos       = Costo::where('producto_id', $producto_id)->get();
        
        $this->producto     = $producto;
        $this->nombre       = $producto->nombre; 
        $this->codigo       = $producto->codigo; 
        $this->descripcion  = $producto->descripcion; 
        $this->marca        = $producto->marca; 
        $this->referencia   = $producto->referencia; 
        $this->categoria    = $producto->categoria; 
        $this->tiempo       = $producto->tiempo; 
        $this->iva          = $producto->iva; 
        $this->presentacion = $producto->presentacion; 
        $this->precio       = number_format($producto->precio,0,'','.'); 
        $this->observaciones= $producto->observaciones; 
    } 
  
}
