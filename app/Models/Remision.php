<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Cliente;
use App\Models\Detalles_remision as Detalles;

class Remision extends Model
{
    use HasFactory;
    protected $table = 'remisiones';

    protected $fillable = [
        'codigo',
        'cliente_id',
        'usuario_id',
        'fecha',
        'lote',
        'aprobo',
        'observaciones',
        'reviso',
        'factura',
        'estado',
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function detalles()
    {
        return $this->hasMany(Detalles::class);
    }    
}
