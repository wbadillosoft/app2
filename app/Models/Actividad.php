<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Proceso;

class Actividad extends Model
{
    use HasFactory;

    protected $table = 'actividades'; 

    protected $fillable = [
        'proceso_id',
        'actividad',
        'usuario',
        'fecha',
    ];

    public function proceso()
    {
        return $this->belongsTo(Proceso::class);
    }

}
