<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Proveedor;
use App\Models\Costo;

class Producto extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
        'codigo',
        'referencia',
        'marca',
        'descripcion',
        'presentacion',
        'categoria',
        'tiempo',
        'precio',
        'iva',
        'observaciones',
    ];

    public function proveedores(){ 
        return $this->belongsTo(Proveedor::class); //Pertenece a un producto.
    }
    public function costos(){ 
        return $this->hasMany(Costo::class); 
    }
}
