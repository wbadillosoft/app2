<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Producto;

class Costo extends Model
{
    use HasFactory;

    protected $fillable = [
        'producto_id',
        'costo',
        'proveedor',
    ];

    public function producto(){ //$costo->producto->nombre
        return $this->belongsTo(Producto::class); //Pertenece a un producto.
    }
}
