<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Proceso;
#use App\Models\Contactos_cliente as Contactos;

class Cliente extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'nombre',
        'nit',
        'telefono',
        'email_factura',
        'direccion',
        'ciudad',
        'notas',
    ];

    public function proceso()
    {
        return $this->hasOne(Proceso::class);
    }
   
}
