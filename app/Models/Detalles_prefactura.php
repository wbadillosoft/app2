<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Producto;

class Detalles_prefactura extends Model
{
    use HasFactory;

    protected $table = 'detalles_prefactura';

    protected $fillable = [
        'prefactura_id',
        'producto_id',
        'cantidad',
        'porc_descuento',
        'descuento',
        'subtotal',
        'total',
        'iva',
        'total_iva',
        'observaciones',
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
