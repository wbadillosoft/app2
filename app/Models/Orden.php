<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Proveedor;
use App\Models\Detalles_orden as Detalles;

class Orden extends Model
{
    use HasFactory;

    protected $table = 'ordenes';

    protected $fillable = [
        'codigo',
        'cotizacion',
        'proveedor_id',
        'usuario_id',
        'fecha',
        'subtotal',
        'descuento',
        'iva',
        'total',
        'aprobo',
        'observaciones',
        'forma_pago',
        'estado',
    ];

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function detalles()
    {
        return $this->hasMany(Detalles::class);
    }   
}
