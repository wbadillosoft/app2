<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Producto;

class Detalles_orden extends Model
{
    use HasFactory;

    protected $table = 'detalles_orden';

    protected $fillable = [
        'orden_id',
        'producto_id',
        'cantidad',
        'costo',
        'porc_descuento',
        'descuento',
        'subtotal',
        'total',
        'iva',
        'total_iva',
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
  
}
