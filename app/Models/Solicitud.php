<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Detalles_solicitud As Detalles;

class Solicitud extends Model
{
    use HasFactory;
    protected $table = 'solicitudes';

    protected $fillable = [
        'cliente_id',
        'fecha',
        'nombre',
        'nit',
        'ciudad',
        'contacto',
        'email',
        'telefono',
        'estado',
        'codigo',
    ];

    public function detalles()
    {
        return $this->hasMany(Detalles::class);
    }
}
