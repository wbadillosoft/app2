<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Producto;

class Detalles_remision extends Model
{
    use HasFactory;
    protected $table = 'detalles_remision';

    protected $fillable = [
        'remision_id',
        'producto_id',
        'cantidad',
        'lote',
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
