<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Proveedor;

class Proveedores_por_producto extends Model
{
    use HasFactory;
    
    protected $table = 'proveedores_por_producto';

    protected $fillable = [
        'producto_id',
        'proveedor_id',
        'deleted',
    ];
    
    public function proveedor(){ 
        return $this->belongsTo(Proveedor::class); //Pertenece a un producto.
    }
}
