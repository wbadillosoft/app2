<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Producto;

class Detalles_cotizacion extends Model
{
    use HasFactory;

    protected $table = 'detalles_cotizacion';

    protected $fillable = [
        'cotizacion_id',
        'producto_id',
        'cantidad',
        'porc_descuento',
        'descuento',
        'subtotal',
        'total',
        'iva',
        'total_iva',
        'tiempo',
        'notas',
        'condiciones',
        'observaciones'
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
