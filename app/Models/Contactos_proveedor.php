<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contactos_proveedor extends Model
{
    use HasFactory;

    protected $table = 'contactos_proveedor';

    protected $fillable = [
        'proveedor_id',
        'nombre',
        'telefono',
        'cargo',
        'email',
    ];

}
