<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteFacturacion extends Model
{
    use HasFactory;

    protected $connection = 'mysql2';
    protected $table = "clients";
    protected $fillable = [
        'name',
        'rut', 
        'web',
        'address',
        'notes',
        'email_factura',
        'city',
        'user',
        
    ]; 
}
