<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detalles_solicitud extends Model
{
    use HasFactory;
    protected $table = 'detalles_solicitud';
    protected $fillable = [
        'solicitud_id',
        'equipo',
        'marca',
        'modelo',
        'serial',
        'codigo_interno',
        'ubicacion',
        'mantenimiento',
        'mantenimiento_tipo',
        'descripcion',
        'metrologia',
        'metrologia_tipo',
        'conformidad',
        'rango',
        'magnitud',
    ];

}
