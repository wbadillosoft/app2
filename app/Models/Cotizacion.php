<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Cliente;
use App\Models\Detalles_cotizacion as Detalles;

class Cotizacion extends Model
{
    use HasFactory;
    
    protected $table = 'cotizaciones';

    protected $fillable = [
        'codigo',
        'cliente_id',
        'usuario_id',
        'fecha',
        'subtotal',
        'descuento',
        'iva',
        'total',
        'contacto_id',
        'validez',
        'observaciones',
        'condiciones',
        'estado',
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function detalles()
    {
        return $this->hasMany(Detalles::class);
    }
}
