<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Actividad;
use App\Models\Cliente;

class Proceso extends Model
{
    use HasFactory;

    protected $fillable = [
        'titulo',
        'cliente_id',
        'usuario',
        'fecha_apertura',
        'fecha_cierre',
        'abierto_por',
        'responsable',
        'asignado',
        'estado',
        'dias',
        'estado',
    ];

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

}
