<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Cliente;


class Contactos_cliente extends Model
{
    use HasFactory;

    protected $table = 'contactos_cliente';

    protected $fillable = [
        'cliente_id',
        'nombre',
        'telefono',
        'cargo',
        'email',
    ];
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
    
}
