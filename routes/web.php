<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\UsersTable;
use App\Http\Livewire\UserEdit;
use App\Http\Livewire\Clientes;
use App\Http\Livewire\Proveedores;
use App\Http\Livewire\Productos;
use App\Http\Livewire\Procesos;
use App\Http\Livewire\Cotizaciones;
use App\Http\Livewire\Solicitudes;
/*use App\Http\Livewire\Prefacturas;
use App\Http\Livewire\Remisiones;
use App\Http\Livewire\Ordenes;
*/
use App\Models\User;

use App\Models\Proceso;

use App\Http\Controllers\PdfController;
use App\Http\Controllers\Pdf_Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('/auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/home', Procesos::class)->name('procesos');
/* 
Route::middleware(['auth:sanctum', 'verified'])->get('/home', function () {
    return view('dashboard');
})->name('dashboard');
 */
#----------Usuarios   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/users', UsersTable::class)->name('users');
Route::middleware(['auth:sanctum', 'verified'])->get('/user/edit/{id}', UserEdit::class)->name('user.edit');

#----------Clientes   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/clientes', Clientes::class)->name('clientes');

#----------Proveedores   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/proveedores', Proveedores::class)->name('proveedores');

#----------Productos   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/productos', Productos::class)->name('productos');

#----------Procesos   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/procesos', Procesos::class)->name('procesos');

#----------Solicitudes   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/solicitudes', Solicitudes::class)->name('solicitudes');

#----------Cotizaciones   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/cotizaciones', Cotizaciones::class)->name('cotizaciones');
Route::middleware(['auth:sanctum', 'verified'])->get('/cotizaciones/pdf/{id}',[Pdf_Controller::class, 'cotizacionPdf'])->name('cotizacionesPdf');

#----------Solicitudes   -----------
#Route::middleware(['auth:sanctum', 'verified'])->get('/solicitudes', Solicitudes::class)->name('solicitudes');

#----------Prefacturas   -----------
/*
Route::middleware(['auth:sanctum', 'verified'])->get('/prefacturas', Prefacturas::class)->name('prefacturas');
Route::middleware(['auth:sanctum', 'verified'])->get('/prefacturas/pdf/{id}',[Pdf_Controller::class, 'prefacturaPdf'])->name('prefacturasPdf');
Route::middleware(['auth:sanctum', 'verified'])->get('/prefacturas/preview/{id}',[Pdf_Controller::class, 'prefacturaPreview'])->name('prefacturasPreview');

#----------Remisiones   -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/remisiones', Remisiones::class)->name('remisiones');
Route::middleware(['auth:sanctum', 'verified'])->get('/remisiones/pdf/{id}',[PdfController::class, 'remisionPdf'])->name('remisionesPdf');

#----------Ordenes de compra  -----------
Route::middleware(['auth:sanctum', 'verified'])->get('/compras', Ordenes::class)->name('compras');
Route::middleware(['auth:sanctum', 'verified'])->get('/ordenes/pdf/{id}',[Pdf_Controller::class, 'ordenPdf'])->name('ordenesPdf');
*/